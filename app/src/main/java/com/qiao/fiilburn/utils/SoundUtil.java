package com.qiao.fiilburn.utils;

import android.content.Context;
import android.media.AudioManager;
import android.util.Log;

/**
 * 声音大小的处理类
 *
 * @author Administrator
 */
public class SoundUtil {
    public static void setVoise(Context context, int percent) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int maxVolume;// 最大音量
        maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int volume = (percent * maxVolume) / 100;
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
    }

    public static boolean isWiredHeadsetOn(Context context) {
        AudioManager localAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        LogUtil.i("isWiredHeadseOn  —————— ：" + localAudioManager.isWiredHeadsetOn());
        return localAudioManager.isWiredHeadsetOn();
    }

    /**
     * 得到多媒体的音量,返回的是百分比
     *
     * @param context
     */
    public static int getMediaVolume(Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);// 最大音量当前音量
        int volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);// 当前的音量
        int nowVolume = volume * 100 / maxVolume;
        return nowVolume;
    }
}

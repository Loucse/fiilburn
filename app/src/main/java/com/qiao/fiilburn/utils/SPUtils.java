package com.qiao.fiilburn.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;


import com.qiao.fiilburn.launcher.bean.UrlImageVideo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 * 共享参数工具类
 *
 * @author apple
 */
public class SPUtils {
    public static final String SP_NAME = "config_fill";

    public static SharedPreferences getSP(Context context) {
        return context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
    }

    // 判断属性是否设置
    public static boolean isExist(Context context, String pname) {
        return (getSP(context).getAll() != null && getSP(context).getAll().size() > 0 && getSP(context).getAll().get(pname) != null) ? true : false;
    }

    // 获取boolean类型参数
    public static boolean getBoolean(Context context, String pname) {
        return getSP(context).getBoolean(pname, false);
    }

    // 设置boolean类型参数
    public static void setBoolean(Context context, String pname, boolean pvalue) {
        SharedPreferences.Editor editor = getSP(context).edit();
        editor.putBoolean(pname, pvalue);
        editor.commit();
    }

    public static double getLocate(Context context, String name) {
        return Double.parseDouble(getSP(context).getString(name, "0.0"));
    }

    public static void setLocate(Context context, String name, String value) {
        SharedPreferences.Editor editor = getSP(context).edit();
        editor.putString(name, value);
        editor.commit();
    }

    // 获取String类型参数
    public static String getString(Context context, String pname) {
        return getSP(context).getString(pname, null);
    }

    // 设置String类型参数
    public static void setString(Context context, String pname, String pvalue) {
        SharedPreferences.Editor editor = getSP(context).edit();
        editor.putString(pname, pvalue);
        editor.commit();
    }

    // 获取int类型参数
    public static int getInt(Context context, String pname) {
        return getSP(context).getInt(pname, 0);
    }

    // 设置int类型参数
    public static void setInt(Context context, String pname, int pvalue) {
        SharedPreferences.Editor editor = getSP(context).edit();
        editor.putInt(pname, pvalue);
        editor.commit();
//        if(Constan.USERID_FOREAR.equals(pname)){
//            LogUtil.e("发送userid = "+pvalue);
//            if(getInt(context,pname) == pvalue){
//                return;
//            }
//            new DBHelpDAO(context).cleanEnjoyList(SPUtils.getInt(context, Constan.USERID_FOREAR));
//            EventBus.getDefault().post(new BlueServerSendCommand(23,pvalue));
//        }
    }

    // 移除某一项的值。。
    public static void remove(Context context, String pname) {
        SharedPreferences.Editor editor = getSP(context).edit();
        editor.remove(pname);
        editor.commit();
    }

    /**
     * 集合存储为String
     *
     * @param imageVideoList
     * @return
     * @throws IOException
     */
    public static String WeatherList2String(List<UrlImageVideo> imageVideoList) throws IOException {
        // 实例化一个ByteArrayOutputStream对象，用来装载压缩后的字节文件。
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        // 然后将得到的字符数据装载到ObjectOutputStream
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        // writeObject 方法负责写入特定类的对象的状态，以便相应的 readObject 方法可以还原它
        objectOutputStream.writeObject(imageVideoList);
        // 最后，用Base64.encode将字节文件转换成Base64编码保存在String中
        String WeatherListString = new String(Base64.encode(byteArrayOutputStream.toByteArray(), Base64.DEFAULT));
        // 关闭objectOutputStream
        objectOutputStream.close();
        return WeatherListString;
    }

    /**
     * String转化为集合
     *
     * @return
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    public static List<UrlImageVideo> String2WeatherList(String WeatherListString) throws IOException, ClassNotFoundException {
        byte[] mobileBytes = Base64.decode(WeatherListString.getBytes(), Base64.DEFAULT);
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(mobileBytes);
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        List<UrlImageVideo> imageVideoList = (List<UrlImageVideo>) objectInputStream.readObject();
        objectInputStream.close();
        return imageVideoList;
    }

    // 获取int类型参数,默认值是-1，专门用于语音搜歌设置。。
    public static int getIntforSearch(Context context, String pname) {
        return getSP(context).getInt(pname, -1);
    }


}

package com.qiao.fiilburn.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.adapter.BurnViewAdapter;
import com.qiao.fiilburn.burn.bean.BurnSingle;
import com.qiao.fiilburn.burn.secvicer.BurnService;

public class DialogUtils {
    private static DialogUtils mDialogUtils;
    private AlertDialog mAlertDialog;


    public static DialogUtils getInstance() {
        if (mDialogUtils == null) {
            synchronized (DialogUtils.class) {
                if (mDialogUtils == null) {
                    mDialogUtils = new DialogUtils();
                }
            }
        }
        return mDialogUtils;
    }

    /**
     * 煲耳机修改名称
     *
     * @param context
     * @param position
     * @param burnSigle
     * @param burnService
     * @param mBurnViewAdapter
     */
    public void burnNameChange(Context context, final int position, final BurnSingle burnSigle, final BurnService burnService, final BurnViewAdapter mBurnViewAdapter) {
        AlertDialog.Builder builderPeriod = new AlertDialog.Builder(context);
        View viewPeriod = LayoutInflater.from(context).inflate(R.layout.fix_burn_dialog, null);
        final EditText etCreate = (EditText) viewPeriod.findViewById(R.id.et_fix);
        Button ok = (Button) viewPeriod.findViewById(R.id.set_name);
        Button cancle = (Button) viewPeriod.findViewById(R.id.cancel);
        int brunType = burnSigle.getBurntype();
        String hintStr = null;
        switch (brunType) {
            case 1:
                hintStr = context.getResources().getString(R.string.my_name);
                break;
            case 2:
                hintStr = context.getResources().getString(R.string.my_name);
                break;
            default:
                hintStr = burnSigle.getSname();
                break;
        }
        etCreate.setHint(hintStr);
        etCreate.setText(burnSigle.getSname());
        if (burnSigle.getSname().length() > 25) {
            etCreate.setSelection(25);
        } else {
            etCreate.setSelection(burnSigle.getSname().length());
        }

        // 设置自定义的布局
        builderPeriod.setView(viewPeriod);
        // 是否可以被取消
        builderPeriod.setCancelable(false);
        mAlertDialog = builderPeriod.show();
        final String finalHintStr = hintStr;
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                burnSigle.setSname(etCreate.getText().toString().equals("") ? finalHintStr : etCreate.getText().toString());
                burnService.updateBurnSigle(burnSigle);
                burnService.commitLocal();
//                burnService.commitHttp();
                mBurnViewAdapter.notifyItemChanged(position);
                mAlertDialog.dismiss();
            }
        });
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.dismiss();
            }
        });
    }

}

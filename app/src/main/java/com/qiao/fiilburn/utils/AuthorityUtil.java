package com.qiao.fiilburn.utils;

/**
 * 对http请求加密算法
 * @author liuyadong
 *
 */
public class AuthorityUtil {
	public static boolean isAuth(String uuid,String auth){
		if(uuid==null||uuid.length()<6){
			return false;
		}
		
		StringBuffer sb = new StringBuffer();
		sb.append(uuid);
		sb.insert(1, 'f');
		sb.insert(5, 'i');
		sb.insert(5, 'l');
		sb.insert(4, 'l');
		return Md5Util.getMD5Str(sb.toString()).equals(auth);
	}
	
	public static String auth(String uuid){
		if(uuid==null||uuid.length()<6){
			return null;
		}
		StringBuffer sb = new StringBuffer();
		sb.append(uuid);
		sb.insert(1, 'f');
		sb.insert(5, 'i');
		sb.insert(5, 'l');
		sb.insert(4, 'l');
		return Md5Util.getMD5Str(sb.toString());
	}
}

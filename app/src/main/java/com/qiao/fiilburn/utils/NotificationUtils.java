package com.qiao.fiilburn.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.qiao.fiilburn.R;

/**
 * @auther qiao9 on 2018/7/18
 * @Project FiilBurn
 */
public class NotificationUtils extends ContextWrapper {

    private NotificationManager manager;
    private static NotificationUtils instance;
    public static final String id = "channel";
    public static final String name = "channel_name";
    private Context context;


    public NotificationUtils(Context context) {
        super(context);
        this.context = context;
    }

    public static NotificationUtils getInstance(Context context) {
        if (instance == null) {
            synchronized (NotificationUtils.class) {
                if (instance == null) {
                    instance = new NotificationUtils(context);
                }
            }
        }
        return instance;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void createNotChannel() {
        NotificationChannel notChannel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_HIGH);
        notChannel.enableLights(true);
        notChannel.setLightColor(Color.RED);
        notChannel.setShowBadge(true);
        getManager().createNotificationChannel(notChannel);
    }


    public NotificationManager getManager() {
        if (manager == null) {
            synchronized (NotificationManager.class) {
                if (manager == null) {
                    manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                }
            }
        }
        return manager;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getChannelNot(CharSequence tickerText, long when, String title, String content, PendingIntent contentIntent) {
        return new Notification.Builder(context, id)
                .setContentTitle(title)
                .setContentTitle(content)
                .setTicker(tickerText)
                .setWhen(when)
                .setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.verify_logo)
                .setAutoCancel(true);
    }


    public NotificationCompat.Builder getNot_26(CharSequence tickerText, long when, String title, String content, PendingIntent contentIntent) {
        return new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(content)
                .setTicker(tickerText)
                .setWhen(when)
                .setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.verify_logo)
                .setAutoCancel(true);
    }

    public Notification sendNotification(CharSequence tickerText, long when, String title, String content, PendingIntent contentIntent) {
        Notification notification;
        if (Build.VERSION.SDK_INT >= 26) {
            createNotChannel();
            notification = getChannelNot(tickerText, when, title, content, contentIntent).build();
        } else {
            notification = getNot_26(tickerText, when, title, content, contentIntent).build();
        }
        getManager().notify(1, notification);
        return notification;
    }


}

package com.qiao.fiilburn.utils;

import com.qiao.fiilburn.burn.bean.BurnSingle;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Json工具类
 * Created by hyb on 2016/4/14.
 */
public class JsonUtils {
    private static JsonUtils mJsonUtils;
    private static double maxLatitude;
    private static double maxLongitude;
    private static double minLatitude;
    private static double minLongitude;

    private JsonUtils() {
    }

    public static JsonUtils getInstance() {
        if (mJsonUtils == null) {
            synchronized (JsonUtils.class) {
                if (mJsonUtils == null) {
                    mJsonUtils = new JsonUtils();
                }
            }
        }
        return mJsonUtils;
    }

    /**
     * 版本升级和语言jSON解析
     *
     * @param result
     * @return
     */
   /* public VersionAndLanguageBean parserVersionAndLanagerBeanJson(String result) {
        VersionAndLanguageBean bean = null;
        try {
            bean = new VersionAndLanguageBean();
            JSONObject jsonObject = new JSONObject(result);
            bean.setCode(jsonObject.optString("code"));
            bean.setDesc(jsonObject.optString("desc"));
            JSONObject jsonData = jsonObject.optJSONObject("data");
            VersionAndLanguageBean.DataEntity data = bean.new DataEntity();
            bean.setData(data);
            data.setCrc(jsonData.optString("crc"));
            data.setDesc(jsonData.optString("desc"));
            data.setHardwareversion(jsonData.optString("hardwareversion"));
            data.setVer(jsonData.optString("ver"));
            data.setUrl(jsonData.optString("url"));
            data.setSize(jsonData.optString("size"));
            data.setChipversion(jsonData.optString("chipversion"));
            List<VersionAndLanguageBean.LanguagelistEntity> list = new ArrayList<>();
            data.setLanguagelist(list);
            JSONArray jsonArray = jsonData.optJSONArray("languagelist");
            for (int i = 0; i < jsonArray.length(); i++) {
                VersionAndLanguageBean.LanguagelistEntity languagelistEntity = bean.new LanguagelistEntity();
                list.add(languagelistEntity);
                JSONObject object = jsonArray.optJSONObject(i);
                languagelistEntity.setCode(object.optString("code"));
                languagelistEntity.setId(object.optInt("id"));
                languagelistEntity.setIsshow(object.optInt("isshow"));
                languagelistEntity.setLanguage(object.optString("language"));
                languagelistEntity.setMakedate(object.optString("makedate"));
                languagelistEntity.setModifydate(object.optString("modifydate"));
            }
            Map<String, VersionAndLanguageBean.VerdescListEntity> map = new HashMap<>();
            data.setVerdesclist(map);
            jsonArray = jsonData.optJSONArray("verdescList");
            for (int i = 0; i < jsonArray.length(); i++) {
                VersionAndLanguageBean.VerdescListEntity verdescListEntity = bean.new VerdescListEntity();
                JSONObject object = jsonArray.optJSONObject(i);
                verdescListEntity.setCreatedate(object.optString("createdate"));
                verdescListEntity.setDescription(object.optString("description"));
                verdescListEntity.setId(object.optInt("id"));
                String lan = object.optString("language");
                verdescListEntity.setLanguage(lan);
                verdescListEntity.setModifydate(object.optString("modifydate"));
                verdescListEntity.setState(object.optInt("state"));
                verdescListEntity.setVerid(object.optInt("verid"));
                map.put(lan, verdescListEntity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bean;
    }*/

    /**
     * 解析线下门店数据
     *
     * @param json
     * @return
     */
  /*  public List<ProvinceBean> ParserOfflineJson(String json) {
        List<ProvinceBean> provinceBeanList = null;
        int count = 0;
        try {
            provinceBeanList = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArraySort = jsonObject.optJSONArray("sort");
            for (int i = 0; i < jsonArraySort.length(); i++) {
                ProvinceBean provinceBean = new ProvinceBean();
                provinceBeanList.add(provinceBean);
                provinceBean.sort = String.valueOf(i);
                provinceBean.name = jsonArraySort.optString(i);

                List<String> quNames = new ArrayList<>();
                provinceBean.cityName = quNames;

                Map<String, List<ProvinceBean.QuBean>> quListMap = new HashMap<>();
                provinceBean.cityMap = quListMap;

                JSONObject jsonObjectProvince = jsonObject.optJSONObject(provinceBean.name);

                JSONArray jsonCitySort = jsonObjectProvince.optJSONArray("sort");
                List<String> cityNames = new ArrayList<>();
                provinceBean.CityNames = cityNames;
                if (jsonCitySort != null) {
                    for (int j = 0; j < jsonCitySort.length(); j++) {
                        cityNames.add(jsonCitySort.optString(j));
                    }
                } else {
                    Iterator iterator = jsonObjectProvince.keys();
                    while (iterator.hasNext()) {
                        cityNames.add((String) iterator.next());
                    }
                }
                List<ProvinceBean.CityBean> cityBeens = new ArrayList<>();
                provinceBean.cityBeans = cityBeens;
                for (int j = 0; j < cityNames.size(); j++) {
                    ProvinceBean.CityBean cityBean = provinceBean.new CityBean();
                    cityBeens.add(cityBean);
                    cityBean.name = cityNames.get(j);
                    cityBean.sort = String.valueOf(j);
                    JSONObject jsonObjectCity = jsonObjectProvince.optJSONObject(cityNames.get(j));

                    List<ProvinceBean.QuBean> quBeens = new ArrayList<>();
                    cityBean.quBeans = quBeens;

                    quListMap.put(cityBean.name, quBeens);

                    Map<String, List<ProvinceBean.StoreBean>> quMap = new HashMap<>();
                    cityBean.quMap = quMap;

                    Iterator iterator = jsonObjectCity.keys();
                    int y = 0;
                    while (iterator.hasNext()) {
                        String quName = (String) iterator.next();
                        quNames.add(quName);
                        ProvinceBean.QuBean quBean = provinceBean.new QuBean();
                        quBeens.add(quBean);
                        quBean.name = quName;

                        List<ProvinceBean.StoreBean> storeBeens = new ArrayList<>();
                        quBean.storeBeans = storeBeens;

                        quMap.put(quName, storeBeens);

                        JSONArray jsonQuObject = jsonObjectCity.optJSONArray(quName);
                        for (int x = 0; x < jsonQuObject.length(); x++) {
                            ProvinceBean.StoreBean storeBean = new ProvinceBean.StoreBean();
                            storeBeens.add(storeBean);
                            JSONObject jsonObject2 = jsonQuObject.optJSONObject(x);

                            storeBean.setAddress(jsonObject2.optString("address"));
                            storeBean.setCity(jsonObject2.optString("city"));
                            storeBean.setCreateuserid(jsonObject2.optInt("createuserid"));
                            storeBean.setDisplayorder(jsonObject2.optInt("displayorder"));
                            storeBean.setDistrict(jsonObject2.optString("district"));
                            storeBean.setIcon(jsonObject2.optString("icon"));
                            storeBean.setId(jsonObject2.optInt("id"));
                            storeBean.setIssell(jsonObject2.optInt("issell"));
                            storeBean.setLinkman(jsonObject2.optString("linkman"));
                            storeBean.setMobile(jsonObject2.optString("mobile"));
                            storeBean.setModifyuser(jsonObject2.optString("modifyuser"));
                            storeBean.setModifyuserid(jsonObject2.optInt("modifyuserid"));
                            storeBean.setName(jsonObject2.optString("name"));
                            storeBean.setOpenhours(jsonObject2.optString("openhours"));
                            storeBean.setPhone(jsonObject2.optString("phone"));
                            storeBean.setPositionx(jsonObject2.optString("positionx"));
                            storeBean.setPositiony(jsonObject2.optString("positiony"));
                            storeBean.setProvince(jsonObject2.optString("province"));
                            storeBean.setRemark(jsonObject2.optString("remark"));
                            storeBean.setState(jsonObject2.optInt("state"));
                            storeBean.setTittlepic(jsonObject2.optString("tittlepic"));
                            storeBean.number = i + j + x + y;
                            count = storeBean.number;
                            double storeLatitude = Double.valueOf(storeBean.getPositiony());
                            double storeLongitude = Double.valueOf(storeBean.getPositionx());
                            if (maxLatitude == 0
                                    && minLongitude == 0) {
                                maxLatitude = storeLatitude;
                                minLatitude = storeLatitude;
                                maxLongitude = storeLongitude;
                                minLongitude = storeLongitude;
                            } else {
                                if (storeLatitude > maxLatitude) {
                                    maxLatitude = storeLatitude;
                                } else if (storeLatitude < minLatitude) {
                                    minLatitude = storeLatitude;
                                }

                                if (storeLongitude > maxLongitude) {
                                    maxLongitude = storeLongitude;
                                } else if (storeLongitude < minLongitude) {
                                    minLongitude = storeLongitude;
                                }
                            }
                        }
                        y++;
                    }
                }
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        return provinceBeanList;
    }*/

    /**
     * 解析线下门店数据
     *
     * @param json
     * @return
     */
   /* public int ParserOfflineJsonReturnInt(String json) {
        int count = 0;
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArraySort = jsonObject.optJSONArray("sort");
            for (int i = 0; i < jsonArraySort.length(); i++) {

                JSONObject jsonObjectProvince = jsonObject.optJSONObject(jsonArraySort.optString(i));

                JSONArray jsonCitySort = jsonObjectProvince.optJSONArray("sort");
                List<String> cityNames = new ArrayList<>();
                if (jsonCitySort != null) {
                    for (int j = 0; j < jsonCitySort.length(); j++) {
                        cityNames.add(jsonCitySort.optString(j));
                    }
                } else {
                    Iterator iterator = jsonObjectProvince.keys();
                    while (iterator.hasNext()) {
                        cityNames.add((String) iterator.next());
                    }
                }
                for (int j = 0; j < cityNames.size(); j++) {
                    JSONObject jsonObjectCity = jsonObjectProvince.optJSONObject(cityNames.get(j));


                    Iterator iterator = jsonObjectCity.keys();
                    int y = 0;
                    while (iterator.hasNext()) {
                        String quName = (String) iterator.next();

//                        List<ProvinceBean.StoreBean> storeBeens = new ArrayList<>();

                        JSONArray jsonQuObject = jsonObjectCity.optJSONArray(quName);
                        for (int x = 0; x < jsonQuObject.length(); x++) {
                            count = i + j + x + y;
                        }
                        y++;
                    }
                }
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        return count;
    }*/

    /**
     * 解析线下门店数据
     *
     * @return
     */
   /* public List<ProviceNameBean> ParserOfflineJsonReturnNameBeans(String json) {
        List<ProviceNameBean> proviceNameBeens = null;
        try {
            proviceNameBeens = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArraySort = jsonObject.optJSONArray("sort");
            for (int i = 0; i < jsonArraySort.length(); i++) {
                proviceNameBeens.add(new ProviceNameBean(jsonArraySort.optString(i), i));
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        return proviceNameBeens;
    }*/
    public static double getMaxLatitude() {
        return maxLatitude;
    }

    public static void setMaxLatitude(double maxLatitude) {
        JsonUtils.maxLatitude = maxLatitude;
    }

    public static double getMaxLongitude() {
        return maxLongitude;
    }

    public static void setMaxLongitude(double maxLongitude) {
        JsonUtils.maxLongitude = maxLongitude;
    }

    public static double getMinLatitude() {
        return minLatitude;
    }

    public static void setMinLatitude(double minLatitude) {
        JsonUtils.minLatitude = minLatitude;
    }

    public static double getMinLongitude() {
        return minLongitude;
    }

    public static void setMinLongitude(double minLongitude) {
        JsonUtils.minLongitude = minLongitude;
    }

    /**
     * 解析耳机列表的JSONObject。。在106的的接口出来之后进行的
     *
     * @return
     */
    /*public HttpResJson getHttpResSimpleJsonArray(String resStr) {
        HttpResJson res = null;
        try {
//            if (resStr==null||resStr.equals("")){//数值为空或者没有网。。最主要是没有网。。
//                data = "{\"code\":\"200\",\"desc\":\"SUCCESS\",\"data\":[{\"guaranteeenddate\":\"2016-12-01\",\"id\":11,\"isdefault\":0,\"mac\":\"\",\"makedate\":\"2016-03-42\",\"modifydate\":\"2016-04-06\", \"psn\":\"FIIL\",\"seqcode\":\"F032544U00344\",\"status\":0,\"type\":1,\"uid\":31},{\"guaranteeenddate\":\"2017-12-13\",\"id\":29, \"isdefault\":1,\"mac\":\"\",\"makedate\":\"2016-04-55\",\"modifydate\":\"2016-04-06\",\"psn\":\"FIIL Wireless\",\"seqcode\":\"F032550G00497\", \"status\":0,\"type\":2,\"uid\":31}]}";
//                JSONObject jsonObject = new JSONObject(data);
//                res.setCode(jsonObject.getString("code"));
//                res.setDesc(jsonObject.getString("desc"));
//                res.setTimestemp(resStr);
//            }else {
            res = new HttpResJson();
            JSONObject jsonObject = new JSONObject(resStr);
            res.setCode(jsonObject.getString("code"));
            res.setDesc(jsonObject.getString("desc"));
            res.setData(resStr);
//            }

        } catch (JSONException e) {
            LogUtil.e("json", e);
            res.setCode("300");// 此时回复报文无法解析
            res.setDesc("回复报文无法解析!");
            res = null;
        } catch (NullPointerException e) {
            LogUtil.e(e.toString());
            res = null;
        }

        return res;
    }
*/
    /**
     * 得到信息列表成功,信息拿回来了
     */
  /*  public ArrayList<ChooseEarInfo> parserData(String infoString, String lanKey) {
        List<ChooseEarInfo> earList = null;
        try {
            JSONObject jsonObject = new JSONObject(infoString);
            //当时解析的的时候就少用了这一步。。
            JSONObject dataJson = jsonObject.getJSONObject("data");
            JSONArray array = dataJson.getJSONArray(lanKey);
            earList = JSON.parseArray(array.toString(), ChooseEarInfo.class);
            LogUtil.i("HTTPGGGGGJH时候发放个怀念那+" + lanKey + earList.size());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return (ArrayList<ChooseEarInfo>) earList;
    }
*/

    /**
     * 得到手动解析的煲耳机列表
     */

    public BurnSingle getBurnSigle(String value) {
        BurnSingle burnSigle = new BurnSingle();
//        "firstRemind":[1467032400659,1467118800659,1466600400659,1466686800659,1466773200659]
//       "bid":"","burntype":2,"cTime":0,"diskDate":"",  ,"firstRun":0,"id":"",
// "mTime":0,"open":false,"remainingNum":1,"remainingTime":180000000,"sname":"我的FIIL Wireless耳机","soundLevel":1,"startDate":"2016-06-22 19:55:44","timing":"21:00|一、二、三、四、五",
// "timingids":"","totalNum":0,"totalTime":0,"type":1}}
        try {
            JSONObject jsonObject = new JSONObject(value);
//            burnSigle.setBid(jsonObject.optString("bid"));
            burnSigle.setBurntype(jsonObject.optInt("burntype"));
            burnSigle.setcTime(jsonObject.optLong("cTime"));
            burnSigle.setDiskDate(jsonObject.optString("diskDate"));
            burnSigle.setFirstRun(jsonObject.optInt("firstRun"));
            burnSigle.setId(jsonObject.optString("id"));
            burnSigle.setmTime(jsonObject.optLong("mTime"));
            burnSigle.setOpen(jsonObject.optBoolean("open"));
            burnSigle.setRemainingNum(jsonObject.optInt("remainingNum"));
            burnSigle.setRemainingTime(jsonObject.optLong("remainingTime"));
            burnSigle.setSname(jsonObject.optString("sname"));
            burnSigle.setSoundLevel(jsonObject.optInt("soundLevel"));
            burnSigle.setStartDate(jsonObject.optString("startDate"));
            burnSigle.setTiming(jsonObject.optString("timing"));
            burnSigle.setTimingids(jsonObject.optString("timingids"));
            burnSigle.setTotalNum(jsonObject.optInt("totalNum"));
            burnSigle.setTotalTime(jsonObject.optInt("totalTime"));
//            burnSigle.setType(jsonObject.optInt("type"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return burnSigle;
    }

    /**
     * 解析最爱音乐信息
     *
     * @param value
     * @return
     */
   /* public EnjoyUpMusicInfo getEnjoyUpMusicInfo(String value) {
        EnjoyUpMusicInfo enjoyUpMusicInfos = null;
        try {
//            value = value.substring(1,value.length()-2);
//            int start = value.indexOf("data");
//            String s1 = value.substring(0,start+6);
//            String s2 = value.substring(start+7,value.length()-2);
//            String s3 = value.substring(value.length()-1,value.length());
//            value = s1+s2+s3;
            value = value.replaceAll("\"\\{", "{");
            value = value.replaceAll("\\}\"", "}");
            JSONObject jsonObject = new JSONObject(value);
            String code = jsonObject.optString("code");
            if ("200".equals(code)) {
                enjoyUpMusicInfos = new EnjoyUpMusicInfo();
                JSONObject jsonObject1 = jsonObject.optJSONObject("data");
                if (jsonObject1 != null) {
                    long timer = jsonObject1.optLong("timer");
                    enjoyUpMusicInfos.setTimer(timer);
                    ArrayList<EnjoyUpMusicInfo.EnjoyMusicInfo> enjoyMusicInfos = new ArrayList<>();
                    enjoyUpMusicInfos.setData(enjoyMusicInfos);
                    JSONArray jsonArray = jsonObject1.optJSONArray("data");
                    if (jsonArray != null) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            EnjoyUpMusicInfo.EnjoyMusicInfo info = enjoyUpMusicInfos.new EnjoyMusicInfo();
                            enjoyMusicInfos.add(info);
                            JSONObject jo = jsonArray.optJSONObject(i);
                            info.setTitle(jo.optString("title"));
                            info.setArtist(jo.optString("artist"));
                            info.setLocation(jo.optInt("location"));
                            LogUtil.e(info.toString());
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return enjoyUpMusicInfos;
    }

    public FiilUpdateBean parseUpdateInfo(String json) {
        FiilUpdateBean fiilUpdateBean = null;
        try {
            JSONObject jsonObject = new JSONObject(json);
            if (jsonObject != null) {
                fiilUpdateBean = new FiilUpdateBean();
                fiilUpdateBean.setCode(jsonObject.optString("code"));
                fiilUpdateBean.setDesc(jsonObject.optString("desc"));
                JSONObject jsonObject1 = jsonObject.optJSONObject("data");
                if (jsonObject1 != null) {
                    FiilUpdateBean.AppUpgradeInfo appUpgradeInfo = fiilUpdateBean.new AppUpgradeInfo();
                    fiilUpdateBean.setAppUpgradeInfos(appUpgradeInfo);
                    appUpgradeInfo.setChannel(jsonObject1.optString("channel"));
                    appUpgradeInfo.setDescription(jsonObject1.optString("description"));
                    appUpgradeInfo.setIsMust(jsonObject1.optInt("ismust"));
                    appUpgradeInfo.setSize(jsonObject1.optString("size"));
                    appUpgradeInfo.setUrl(jsonObject1.optString("url"));
                    appUpgradeInfo.setVersion(jsonObject1.optString("version"));
                    JSONObject jsonObject3 = jsonObject1.optJSONObject("appUpgradeInfoData");
                    if (jsonObject3 != null) {
                        Map<String, FiilUpdateBean.LanangerUpdateInfo> map = new HashMap<>();
                        appUpgradeInfo.setLanangerInfos(map);
                        Iterator<String> iterator = jsonObject3.keys();
                        while (iterator.hasNext()) {
                            FiilUpdateBean.LanangerUpdateInfo lanangerUpdateInfo = fiilUpdateBean.new LanangerUpdateInfo();
                            String key = iterator.next();
                            map.put(key, lanangerUpdateInfo);
                            JSONObject jsonObject2 = jsonObject3.optJSONObject(key);
                            lanangerUpdateInfo.setLanguage(jsonObject2.optString("language"));
                            lanangerUpdateInfo.setDescription(jsonObject2.optString("description"));
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return fiilUpdateBean;
    }

    @Nullable
    public List<SportUpdateBean> getSportUpdateBeen(HttpResJson resJson) {
        JSONArray listStrM = resJson.getListStr();//就是得到列表的一些信息
        if (listStrM == null || listStrM.length() == 0) {
            return null;
        }
        List<SportUpdateBean> sportBeens = new ArrayList<>();
        for (int i = 0; i < listStrM.length(); i++) {
            JSONObject jsonObject = listStrM.optJSONObject(i);
            SportUpdateBean bean = new SportUpdateBean();
            sportBeens.add(bean);
            bean.setMac(jsonObject.optString("mac"));
            bean.setUid(jsonObject.optInt("uid"));
            bean.setTimestemp(jsonObject.optLong("timestemp"));
            bean.setStepdata(jsonObject.optString("step"));
        }
        return sportBeens;
    }

    public List<CaratProDataBean> getCaratProDataArray(String s) {
        if (s == null || "".equals(s)) {
            return null;
        }
        try {
            JSONObject serverBean = new JSONObject(s);
            String code = serverBean.optString("code");
            if (!"200".equals(code)) {
                return null;
            }
            List<CaratProDataBean> caratProDataBeanLists = new ArrayList<>();
            JSONArray jsonArray = serverBean.optJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject caratBean = jsonArray.optJSONObject(i);
                CaratProDataBean bean = new CaratProDataBean();
                caratProDataBeanLists.add(bean);
                int avgHeartRate = caratBean.optInt("avgheartrate");
                bean.setAvgHeartRate(avgHeartRate);
                bean.setDuration(caratBean.optString("duration"));
                bean.setEndDate(caratBean.optString("enddate"));
                bean.setExcode(caratBean.optString("excode"));
                bean.setMac(caratBean.optString("mac"));
                bean.setMode(caratBean.optInt("mode"));
                bean.setUid(caratBean.optInt("uid"));
                bean.setStartDate(caratBean.optString("startdate"));
                bean.setTotalDistance(caratBean.optInt("totaldistance"));
                bean.setTotalSteps(caratBean.optInt("totalstep"));
                bean.setVo2Max(caratBean.optInt("vo2max"));
                bean.setType(1);
                JSONArray detaillist = caratBean.optJSONArray("detaillist");
                for (int j = 0; j < detaillist.length(); j++) {
                    JSONObject jsonObject = detaillist.optJSONObject(j);
                    String type = jsonObject.optString("type");
                    String value = jsonObject.optString("value");
                    switch (type) {
                        case "heartrates":
                            bean.setHeartRates(value);
                            break;
                        case "location":
                            bean.setLocation(value);
                            break;
                        case "speeds":
                            bean.setSpeeds(value);
                            break;
                        case "vo2s":
                            bean.setVo2(value);
                            break;
                        case "signalflagandqualitys":
                            bean.setSignalFlagAndQualitys(value);
                            break;
                        case "speedrate":
                            bean.setSpeedRate(value);
                            break;
                        case "distance":
                            bean.setDistance(value);
                            break;
                        case "calrs":
                            bean.setCalrs(value);
                            break;
                        case "calss":
                            bean.setCalss(value);
                            break;
                        case "correctvalue":
                            bean.setCorrectValue(Integer.valueOf(value));
                            break;
                    }
                }
            }
            return caratProDataBeanLists;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
*/
    /**
     * 解析服务器返回的平均数值
     *
     * @param result
     * @return
     */
 /*   public Map<String, Object> parserAvgData(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            String code = jsonObject.optString("code");
            if ("200".equals(code)) {
                Map<String, Object> map = new HashMap<>();
                JSONObject object = jsonObject.optJSONObject("data");
                map.put("avg_duration", object.optInt("avg_duration"));
                map.put("avg_heartrate", object.optInt("avg_heartrate"));
                map.put("avg_steppace", object.optInt("avg_steppace"));
                map.put("avg_speed", object.optInt("avg_speed"));
                map.put("avg_totaldistans", object.optInt("avg_totaldistans"));
                map.put("avg_vo2max", object.optInt("avg_vo2max"));
                map.put("count", object.optInt("count"));
                map.put("avg_totalcalr", object.optDouble("avg_totalcalr"));
                return map;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int[] getTotalDistanceAndCalr(String result) {
        int[] ints = new int[]{0, 0, 0, 0, 0};
        try {
            if (result == null) return ints;
            JSONObject jsonObject = new JSONObject(result);
            String code = jsonObject.optString("code");
            if ("200".equals(code)) {
                JSONArray jsonArray = jsonObject.optJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.optJSONObject(i);
                    int mode = object.optInt("mode");
                    switch (mode) {
                        case 0:
                            ints[i] = object.optInt("totaldistance");
                            break;
                        case 1:
                            ints[i] = object.optInt("totaldistance");
                            break;
                        case 2:
                            ints[i] = object.optInt("totaldistance");
                            break;
                        case 3:
                            ints[i] = object.optInt("totalcalr");
                            break;
                        case 4:
                            ints[i] = object.optInt("totalcalr");
                            break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ints;
    }
*/
    /**
     * 解析服务器返回的是使用谷歌还是微软
     *
     * @param result
     * @return
     */
   /* public String getGooleOrWei(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            String code = jsonObject.optString("code");
            if ("200".equals(code)) {
                String weiruan = jsonObject.optString("engine");
                return weiruan;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
*/

}

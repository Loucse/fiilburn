package com.qiao.fiilburn.utils;


import org.json.JSONException;
import org.json.JSONObject;

public class JSONObjectUtils {


    private static JSONObject jsonObject = null;

    public static JSONObject getJSONObject(String jsonStr) {
        if (jsonObject == null) {
            synchronized (JSONObject.class) {
                if (jsonObject == null) {
                    try {
                        jsonObject = new JSONObject(jsonStr);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }


        return jsonObject;
    }


}

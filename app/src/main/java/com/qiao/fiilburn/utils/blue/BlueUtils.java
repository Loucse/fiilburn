package com.qiao.fiilburn.utils.blue;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;

import com.qiao.fiilburn.utils.LogUtil;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@SuppressLint("NewApi")
public class BlueUtils {
    private static BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter(); // 蓝牙适配器
    private static String deviceNameLike = "fiil,f002";
    private boolean isSame;

    /**
     * 是否包含蓝牙模块
     *
     * @return
     */
    public static boolean isExist() {
        return adapter != null;
    }

    /**
     * 蓝牙模块是否可用
     *
     * @return
     */
    public static boolean isEnable() {
        if (!isExist()) {
            return false;
        }
        return adapter.enable();
    }

    /**
     * 打开蓝牙
     */
    public static void openBlue(Context context) {
        if (!isExist()) {
            return;
        }
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        context.startActivity(enableBtIntent);
    }

    /**
     * 获取已配对设备列表
     *
     * @return
     */
    public static List<BluetoothDevice> getPairs() {
        List<BluetoothDevice> list = new ArrayList<BluetoothDevice>();
        Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();
        // If there are paired devices
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                list.add(device);
            }
        }
        return list;
    }

    /**
     * 获取已经配对的fiil设备,就是为了处理配对的列表中有没有，我们的Fiil耳机
     *
     * @return
     */
    /*public static String getFiilDeviceAddress() {
        Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (isFiilAdressName(device.getAddress())) {
                    return device.getAddress();
                }
            }
        }
        return null;
    }*/

    public static boolean isConnectFiil() {
        Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (isFiilAdressName(device.getAddress())) {
                    Class clazz = device.getClass();
                    try {
                        Method method = clazz.getDeclaredMethod("isConnected");
                        boolean isConnect = (boolean) method.invoke(device);
                        if (isConnect)
                            return true;
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return false;
    }

    public static boolean isConnectBlue() {
        Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                Class clazz = device.getClass();
                try {
                    Method method = clazz.getDeclaredMethod("isConnected");
                    boolean isConnect = (boolean) method.invoke(device);
                    if (isConnect)
                        return true;
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }


    /**
     * 判断现在连接的蓝牙耳机是否为FIIL耳机，并且是通过蓝牙地址进行的判断,只是传过来一个地址
     *
     * @param name
     * @return
     */
    public static boolean isFiilAdressName(String name) {
        if (name == null || "".equals(name.trim())) {
            return false;
        }
        if (name.indexOf("B0:F1:A3") == 0) {
            // 如果蓝牙地址是以B0:F1:A3开头的
            return true;
        }
        if (name.indexOf("00:23:7F:00:00") == 0) {
            // 如果蓝牙地址是以 00:23:7F:00:00开头的
            String[] split = name.split(":");
            int length = split.length;
            int parseInt = Integer.parseInt(split[length - 1], 16);
            if (parseInt <= 99) {
                return true;
            }
        }
        return false;
    }

    /**
     * 是否连接蓝牙耳机
     *
     * @return
     */
    public static boolean isConnHeadSet() {
        try {
            if (adapter == null) {
                adapter = BluetoothAdapter.getDefaultAdapter();
            }
            int state = adapter.getProfileConnectionState(BluetoothProfile.HEADSET); // 蓝牙头戴式耳机，
            return BluetoothProfile.STATE_CONNECTED == state;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }


    /**
     * 是否连接a2DP,这个条件作为判断一点都不好用，在耳机进入配对模式的时候会出问题。。。
     *
     * @return
     */
    public static boolean isConnA2DP() {
        try {
            if (adapter == null) {
                adapter = BluetoothAdapter.getDefaultAdapter();
            }
            int state = adapter.getProfileConnectionState(BluetoothProfile.A2DP); // 蓝牙头戴式耳机，
            return BluetoothProfile.STATE_CONNECTED == state;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public static String getPairsStr() {
        String str = "";
        Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                str = str + device.getName() + "---" + device.getAddress() + "\n";
            }
        }
        return str;
    }

    /**
     * 开启蓝牙扫描
     */
    public static void startScan() {
        adapter.startDiscovery();
    }

    public static boolean isScan() {
        return adapter.isDiscovering();
    }

    /**
     * 结束扫描
     */
    public static void cancelScan() {
        adapter.cancelDiscovery();
    }

    public void connect(BluetoothDevice device) {
        // 固定的UUID

        final String SPP_UUID = "0000110B-0000-1000-8000-00805F9B34FB";
        UUID uuid = UUID.fromString(SPP_UUID);
        BluetoothSocket socket;
        try {
            socket = device.createRfcommSocketToServiceRecord(uuid);
            socket.connect();
        } catch (IOException e) {
            LogUtil.e("连接蓝牙设备", e);
        }
    }

    /**
     * 判断是否连接Fiil耳机，如果返回true就是连接，否则就是没有连接
     *
     * @param context
     * @return
     */
    /*public static void isContendFiil(final Context context) {
        int flag = -1;
        try {
            int a2dp = adapter.getProfileConnectionState(BluetoothProfile.A2DP);
            int headset = adapter.getProfileConnectionState(BluetoothProfile.HEADSET);
            int health = adapter.getProfileConnectionState(BluetoothProfile.HEALTH);
            if (a2dp == BluetoothProfile.STATE_CONNECTED) {
                flag = a2dp;
            } else if (headset == BluetoothProfile.STATE_CONNECTED) {
                flag = headset;
            } else if (health == BluetoothProfile.STATE_CONNECTED) {
                flag = health;
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        if (flag != -1) {
            adapter.getProfileProxy(context, new ServiceListener() {

                @Override
                public void onServiceDisconnected(int profile) {

                }

                @Override
                public void onServiceConnected(int profile, BluetoothProfile proxy) {
                    List<BluetoothDevice> mDevices = proxy.getConnectedDevices();
                    // LogUtil.i("数值++BlueUtil的问题++最外面++");
                    if (mDevices != null && mDevices.size() > 0) {
                        for (BluetoothDevice device : mDevices) {
                            // LogUtil.i("数值++BlueUtil的问题++进入++");
                            String blueAddress = String.valueOf(device.getAddress());
                            if (isFiilAdressName(blueAddress)) {
                                DeviceInfoHelp.setFiil(true);
                                DeviceInfoHelp.setIsFiilAddress(blueAddress);
                            } else {
                                DeviceInfoHelp.setFiil(false);
                                DeviceInfoHelp.setIsFiilAddress("");
                            }
                        }
                    } else {
                        DeviceInfoHelp.setFiil(false);
                        DeviceInfoHelp.setIsFiilAddress("");
                    }
                }
            }, flag);
        }
    }*/

    /**
     * 获取蓝牙地址
     *
     * @return
     */
    public static String getConnectBlueToothAddress() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        Class<BluetoothAdapter> bluetoothAdapterClass = BluetoothAdapter.class;//得到BluetoothAdapter的Class对象
        try {//得到连接状态的方法
            Method method = bluetoothAdapterClass.getDeclaredMethod("getConnectionState", (Class[]) null);
            //打开权限
            method.setAccessible(true);
            int state = (int) method.invoke(adapter, (Object[]) null);

            if (state == BluetoothAdapter.STATE_CONNECTED) {
                Set<BluetoothDevice> devices = adapter.getBondedDevices();

                for (BluetoothDevice device : devices) {
                    Method isConnectedMethod = BluetoothDevice.class.getDeclaredMethod("isConnected", (Class[]) null);
                    method.setAccessible(true);
                    boolean isConnected = (boolean) isConnectedMethod.invoke(device, (Object[]) null);
                    if (isConnected) {
                        //deviceList.add(device);
                        LogUtil.e(device.getAddress());
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

}

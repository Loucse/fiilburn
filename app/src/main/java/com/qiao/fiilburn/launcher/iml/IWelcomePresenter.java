package com.qiao.fiilburn.launcher.iml;

import android.content.Context;

import com.qiao.fiilburn.base.BaseIPresenter;

/**
 * Created by qiao9 on 2018/6/21.
 */

public interface IWelcomePresenter extends BaseIPresenter {


    /**
     * 对外提供是否跳过协议
     *
     * @return
     */
    boolean getIsSkip();

    void setIsSkip(boolean isSkip);


    /**
     * 设置声音是否播放
     */
    void setLauncherVoice();

    /**
     * 播放视频
     */
    void playVideo();

    /**
     * 获取权限
     */
    void getPermission();

    /**
     * 权限处理
     */
    void disPosePermission();

    /**
     * 跳过视频
     */
    void skipVideo();

    /**
     * 打开系统浏览器并跳转到指定url
     */
    void jumpSystemWeb();
}

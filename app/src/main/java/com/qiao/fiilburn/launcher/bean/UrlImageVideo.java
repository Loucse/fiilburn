package com.qiao.fiilburn.launcher.bean;

/**
 * 启动页的接口数据
 */

public class UrlImageVideo {
    private String desc;// 描述
    private int did;// 自定义id
    private String startDate;// 开始时间
    private String endDate;// 结束时间
    private String imageURLs;// 要展示的图片地址，多个以','分割
    private String name;// 名字
    private int jumpType;// 跳转类型 1：app内部打开2：app外部打开
    private String jumpURL;// 跳转URL，如果该字段有值，点击图片/视频会跳转到该地址
    private int type;// 类型 1：图2：视频
    private String vedioURL;// 视频地址
    private int onlyWifiDownload;// 后台控制在什么情况下下载1：只能wifi下载 0：不限制下载网络
    private int status;// ？？？？不清楚
    private int voiceCanClosed;// 用户是否可以主动关闭声音 1：用户能够主动关闭声音，显示关闭按钮
    // 0：用户不能够主动关闭声音，不显示关闭按钮
    private int lis;
    private int showTime;
    private int platform;

    public int getLis() {
        return lis;
    }

    public void setLis(int lis) {
        this.lis = lis;
    }

    public int getShowTime() {
        return showTime;
    }

    public void setShowTime(int showTime) {
        this.showTime = showTime;
    }

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public UrlImageVideo(String desc, int did, String startDate, String endDate, String imageURLs, String name, int jumpType, String jumpURL, int type, String vedioURL, int onlyWifiDownload, int status, int voiceCanClosed, int lis, int showTime, int platform) {
        this.desc = desc;
        this.did = did;
        this.startDate = startDate;
        this.endDate = endDate;
        this.imageURLs = imageURLs;
        this.name = name;
        this.jumpType = jumpType;
        this.jumpURL = jumpURL;
        this.type = type;
        this.vedioURL = vedioURL;
        this.onlyWifiDownload = onlyWifiDownload;
        this.status = status;
        this.voiceCanClosed = voiceCanClosed;
        this.lis = lis;
        this.showTime = showTime;
        this.platform = platform;
    }

    /**
     * 两个构造方法
     */

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getDid() {
        return did;
    }

    public void setDid(int did) {
        this.did = did;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getImageURLs() {
        return imageURLs;
    }

    public void setImageURLs(String imageURLs) {
        this.imageURLs = imageURLs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getJumpType() {
        return jumpType;
    }

    public void setJumpType(int jumpType) {
        this.jumpType = jumpType;
    }

    public String getJumpURL() {
        return jumpURL;
    }

    public void setJumpURL(String jumpURL) {
        this.jumpURL = jumpURL;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getVedioURL() {
        return vedioURL;
    }

    public void setVedioURL(String vedioURL) {
        this.vedioURL = vedioURL;
    }

    public int getOnlyWifiDownload() {
        return onlyWifiDownload;
    }

    public void setOnlyWifiDownload(int onlyWifiDownload) {
        this.onlyWifiDownload = onlyWifiDownload;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getVoiceCanClosed() {
        return voiceCanClosed;
    }

    public void setVoiceCanClosed(int voiceCanClosed) {
        this.voiceCanClosed = voiceCanClosed;
    }

    @Override
    public String toString() {
        return "UrlImageVideo [desc=" + desc + ", did=" + did + ", startDate="
                + startDate + ", endDate=" + endDate + ", imageURLs="
                + imageURLs + ", name=" + name + ", jumpType=" + jumpType
                + ", jumpURL=" + jumpURL + ", type=" + type + ", vedioURL="
                + vedioURL + ", onlyWifiDownload=" + onlyWifiDownload
                + ", status=" + status + ", voiceCanClosed=" + voiceCanClosed
                + "]";
    }

}

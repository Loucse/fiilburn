package com.qiao.fiilburn.launcher.presenter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.http.mode.HttpReqHeader;
import com.qiao.fiilburn.launcher.bean.UrlImageVideo;
import com.qiao.fiilburn.launcher.iml.IWelcomePresenter;
import com.qiao.fiilburn.launcher.iml.IWelcomeView;
import com.qiao.fiilburn.launcher.mode.WelcomeMode;
import com.qiao.fiilburn.launcher.view.WelcomeActivity;
import com.qiao.fiilburn.utils.AppUtils;
import com.qiao.fiilburn.utils.LogUtil;
import com.qiao.fiilburn.utils.Md5Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by qiao9 on 2018/6/21.
 * 启动页
 */

public class WelcomeActivityPresenter implements IWelcomePresenter {
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 1;

    private long permissionTime;//请求权限的时间

    private long videoStart;//视频开始播放的时间
    private String splashData;// 从永久中获取的数据，进行处理
    private String currentTime;// 当前手机的时间，开始就获取当前的时间
    private Boolean isImageShow = false;// 是否展示图片
    private Boolean isVideoShow = false;// 是否展示视频
    private List<String> imageUrlList = new ArrayList<String>();// 可用的图片网站的集合，经过MD5加密的地址，就是这么处理的
    private List<String> jumpUrlList = new ArrayList<>();  //调整的地址
    private List<String> videoUrlList = new ArrayList<String>();// 可用的视频网站的集合，经过MD5加密的地址，就是这么处理的
    private List<String> videoUrlListName = new ArrayList<String>();// 可用的视频网站的集合，没有经过MD5加密的地址，不是加密的
    private List<Integer> videoUrlListVoice = new ArrayList<Integer>();// 可用的视频声音开启的集合，记录的是是否显示声音的按钮
    private List<Integer> idImageList = new ArrayList<Integer>();// 可用的图片Id的集合，就是为了友盟统计
    private List<Integer> idVideoList = new ArrayList<Integer>();// 可用的视频Id的集合，就是为了友盟统计
    private String[] imageUrls = null;// 使用图片的数组
    private String[] videoUrls = null;// 使用视频的数组
    private int imageRandom = 0;// 使用图片地址随机数
    private int videoRandom = 0;// 使用视频地址随机数
    private List<UrlImageVideo> urlList;// 里面是两个对象，存了所需要的信息,有可能是多个对象
    private final String imagerBasePath = AppUtils.getFileRoot() + "image" + File.separator;// 存放图片的基本地址
    private final String videoBasePath = AppUtils.getFileRoot() + "video" + File.separator;// 存放图片的基本地址
    private long skipTime = 1000 * 60 * 180;// 增加的时间，3个小时。。
    private boolean isVoice;// 就是视频是否有声音,为true
    private MediaPlayer mediaPlayer;
    private int prosition = 0;// 处理从哪里开始的额问题
    private File fileVideo;// 视频的地址，就是存储在哪个地方。。。。

    private SurceCallBack mSurceCallBack = new SurceCallBack();

    public String[] permissionArray = new String[]{};
    private int[] permissionRecord = new int[2];//0,存储：1，手机

    private boolean isSkip, isAlreadyLoaded;//防止多次跳转，加载
    private int screenWidth, screenHeight;

    private SoftReference<Context> mContext = null;//通过弱引用实例化Context
    private SoftReference<WelcomeMode> mWelcomeMode = null;//通过弱引用实例化Context
    private IWelcomeView mView = null;//实例化接口类

    public WelcomeActivityPresenter(IWelcomeView mView) {
        this.mView = mView;
    }

    public WelcomeMode getMode() {
        if (mWelcomeMode == null || mWelcomeMode.get() == null) {
            mWelcomeMode = new SoftReference<>(new WelcomeMode(getContext()));
            return mWelcomeMode.get();
        }
        return mWelcomeMode.get();
    }

    @Override
    public Context getContext() {
        if (mContext == null || mContext.get() == null) {
            mContext = new SoftReference(mView.getContext());
            return mContext.get();
        }
        return mContext.get();
    }

    @Override
    public void onCreate() {
        initScreenParm();
        HttpReqHeader.getInstance().init(getContext());
        isVoice = getMode().getIsVoice();
//        mView.getOpenUrlSv().getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mView.getOpenUrlSv().getHolder().addCallback(mSurceCallBack);

        mediaPlayer = new MediaPlayer();
        getMode().setCurrentCome(1);
        ((WelcomeActivity) getContext()).saveLog(Constant.LAUNCHER, "");
    }

    /**
     * 初始化屏幕参数
     */
    private void initScreenParm() {
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
        double den = (double) screenHeight / (double) screenWidth;
        LogUtil.i(" screenHeight ___ " + den);
        LogUtil.i(" screenHeight ___ " + screenHeight);
        LogUtil.i(" screenHeight ___ " + screenWidth);
        if (den <= 1.86) {
            mView.getLauncherBelow().setVisibility(View.GONE);
        }
        mView.getVoiceBtn().getParent().requestDisallowInterceptTouchEvent(true);
        // 已经得到图片的地址和视频的地址了，开始处理下面的问题
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentTime = formatter.format(new Date(System.currentTimeMillis()));// 获取当前时间
        isAlreadyLoaded = false;
    }


    @Override
    public void onStart() {
    }


    @Override
    public void onResume() {
        if (isAlreadyLoaded) {
            //已经加载过
            return;
        }
        // 处理图片和视频的问题
        obtainSplashUrl();
        // 获取随机数的方法
        obtainRandom();
        isShowVoice();

        disPosePermission();
        //图片展示
        showPic();
    }

    /**
     * 图片展示
     */
    @SuppressLint("CheckResult")
    private void showPic() {
        if (permissionArray.length > 0) {
            Flowable.timer(2, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(welPmsConsumer);
        } else {
            if (isImageShow) {
                // 展示图片
                Flowable.timer(1, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(welShowConsumer);
            } else if (isVideoShow) {
                Flowable.timer(1, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(welVideoConsumer);
            } else {
                Flowable.timer(2, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(welNextConsumer);
            }
        }
    }

    /**
     * 显示图片Consumer
     */
    Consumer welShowConsumer = new Consumer() {
        @SuppressLint("ResourceType")
        @Override
        public void accept(Object o) throws Exception {
            LogUtil.i(" >>>>  图片啊 启动页<<<<");
            mView.getActWelcomeFl().setBackgroundColor(Color.BLACK);
            mView.getSplashImg().setVisibility(View.GONE);
            mView.getOpenUrlImg().setVisibility(View.VISIBLE);
            ((WelcomeActivity) getContext()).saveLog(Constant.SHOW_LAUNCHER_PIC, String.valueOf(idImageList.get(imageRandom)));
            //展示图片
            File file = new File(imageUrlList.get(imageRandom));
            if (file.exists()) {
                //如果文件存在，显示该文件
                InputStream inputStream;
                try {
                    inputStream = new FileInputStream(file);
                    // 通过InputStream创建BitmapDrawable对象
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ALPHA_8;
                    options.inSampleSize = 1;
                    // 通过BitmapDrawable对象获取Bitmap对象
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, options);
                    int width = bitmap.getWidth();
                    int height = bitmap.getHeight();
                    float scanWidth = screenWidth / width;

                    Matrix matrix = new Matrix();
                    matrix.postScale(scanWidth, scanWidth);
                    Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
                    if (bitmap != null) {
                        mView.getOpenUrlImg().setImageBitmap(createBitmap);
                    }

                } catch (Exception e) {
                    Flowable.just(3).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(welNextConsumer);
                    LogUtil.i("图片异常啦");
                }
            } else {
                Flowable.just(2).observeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread()).subscribe(welVideoConsumer);
            }
            if (isVideoShow) {
                Flowable.timer(3, TimeUnit.SECONDS).observeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(welVideoConsumer);
            } else {
                Flowable.timer(3, TimeUnit.SECONDS).observeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(welNextConsumer);
            }
        }
    };

    /**
     * 视频的啊Consumer
     */
    Consumer welVideoConsumer = new Consumer() {
        @SuppressLint("ResourceType")
        @Override
        public void accept(Object o) throws Exception {
            LogUtil.i(" >>>>  视频啊 启动页<<<<");
            ((WelcomeActivity) getContext()).saveLog(Constant.SHOW_LAUNCHER_VID, String.valueOf(idVideoList.get(videoRandom)));
            mView.getActWelcomeFl().setBackgroundColor(Color.BLACK);
//            mView.getLauncherBelow().setVisibility(View.GONE);
            if (videoUrlListVoice.get(videoRandom) == 1) {
                if (isVoice) {
                    mediaPlayer.setVolume(0.0f, 0.0f);
                }
                mView.getVoiceBtn().setVisibility(View.VISIBLE);
            }
            mView.getSplashImg().setVisibility(View.GONE);
            mView.getOpenUrlImg().setVisibility(View.GONE);
            mView.getOpenUrlSv().setVisibility(View.VISIBLE);
            mView.getSkipBtn().setVisibility(View.VISIBLE);
            playVideo();
            videoStart = System.currentTimeMillis();
        }
    };

    /**
     * 下一个界面啊Consumer
     */
    Consumer welNextConsumer = new Consumer() {
        @Override
        public void accept(Object o) throws Exception {
            LogUtil.i(" >>>>  跳转啊 启动页<<<<");
//            mView.getVoiceBtn().setVisibility(View.GONE);
            switchNextActivity();
        }
    };


    /**
     * 权限的Consumer
     */
    Consumer welPmsConsumer = new Consumer() {
        @Override
        public void accept(Object o) throws Exception {
            LogUtil.i(" >>>>  权限啊 启动页<<<<");
            mView.getSplashImg().setVisibility(View.GONE);
            mView.getPermissinLl().setVisibility(View.VISIBLE);
            mView.getLauncherBelow().setVisibility(View.INVISIBLE);
        }
    };

    /**
     * 判断声音显示
     */
    private void isShowVoice() {
        if (videoUrlList.size() > 0) {
            // 处理声音的图片，和处理是否降低音量的问题
            if (videoUrlListVoice.get(videoRandom) == 1) {
                if (isVoice) {
                    // 已经处理，就是没有声音
                    mView.getVoiceBtn().setText(getContext().getResources().getString(R.string.welcome_openvoice));
                } else {
                    // 第一次启动，或者声音开启了
                    mView.getVoiceBtn().setText(getContext().getResources().getString(R.string.welcome_closevoice));
                }
            }
            LogUtil.i(">>>> 加载视频 <<<< ");
        }
        isAlreadyLoaded = true;
    }

    /**
     * 处理图片和视频的问题
     */
    private void obtainSplashUrl() {
        splashData = getMode().getSplashData();
        LogUtil.i("Enter the get URL __ :" + splashData);
        if (splashData != null) {
            Gson gson = new Gson();
            //可能返回多张图片
            urlList = gson.fromJson(splashData, new TypeToken<List<UrlImageVideo>>() {
            }.getType());
            for (UrlImageVideo urlImageVideo : urlList) {
                //多个对象，区分
                if (urlImageVideo.getType() == 1) {
                    //图片的地址
                    imageUrls = urlImageVideo.getImageURLs().split(",");
                    String[] jumpUrls = urlImageVideo.getJumpURL().split(",");
                    //在开始时间和结束时间之间的那个时间，处理那个图片
                    if (currentTime.compareTo(urlImageVideo.getStartDate()) > 0 && currentTime.compareTo(urlImageVideo.getEndDate()) < 0) {
                        for (int i = 0; i < imageUrls.length; i++) {
                            if (!"".equals(imageUrls[i])) {
                                //如果数组不为空那么就是有图片地址
//                                String imageRecord = SPUtils.getString(getContext().getApplicationContext(), imageUrls[i]);//图片的记录，在下载完成的时候存图的是100，如果点击跳过选择的是遗传的时间
                                String imageRecord = getMode().getFinishTime(imageUrls[i]);//图片的记录，在下载完成的时候存图的是100，如果点击跳过选择的是遗传的时间
                                if (imageRecord != null) {
                                    String md5Str = Md5Util.getMD5Str(imageUrls[i]);
                                    String imageUrlPath = imagerBasePath + md5Str;
                                    File file = new File(imageUrlPath);
                                    //不是空说明图片已经下载完成
                                    if ("100".equals(imageRecord)) {
                                        if (file.exists()) {
                                            imageUrlList.add(imageUrlPath);
                                            jumpUrlList.add(jumpUrls[i]);
                                            idImageList.add(urlImageVideo.getDid());
                                            isImageShow = true;
                                        }
                                    } else if (currentTime.compareTo(imageRecord) > 0) {
                                        if (file.exists()) {
                                            imageUrlList.add(imageUrlPath);
                                            idImageList.add(urlImageVideo.getDid());
                                            isImageShow = true;
                                        }
                                    }
                                }

                            }
                        }
                    }
                } else if (urlImageVideo.getType() == 2) {
                    videoUrls = urlImageVideo.getVedioURL().split(",");
                    if (currentTime.compareTo(urlImageVideo.getStartDate()) > 0 && currentTime.compareTo(urlImageVideo.getEndDate()) < 0) {
                        for (int i = 0; i < videoUrls.length; i++) {
                            if (!"".equals(videoUrls[i])) {
                                // 得到的数组元素不是空，就是有图片的地址
                                String videoRecord = getMode().getFinishTime(videoUrls[i]);// 视频的记录，在下载完成的时候存入的是100，如果点击跳过选择的是遗传的时间
                                LogUtil.i("数值++得到的与视频恩额恩恩嫩有关的关于存储的值++" + videoRecord + "+本地的时间+" + currentTime);
                                if (videoRecord != null) {// 不是null说明视频已经下载完成了。。
                                    if ("100".equals(videoRecord)) {
                                        String md5Str = Md5Util.getMD5Str(videoUrls[i]);
                                        String videoUrlPath = videoBasePath + md5Str; //+ ".mp4"
                                        File file = new File(videoUrlPath);
                                        if (file.exists()) {
                                            videoUrlListName.add(videoUrls[i]);
                                            idVideoList.add(urlImageVideo.getDid());
                                            videoUrlListVoice.add(urlImageVideo.getVoiceCanClosed());
                                            videoUrlList.add(videoUrlPath);
                                            isVideoShow = true;
                                            LogUtil.i(">>>> 视频文件 <<<<< , 这是 1");
                                        }
                                    } else if (currentTime.compareTo(videoRecord) > 0) {
                                        String md5Str = Md5Util.getMD5Str(videoUrls[i]);
                                        String videoUrlPath = videoBasePath + md5Str;
                                        File file = new File(videoUrlPath);
                                        if (file.exists()) {
                                            videoUrlListName.add(videoUrls[i]);
                                            idVideoList.add(urlImageVideo.getDid());
                                            videoUrlListVoice.add(urlImageVideo.getVoiceCanClosed());
                                            videoUrlList.add(videoUrlPath);
                                            isVideoShow = true;
                                            LogUtil.i(">>>> 视频文件 <<<<< , 这是 2");
                                        }
                                    } else {
                                        int betweenTimer = getBetweenTimer(videoRecord);
                                        /*Map<String, String> arg3 = new HashMap<String, String>();
                                        arg3.put("isLog", "1");*/
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }

    /**
     * 用于统计。处理两个时间相差多少分钟。
     *
     * @param videoRecord
     */
    private int getBetweenTimer(String videoRecord) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            long currentTimeSeconds = formatter.parse(currentTime).getTime();
            long videoRecordSeconds = formatter.parse(videoRecord).getTime();
            long between = videoRecordSeconds - currentTimeSeconds;
            int betTime = (int) (between / 1000 / 60);
            return betTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 获取随机数的方法
     */
    private void obtainRandom() {
        if (imageUrlList.size() > 0) {
            imageRandom = new Random().nextInt(imageUrlList.size());
        }
        if (videoUrlList.size() > 0) {
            videoRandom = new Random().nextInt(videoUrlList.size());
        }
    }

    @Override
    public void onDestory() {

    }

    @Override
    public boolean getIsSkip() {
        return isSkip;
    }

    @Override
    public void setIsSkip(boolean isSkip) {
        this.isSkip = isSkip;
    }

    @Override
    public void setLauncherVoice() {
        if (isVoice) {
            mView.getVoiceBtn().setText(R.string.welcome_closevoice);
            isVoice = !isVoice;
            mediaPlayer.setVolume(1.0f, 1.0f);
            getMode().setLauncherVoice(isVoice);
        } else {
            mView.getVoiceBtn().setText(R.string.welcome_openvoice);
            isVoice = !isVoice;
            mediaPlayer.setVolume(1.0f, 1.0f);
            getMode().setLauncherVoice(isVoice);
        }

    }

    @SuppressLint("CheckResult")
    @Override
    public void skipVideo() {
        long startTime = System.currentTimeMillis() - videoStart;
        if (startTime > 0) {
            long time = startTime / 1000;
            if (time > 0 && time < 10) {
//                上传
            }
        }
        getMode().saveSkipUrlAndTime(videoUrlListName.get(videoRandom), getSkipTime());
        ((WelcomeActivity) getContext()).saveLog(Constant.SKIP_VID, String.valueOf(idVideoList.get(videoRandom)));
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
        Flowable.just(3).subscribeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread()).subscribe(welNextConsumer);
        LogUtil.i("====== 图片视频跳过 mediaPlayer __ >>> :" + mediaPlayer.isPlaying() + " _____ videoRandom _____ >>>>>>> ：" + videoRandom + " ______ videoUrlListName _____ >>>>>> : " + videoUrlListName);
    }

    @Override
    public void jumpSystemWeb() {
        if (jumpUrlList.size() == 0) {
            return;
        }
        switchNextActivity();
        String url = jumpUrlList.get(imageRandom);
        if (TextUtils.isEmpty(url) || "*".equals(url)) {
            return;
        }
        ((WelcomeActivity) getContext()).saveLog(Constant.JUMP_LAUNCHER_URL, String.valueOf(idImageList.get(imageRandom)));
        mView.disposeSkipAct(url);
    }

    /**
     * 切换到下一个Act
     */
    private void switchNextActivity() {
        int come = getMode().getCurrentCome();
        if (come == 0) {
            ((WelcomeActivity) getContext()).saveLog(Constant.FIRST_LAUNCHER, "首次启动");
        }
        mView.disposeSkipAct();
        ((WelcomeActivity) getContext()).finish();
    }

    /**
     * 点击跳过，处理下次实现的时间
     *
     * @return
     */
    public String getSkipTime() {

        @SuppressLint("SimpleDateFormat") SimpleDateFormat spFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = spFormatter.format(new Date(System.currentTimeMillis()));
        try {
            long millionSeconds = spFormatter.parse(currentTime).getTime();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(millionSeconds + skipTime);
            return spFormatter.format(calendar.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return currentTime;
    }

    @Override
    public void disPosePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int storage = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int phone = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE);
            ArrayList<String> list = new ArrayList<>();
            if (storage != PackageManager.PERMISSION_GRANTED) {
                list.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (phone != PackageManager.PERMISSION_GRANTED) {
                list.add(Manifest.permission.READ_PHONE_STATE);
            }
            if (list.size() > 0) {
                permissionArray = new String[list.size()];
                for (int m = 0; m < list.size(); m++) {
                    permissionArray[m] = list.get(m);
                }
            } else {
                permissionArray = new String[0];
            }
        }

    }

    @SuppressLint("CheckResult")
    @Override
    public void getPermission() {
        LogUtil.i("--- 获取权限 ---- >>>> ：" + permissionArray.length + ":___ : " + permissionArray[0]);
        ActivityCompat.requestPermissions((WelcomeActivity) getContext(), permissionArray, WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
        permissionTime = System.currentTimeMillis();
    }

    @SuppressLint("CheckResult")
    @Override
    public void playVideo() {
        fileVideo = new File(videoUrlList.get(videoRandom));
        if (!fileVideo.exists()) {
            Flowable.just(3).observeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(welNextConsumer);
            return;
        }
        LogUtil.i("视频播放" + videoUrlList.get(videoRandom));
        mediaPlayer.reset();// 重置为初始状态
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);// 设置音乐流的类型
        mediaPlayer.setDisplay(mView.getOpenUrlSv().getHolder());// 设置video影片以surfaceviewholder播放
        try {
            mediaPlayer.setDataSource(fileVideo.getAbsolutePath());
            mediaPlayer.prepare();// 缓冲
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    // Adjust the size of the video so it fits on the screen
                    int videoWidth = mediaPlayer.getVideoWidth();
                    int videoHeight = mediaPlayer.getVideoHeight();
                    float videoProportion = (float) videoWidth / (float) videoHeight;
                    android.view.ViewGroup.LayoutParams lp = mView.getOpenUrlSv().getLayoutParams();
                    if (screenWidth == 1080) {
                        screenHeight = 1920;
                    } else if (screenWidth == 720) {
                        screenHeight = 1280;
                    }
                    float screenProportion = (float) screenWidth / (float) screenHeight;
                    if (videoProportion > screenProportion) {
                        lp.width = screenWidth;
                        lp.height = (int) ((float) screenWidth / videoProportion);
                    } else {
                        lp.width = (int) (videoProportion * (float) screenHeight);
                        lp.height = screenHeight;
                    }
                    mView.getOpenUrlSv().setLayoutParams(lp);
                }
            });
            mediaPlayer.setOnCompletionListener(mp -> {
                Flowable.just(3).observeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(welNextConsumer);
                LogUtil.i("图片视频 just(3) 播放完毕，播放完毕");
            });

        } catch (Exception e) {
            // 处理比较大的错误
            e.printStackTrace();
        } // 设置路径
        mediaPlayer.start();// 播放

    }

    public void doNext(int requestCode, int[] grantResults) {
        if (requestCode == WRITE_EXTERNAL_STORAGE_REQUEST_CODE) {
            LogUtil.i("授权 ———— ：" + grantResults.length);
            //情况一：两次授权通过
            if (grantResults.length == 2) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    switchNextActivity();
//                    UmengInfo(false);
                } else {
                    if (System.currentTimeMillis() - permissionTime < 500) {//回调的太快
//                        new AlertDialog.Builder(WelcomeActivity.this).setMessage("")
                        Uri packageURI = Uri.parse("package:" + "com.qiao.fiilburn");
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, packageURI);
                        getContext().startActivity(intent);
                    }
                }
                /*if (grantResults[0] != PackageManager.PERMISSION_GRANTED && grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    // TODO: 2017/4/22 申请权限时，首先是电话权限，接着是存储权限
                    saveLog("30002", "存储和信息");
                } else if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    saveLog("30002", "信息");
                } else if (grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    saveLog("30002", "存储");
                }*/

            } else if (grantResults.length == 1) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    switchNextActivity();
//                    UmengInfo(false);
                } else {
                    if (System.currentTimeMillis() - permissionTime < 500) {//回调的太快
                        Uri packageURI = Uri.parse("package:" + "com.qiao.fiilburn");
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, packageURI);
                        getContext().startActivity(intent);
                    }
                  /*  if (permissionRecord[0] == 1) {
                        saveLog("30002", "存储");
                    } else if (permissionRecord[1] == 1) {
                        saveLog("30002", "手机");
                    }*/
                }
            }
        }
    }

    /**
     * 处理画面，为 serfaceView 服务
     */
    private final class SurceCallBack implements SurfaceHolder.Callback {
        /**
         * 创建画面
         *
         * @param holder
         */
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            if (prosition > 0 && fileVideo != null) {
                playVideo();
                mediaPlayer.seekTo(prosition);
                prosition = 0;
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            if (mediaPlayer.isPlaying()) {
                prosition = mediaPlayer.getCurrentPosition();
                mediaPlayer.stop();
            }
        }
    }

}

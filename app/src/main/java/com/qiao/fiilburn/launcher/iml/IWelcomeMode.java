package com.qiao.fiilburn.launcher.iml;

/**
 * Created by qiao9 on 2018/6/21.
 */

public interface IWelcomeMode {


    void saveSkipUrlAndTime(String s, String skipTime);

    boolean getIsVoice();

    void setLauncherVoice(boolean isVoice);

    int getCurrentCome();

    void setCurrentCome(int c);

    String getSplashData();

    String getFinishTime(String url);

}

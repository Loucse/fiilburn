package com.qiao.fiilburn.launcher.iml;

import android.content.Context;
import android.view.SurfaceView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.qiao.fiilburn.base.BaseIView;

/**
 * Created by qiao9 on 2018/6/21.
 */

public interface IWelcomeView extends BaseIView {


    RelativeLayout getLauncherRl();

    ImageView getSplashImg();

    ImageView getOpenUrlImg();

    SurfaceView getOpenUrlSv();

    Button getVoiceBtn();

    Button getSkipBtn();

    Button getNextBtn();


    LinearLayout getPermissinLl();

    LinearLayout getActWelcomeFl();


    RelativeLayout getLauncherBelow();
    /**
     * 处理跳转逻辑
     */
    void disposeSkipAct();

    void disposeSkipAct(String url);

}

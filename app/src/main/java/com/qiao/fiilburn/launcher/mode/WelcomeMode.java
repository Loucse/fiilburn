package com.qiao.fiilburn.launcher.mode;

import android.content.Context;

import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.launcher.iml.IWelcomeMode;
import com.qiao.fiilburn.utils.SPUtils;

import java.lang.ref.SoftReference;

public class WelcomeMode implements IWelcomeMode {
    private SoftReference<Context> mContext;

    public WelcomeMode(Context context) {
        mContext = new SoftReference<Context>(context);
    }

    @Override
    public void saveSkipUrlAndTime(String url, String skipTime) {
        SPUtils.setString(mContext.get().getApplicationContext(), url, skipTime);
    }

    @Override
    public boolean getIsVoice() {
        return SPUtils.getBoolean(mContext.get().getApplicationContext(), Constant.SPLASH_VIDEO_VOICE);// 就是视频是否有声音;
    }

    @Override
    public void setLauncherVoice(boolean isVoice) {
        SPUtils.setBoolean(mContext.get().getApplicationContext(), Constant.SPLASH_VIDEO_VOICE, isVoice);
    }

    @Override
    public int getCurrentCome() {
        return SPUtils.getInt(mContext.get(), Constant.CURRENT_COME);
    }

    @Override
    public void setCurrentCome(int currentCome) {
        if (currentCome != 0) {
            SPUtils.setInt(mContext.get(), Constant.CURRENT_COME, currentCome);
        }
    }


    @Override
    public String getSplashData() {
        return SPUtils.getString(mContext.get().getApplicationContext(), Constant.SPLASH_DATA);
    }

    @Override
    public String getFinishTime(String url) {
        return SPUtils.getString(mContext.get().getApplicationContext(), url);
    }
}

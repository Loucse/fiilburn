package com.qiao.fiilburn.launcher.service;

import android.annotation.SuppressLint;
import android.content.Intent;

import com.google.gson.Gson;
import com.qiao.fiilburn.base.BaseService;
import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.http.RetrofitInit;
import com.qiao.fiilburn.http.manager.DownLoadManager;
import com.qiao.fiilburn.launcher.bean.UrlImageVideo;
import com.qiao.fiilburn.utils.AppUtils;
import com.qiao.fiilburn.utils.LogUtil;
import com.qiao.fiilburn.utils.Md5Util;
import com.qiao.fiilburn.utils.NetUtils;
import com.qiao.fiilburn.utils.SPUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.schedulers.Schedulers;


/**
 * @auther qiao9 on 2018/7/13
 * @Project FiilBurn
 */
public class SplashService extends BaseService {

    private String[] imageUrls;// 使用图片的数组
    private String[] videoUrls;// 使用视频的数组

    private ArrayList<String> imageUrlList = new ArrayList<>();// 下载的图片网站的集合
    private ArrayList<String> videoUrlList = new ArrayList<>();// 下载的视频网站的集合

    private List<String> imageUrlListSp = new ArrayList<String>();// 永久的图片网站的集合
    private List<String> VideoUrlListSp = new ArrayList<String>();// 永久的视频网站的集合

    private int startDown = 0;// 下载的数量
    private int endDown = 0;// 下载完成的数量
    private boolean isDown = false;// 是否开始下载。有下载的话，就不关闭启动页服务

    private String imageBasePath = AppUtils.getFileRoot() + "image" + File.separator;
    private String videoBasePath = AppUtils.getFileRoot() + "video" + File.separator;

    @Override
    public void onCreate() {
        super.onCreate();
        RetrofitInit.getInstance().initGet();
        if (!new File(imageBasePath).exists()) {
            new File(imageBasePath).mkdirs();
//            LogUtil.e(" imagepath  + : " + new File(imageBasePath).exists());
        }
        if (!new File(videoBasePath).exists()) {
            new File(videoBasePath).mkdirs();
//            LogUtil.e(" imagepath  + : " + new File(imageBasePath).exists());
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        imageUrlList.clear();
        LogUtil.i("启动页服务 _____ 开启");
        List<UrlImageVideo> splashList = Constant.getSplashData();//获取启动页获得的数据
        if (splashList != null) {
            LogUtil.i("启动页服务 _____ 开启 _____ 不为空");
            for (UrlImageVideo urlImageVideo : splashList) {
                LogUtil.i("启动页服务 _____ 开启 _____ 开始获取urlimagevideo对象 —— ：" + urlImageVideo.getType());
                if (urlImageVideo.getType() == 1) {//判断需要显示图片还是视频
                    imageUrls = urlImageVideo.getImageURLs().split(",");//获取图片地址数组
                    for (int i = 0; i < imageUrls.length; i++) {//处理开始时间和显示时间之间的那个图片
                        if (!"".equals(imageUrls[i])) {//得到的数组元素不是空，就必然有图片地址
                            imageUrlList.add(imageUrls[i]);
                            fileAndDown(urlImageVideo, i);
                        }
                    }
                } else if (urlImageVideo.getType() == 2) {
                    videoUrls = urlImageVideo.getVedioURL().split(",");//获取图片地址数组
                    for (int i = 0; i < videoUrls.length; i++) {//处理开始时间和显示时间之间的那个图片
                        if (!"".equals(videoUrls[i])) {//得到的数组元素不是空，就必然有图片地址
                            videoUrlList.add(videoUrls[i]);
                            fileAndDown(urlImageVideo, i);
                        }
                    }
                }
            }
            deleteImageVideo();
            String splashDataStr = new Gson().toJson(splashList);
            LogUtil.i("启动页服务 ———— json ：" + splashDataStr);
            SPUtils.setString(getApplicationContext(), Constant.SPLASH_DATA, splashDataStr);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 处理文件事件 抽方法
     *
     * @param urlImageVideo
     * @param i
     */
    private void fileAndDown(UrlImageVideo urlImageVideo, final int i) {
        String str;
        String md5Str;
        String basePath;// 存放的基本地址
        if (urlImageVideo.getType() == 1) {
            str = SPUtils.getString(getApplicationContext(), imageUrls[i]);
            md5Str = Md5Util.getMD5Str(imageUrls[i]);
            basePath = imageBasePath + md5Str;
            if (str == null || !isFile(basePath)) {
                if (urlImageVideo.getOnlyWifiDownload() == 1) {//只在wifi下载
                    if (NetUtils.isWifi(getApplicationContext())) {
                        LogUtil.i("启动页服务 _____ 需要wifi ——下载图片");
                        downImage(basePath, imageUrls[i]);
                    } else {
                        SPUtils.setString(getApplicationContext(), imageUrls[i], null);
                    }
                } else {
                    LogUtil.i("启动页服务 _____ 不需wifi ——下载图片");
                    downImage(basePath, imageUrls[i]);
                }
            }
        } else if (urlImageVideo.getType() == 2) {
            str = SPUtils.getString(getApplicationContext(), videoUrls[i]);
            md5Str = Md5Util.getMD5Str(videoUrls[i]);
            basePath = videoBasePath + md5Str;
            if (str == null || !isFile(basePath)) {
                if (urlImageVideo.getOnlyWifiDownload() == 1) {//只在wifi下载
                    if (NetUtils.isWifi(getApplicationContext())) {
                        LogUtil.i("启动页服务 _____ 需要wifi ——下载图片");
                        downImage(basePath, videoUrls[i]);
                        LogUtil.i("启动页服务 下载视频");
                    } else {
                        SPUtils.setString(getApplicationContext(), videoUrls[i], null);
                    }
                } else {
                    LogUtil.i("启动页服务 _____ 不需wifi ——下载图片");
                    downImage(basePath, videoUrls[i]);
                }
            }
        }


    }

    @SuppressLint("CheckResult")
    private void downImage(String basePath, String dowloadUrls) {
        RetrofitInit.getApi().downloadFile(dowloadUrls)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .subscribe(responseBody -> {
                    if (DownLoadManager.writeResponseBodyToDisk(responseBody, basePath)) {
                        SPUtils.setString(SplashService.this, dowloadUrls, "100");
                        endDown++;
                        if (startDown == endDown) {
                            LogUtil.i("停止服务");
                            stopSelf();
                        }
                        LogUtil.i("下载完了");
                    }

                });
        startDown++;
        isDown = true;
    }

    private boolean isFile(String basePath) {
        File file = new File(basePath);
        if (file.exists()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 删除图片和视频
     */
    private void deleteImageVideo() {
        for (int j = 0; j < imageUrlListSp.size(); j++) {
            Boolean isDeleteImage = false;// 是否删除这张图片。
            for (int k = 0; k < imageUrlList.size(); k++) {
                if (imageUrlListSp.get(j).equals(imageUrlList.get(k))) {
                    isDeleteImage = true;
                }
            }
            if (!isDeleteImage) {
                // 这样图片在最新获取的资源中不存在。进行删除操作
                String md5Str = Md5Util.getMD5Str(imageUrlListSp.get(j));
                String imageUrlPath = imageBasePath + md5Str;
                File file = new File(imageUrlPath);
                if (file.exists()) {
                    file.delete();
                }
            }
        }
        for (int j = 0; j < VideoUrlListSp.size(); j++) {
            Boolean isDeleteImage = false;// 是否删除这张图片。
            for (int k = 0; k < videoUrlList.size(); k++) {
                if (VideoUrlListSp.get(j).equals(videoUrlList.get(k))) {
                    isDeleteImage = true;
                }
            }
            if (!isDeleteImage) {
                // 这样图片在最新获取的资源中不存在。进行删除操作
                String md5Str = Md5Util.getMD5Str(VideoUrlListSp.get(j));
                String videoUrlPath = videoBasePath + md5Str + ".mp4";
                File file = new File(videoUrlPath);
                if (file.exists()) {
                    file.delete();
                }
            }
        }
    }
}

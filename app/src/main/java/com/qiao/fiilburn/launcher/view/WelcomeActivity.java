package com.qiao.fiilburn.launcher.view;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.BaseActivity;
import com.qiao.fiilburn.burn.view.act.MainActivity;
import com.qiao.fiilburn.launcher.iml.IWelcomeView;
import com.qiao.fiilburn.launcher.presenter.WelcomeActivityPresenter;
import com.qiao.fiilburn.utils.LogUtil;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by qiao9 on 2018/6/20.
 */

public class WelcomeActivity extends BaseActivity implements IWelcomeView {


    @BindView(R.id.welcome_splash_img)
    ImageView welcomeSplashImg;
    @BindView(R.id.welcome_openurl_img)
    ImageView welcomeOpenurlImg;
    @BindView(R.id.welcome_openurl_sv)
    SurfaceView welcomeOpenurlSv;
    @BindView(R.id.welcome_voice_btn)
    Button welcomeVoiceBtn;
    @BindView(R.id.welcome_launcher_rl)
    RelativeLayout welcomeLauncherRl;
    @BindView(R.id.welcome_skip_btn)
    Button welcomeSkipBtn;
    @BindView(R.id.welcome_next_btn)
    Button welcomeNextBtn;
    @BindView(R.id.welcome_permission_ll)
    LinearLayout welcomePermissionLl;
    @BindView(R.id.activity_welcome_fl)
    LinearLayout activityWelcomeFl;
    @BindView(R.id.welcome_scroll)
    ScrollView welcomeScroll;
    @BindView(R.id.welcome_launcher_below)
    RelativeLayout welcomeLauncherBelow;


    private WelcomeActivityPresenter welcomeActivityPresenter = new WelcomeActivityPresenter(this);

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHideTile();
        setSystem7Gray();
        setTransNavigation();
        welcomeActivityPresenter.onCreate();
        welcomeScroll.setOnTouchListener((v, event) -> true);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_welcome;
    }

    @Override
    protected void onResume() {
        super.onResume();
        welcomeActivityPresenter.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        welcomeActivityPresenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        welcomeActivityPresenter.onDestory();
    }

    @SuppressLint("ResourceType")
    @OnClick({R.id.welcome_openurl_img, R.id.welcome_voice_btn, R.id.welcome_skip_btn, R.id.welcome_next_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.welcome_openurl_img:
                welcomeActivityPresenter.jumpSystemWeb();
                break;
            case R.id.welcome_voice_btn:
                welcomeActivityPresenter.setLauncherVoice();
                break;
            case R.id.welcome_skip_btn:
                welcomeActivityPresenter.skipVideo();
                break;
            case R.id.welcome_next_btn:
                welcomeActivityPresenter.disPosePermission();
                if (welcomeActivityPresenter.permissionArray.length > 0) {
                    welcomeActivityPresenter.getPermission();
                }
                break;
        }
    }


    /**
     * 处理跳转事件
     */
    @Override
    public void disposeSkipAct() {
        startActivity(new Intent(getContext(), MainActivity.class));
        welcomeActivityPresenter.setIsSkip(true);
        finish();
    }

    @Override
    public void disposeSkipAct(String url) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        Uri content_url = Uri.parse(url);
        intent.setData(content_url);
        startActivity(intent);
    }

    /**
     * 授权回来的提示
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        welcomeActivityPresenter.doNext(requestCode, grantResults);
        if (permissions.length > 0) {
            for (int m = 0; m < permissions.length; m++) {
                LogUtil.e("授权+" + permissions[m]);
            }
        }
        if (grantResults.length > 0) {
            for (int k = 0; k < grantResults.length; k++) {
                LogUtil.e("授权+" + grantResults[k]);
            }
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public RelativeLayout getLauncherRl() {
        return welcomeLauncherRl;
    }

    @Override
    public ImageView getSplashImg() {
        return welcomeSplashImg;
    }

    @Override
    public ImageView getOpenUrlImg() {
        return welcomeOpenurlImg;
    }

    @Override
    public SurfaceView getOpenUrlSv() {
        return welcomeOpenurlSv;
    }

    @Override
    public Button getVoiceBtn() {
        return welcomeVoiceBtn;
    }

    @Override
    public Button getSkipBtn() {
        return welcomeSkipBtn;
    }

    @Override
    public Button getNextBtn() {
        return welcomeNextBtn;
    }

    @Override
    public LinearLayout getPermissinLl() {
        return welcomePermissionLl;
    }

    @Override
    public LinearLayout getActWelcomeFl() {
        return activityWelcomeFl;
    }

    @Override
    public RelativeLayout getLauncherBelow() {
        return welcomeLauncherBelow;
    }
}
package com.qiao.fiilburn.about.view;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @auther qiao9 on 2018/7/12
 * @Project FiilBurn
 */
public class AboutActivity extends BaseActivity {


    @BindView(R.id.about_version)
    TextView aboutVersion;
    @BindView(R.id.title_bar_back)
    ImageView titleBarBack;
    @BindView(R.id.title_bar_title)
    TextView titleBarTitle;
    @BindView(R.id.title_bar_add)
    ImageView titleBarAdd;
    @BindView(R.id.title_bar_about)
    ImageView titleBarAbout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
    }

    private void init() {
        try {
            aboutVersion.setText("V " + getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        titleBarAbout.setVisibility(View.GONE);
        titleBarAdd.setVisibility(View.INVISIBLE);
        titleBarBack.setVisibility(View.VISIBLE);
        titleBarTitle.setText(R.string.about);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_about;
    }


    @OnClick(R.id.title_bar_back)
    public void onViewClicked() {
        this.finish();
    }
}

package com.qiao.fiilburn.headsetchoose.presenter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.qiao.fiilburn.headsetchoose.adapter.HeadListAdatper;
import com.qiao.fiilburn.headsetchoose.bean.HeadListInfo;
import com.qiao.fiilburn.headsetchoose.iml.IChooseEarPhonePresenter;
import com.qiao.fiilburn.headsetchoose.iml.IChooseEarView;
import com.qiao.fiilburn.headsetchoose.mode.ChooseEarMode;
import com.qiao.fiilburn.headsetchoose.view.ChooseEarphoneActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

public class ChooseEarPresenter implements IChooseEarPhonePresenter {

    List<HeadListInfo> earList;//耳机列表

    private SoftReference<Context> mContext = null;//通过弱引用实例化Context
    private SoftReference<ChooseEarMode> mChooseEarMode = null;//通过弱引用实例化Context
    private IChooseEarView mView = null;//实例化接口类

    public ChooseEarPresenter(IChooseEarView mView) {
        this.mView = mView;
    }

    public ChooseEarMode getMode() {
        if (mChooseEarMode == null || mChooseEarMode.get() == null) {
            mChooseEarMode = new SoftReference<>(new ChooseEarMode(getContext()));
            return mChooseEarMode.get();
        }
        return mChooseEarMode.get();
    }

    @Override
    public Context getContext() {
        if (mContext == null || mContext.get() == null) {
            mContext = new SoftReference(mView.getContext());
            return mContext.get();
        }
        return mContext.get();
    }

    @Override
    public void onCreate() {
        earList = getHeatListInfo(getContext());
        HeadListAdatper headListAdatper = new HeadListAdatper((ChooseEarphoneActivity) getContext(), earList);
        mView.getRecycler().setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mView.getRecycler().setAdapter(headListAdatper);
    }

    public List<HeadListInfo> getHeatListInfo(Context context) {
        earList = new ArrayList<>();
        String earModel = getMode().getHeadList();
        if (earModel != null) {
            earList = getNetHeadList(earModel, context, earList);
        } else {
            earList = getEarList(earList);
        }
        return earList;
    }

    private List<HeadListInfo> getEarList(List<HeadListInfo> earList) {
        earList.clear();
        String name = "{\"code\":\"200\",\"desc\":\"SUCCESS\",\"data\":[{\"setitle\":\"Driifter\",\"seimg\":\"http://manage.fengeek.com/fileupload/images/20170915/20170915181350_457.png\",\"sename\":\"头戴式耳机\"},{\"setitle\":\"Driifter\",\"seimg\":\"http://manage.fengeek.com/fileupload/images/20170915/20170915181350_457.png\",\"sename\":\"入耳式耳机\"}]}";
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(name);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            earList = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<HeadListInfo>>() {
            }.getType());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return earList;
    }

    private List<HeadListInfo> getNetHeadList(String earModel, Context context, List<HeadListInfo> earList) {
        return null;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestory() {

    }


}

package com.qiao.fiilburn.headsetchoose.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qiao.fiilburn.R;
import com.qiao.fiilburn.burn.create.presenter.CreateBurnPresenter;
import com.qiao.fiilburn.headsetchoose.bean.HeadListInfo;
import com.qiao.fiilburn.utils.LogUtil;

import java.util.List;

public class HeadListAdatper extends RecyclerView.Adapter<HeadListAdatper.HeatListViewHolder> {


    private Activity act;
    private List<HeadListInfo> headList;

    public HeadListAdatper(Activity act, List<HeadListInfo> headList) {
        this.act = act;
        this.headList = headList;
    }

    @Override
    public HeatListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HeatListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.heatset_choose_item, parent, false));
    }

    @Override
    public void onBindViewHolder(HeatListViewHolder holder, int position) {
        HeadListInfo heatListInfo = headList.get(position);
        holder.tvChooseItem.setText(heatListInfo.getSename());
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher);
        //TODO 记得修改图片
        Glide.with(act).load(heatListInfo.getSeimg()).apply(options).into(holder.chooseItemRecentContent);
    }


    @Override
    public int getItemCount() {
        return headList.size();
    }


    class HeatListViewHolder extends RecyclerView.ViewHolder {
        ImageView chooseItemRecentContent;
        TextView tvChooseItem;

        public HeatListViewHolder(View itemView) {
            super(itemView);
            chooseItemRecentContent = itemView.findViewById(R.id.choose_item_recentContent);
            tvChooseItem = itemView.findViewById(R.id.tv_choose_item);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    ToastUtils.getInstanse(act).showSnack(v, "点击了" + getAdapterPosition());
//                    act.skipMainAct()
                    LogUtil.i("在 Adapter 中打印 ：" + headList.size() + " + getAdapter + " + getAdapterPosition());
//                    new CreateBurnPresenter((CreateBurnActivity) act).createBurnDialog(1,headList.get(getAdapterPosition()).getType());
                }
            });

        }
    }
}

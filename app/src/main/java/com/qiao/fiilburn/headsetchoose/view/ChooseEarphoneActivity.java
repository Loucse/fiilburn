package com.qiao.fiilburn.headsetchoose.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.BaseActivity;
import com.qiao.fiilburn.headsetchoose.iml.IChooseEarView;
import com.qiao.fiilburn.headsetchoose.presenter.ChooseEarPresenter;

import butterknife.BindView;

public class ChooseEarphoneActivity extends BaseActivity implements IChooseEarView {


    /*@BindView(R.id.btn_head_back)
    Button btnHeadBack;*/
    @BindView(R.id.tv_head_title)
    TextView tvHeadTitle;
    /*@BindView(R.id.btn_head_next)
    Button btnHeadNext;*/
    @BindView(R.id.ll_head_title)
    RelativeLayout llHeadTitle;
    @BindView(R.id.rl_title)
    LinearLayout rlTitle;
    @BindView(R.id.activity_chooseheat_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.activity_chooseheat_rl)
    RelativeLayout chooseheatRl;
    @BindView(R.id.activity_chooseheat_viewflipper)
    ViewFlipper viewflipper;

    private ChooseEarPresenter chooseEarPresenter = new ChooseEarPresenter(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSystem7Gray();
        setTransNavigation();
        chooseEarPresenter.onCreate();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_createburn;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public TextView getTvHeadTitle() {
        return tvHeadTitle;
    }

    @Override
    public RecyclerView getRecycler() {
        return recyclerView;
    }

    @Override
    public ViewFlipper getViewflipper() {
        return viewflipper;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                viewflipper.setInAnimation(this, R.anim.in_leftright);
                viewflipper.setOutAnimation(this, R.anim.out_leftright);
                viewflipper.showPrevious();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}

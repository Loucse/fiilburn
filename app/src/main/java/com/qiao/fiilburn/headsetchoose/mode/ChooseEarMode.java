package com.qiao.fiilburn.headsetchoose.mode;

import android.content.Context;

import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.headsetchoose.iml.IChooseEarMode;
import com.qiao.fiilburn.utils.SPUtils;

import java.lang.ref.SoftReference;

public class ChooseEarMode implements IChooseEarMode{
    private SoftReference<Context> mContext;

    public ChooseEarMode(Context context) {
        mContext = new SoftReference<Context>(context);
    }

    public String getHeadList() {
        return SPUtils.getString(mContext.get(), Constant.GET_EAR_MODEL);
    }
}

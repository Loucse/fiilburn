package com.qiao.fiilburn.headsetchoose.bean;

import java.util.List;

public class HeadListInfo {

    private String seimg;
    private String sename;
    private String setitle;
    private int type;


    public String getSeimg() {
        return seimg;
    }

    public void setSeimg(String seimg) {
        this.seimg = seimg;
    }

    public String getSename() {
        return sename;
    }

    public void setSename(String sename) {
        this.sename = sename;
    }

    public String getSetitle() {
        return setitle;
    }

    public void setSetitle(String setitle) {
        this.setitle = setitle;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}

package com.qiao.fiilburn.headsetchoose.iml;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.qiao.fiilburn.R;

import butterknife.BindView;

public interface IChooseEarView {

    Context getContext();

    /**
     * 返回头部返回键
     * @return
     */
//    Button getBtnHeadBack();

    /**
     * 返回头部标题
     *
     * @return
     */
    TextView getTvHeadTitle();

    /**
     * 返回下一步按钮
     * @return
     */
//    Button getBtnHeadNext();

//    RelativeLayout getLlHeadTitle();
//    LinearLayout getRlTitle();

    /**
     * 返回瀑布流控件
     *
     * @return
     */
    RecyclerView getRecycler();

//    RelativeLayout getchooseheatRl();

    /**
     * 返回viewflipper
     *
     * @return
     */
    ViewFlipper getViewflipper();
}

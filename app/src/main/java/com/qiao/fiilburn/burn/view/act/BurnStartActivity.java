package com.qiao.fiilburn.burn.view.act;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.BaseActivity;
import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.burn.bean.BurnInfo;
import com.qiao.fiilburn.burn.iml.start.IStartView;
import com.qiao.fiilburn.burn.presenter.BurnActivityCollector;
import com.qiao.fiilburn.burn.presenter.StartPresenter;
import com.qiao.fiilburn.burn.secvicer.SoundService;
import com.qiao.fiilburn.burn.view.custom.BaseVisua2View;
import com.qiao.fiilburn.burn.view.custom.CirclePointView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 开启煲耳机
 *
 * @author Administrator
 */
public class BurnStartActivity extends BaseActivity implements IStartView {

    @BindView(R.id.title_bar_back)
    ImageView titleBarBack;
    @BindView(R.id.title_bar_title)
    TextView titleBarTitle;
    @BindView(R.id.title_bar_add)
    ImageView titleBarAdd;
    @BindView(R.id.cpv_bstart)
    CirclePointView cpBackground;
    /*@BindView(R.id.iv_start_bkd)
    ImageView ivStartBkd;*/
    @BindView(R.id.tv_bstart_over)
    TextView tvOver;
    @BindView(R.id.tv_start_stage)
    TextView tvStage;
    @BindView(R.id.tv_bstart_rest)
    TextView tvRest;
    /* @BindView(R.id.rl_start_image)
     RelativeLayout rlStartImage;*/
    @BindView(R.id.tv_start_burnnumber)
    TextView tvBurnNumber;
    @BindView(R.id.tv_start_name)
    TextView tvName;
    @BindView(R.id.btn_bstart_basic)
    Button btnBasic;
    @BindView(R.id.btn_bstart_start)
    Button btnStart;
    @BindView(R.id.btn_bstart_pause)
    Button btnPause;
    @BindView(R.id.btn_bstart_stop)
    Button btnStop;
    @BindView(R.id.btn_burn_one)
    Button btnBurnOne;
    @BindView(R.id.btn_burn_three)
    Button btnBurnThree;
    //下面是对音乐柱子的在处理。
    @BindView(R.id.bv_start_vis)
    BaseVisua2View mBaseVisualizerView;
    @BindView(R.id.iv_start_flag)
    ImageView ivTrigon;
    @BindView(R.id.iv_start_circle)
    ImageView ivCircle;
    @BindView(R.id.tv_start_flag)
    TextView tvFlag;
    @BindView(R.id.rl_start_flag)
    RelativeLayout rlStartFlag;
    @BindView(R.id.title_bar_about)
    ImageView titleBarAbout;

    // private int height;// 背景图片的高度，就是直径
    private StartPresenter presenter = new StartPresenter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSystem7Gray();
        setTransNavigation();
        presenter.onCreate();
        saveLog("1007", BurnInfo.getBurnTime());
        /*** 把这个activity加入Activity的堆栈中 *****/
        BurnActivityCollector.addActivity(BurnStartActivity.this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.burn_start_burn;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public void burnStart(String id) {
        Intent intent = new Intent(this, SoundService.class);
        intent.setAction("com.fengeek.service.SoundService");
        if (BurnInfo.isBurning()) {
            intent.putExtra(Constant.BURN_OPER, 1);
        } else {
            intent.putExtra(Constant.BURN_OPER, 0);
            intent.putExtra(Constant.BURN_BSTOSOUND, id);
        }
        startService(intent);
    }

    @Override
    public void burnPause() {
        Intent intent = new Intent(BurnStartActivity.this, SoundService.class);
        intent.setAction("com.fengeek.service.SoundService");
        intent.putExtra(Constant.BURN_OPER, 2);
        startService(intent);
    }

    @Override
    public void stopService() {
        BurnInfo.setId("");
        BurnInfo.init();
        // 暂停音乐
        Intent intent = new Intent(BurnStartActivity.this, SoundService.class);
        intent.setAction("com.fengeek.service.SoundService");
        intent.putExtra(Constant.BURN_OPER, 3);
        startService(intent);
    }

    @Override
    public void startService(int state) {
        Intent intent = new Intent(BurnStartActivity.this, SoundService.class);
        intent.setAction("com.fengeek.service.SoundService");
        switch (state) {
            case 6:
                intent.putExtra(Constant.BURN_OPER, 6);
                break;
        }
        startService(intent);
    }

    @Override
    public void sendReveicer(int i) {
        Intent intent1 = new Intent();
        intent1.putExtra(Constant.BURN_TYPE, 4);
        intent1.setAction("com.burnSigle.TotalTime");
        sendBroadcast(intent1);
        finish();
    }

    /**
     * 设置煲机过程中动画显示
     *
     * @param
     */
    @Override
    public void setPlayingAnim(boolean isBurnMiddle) {
        if (mBaseVisualizerView != null) {
            if (BurnInfo.isPlaying() && BurnInfo.getRealOnTime() > 0 && !isBurnMiddle) {
                //正在煲机
                mBaseVisualizerView.setMode(BaseVisua2View.Mode.MODE_TWO);
            } else {
                //处于休息的地方
                mBaseVisualizerView.setMode(BaseVisua2View.Mode.MODE_ONE);
            }
        }
    }


    @OnClick({R.id.title_bar_back, R.id.btn_bstart_start, R.id.btn_bstart_pause, R.id.btn_bstart_stop, R.id.btn_burn_one, R.id.btn_burn_three})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.title_bar_back:
                Intent intent = new Intent();
                intent.putExtra(Constant.BURN_TYPE, 4);
                intent.setAction("com.burnSigle.TotalTime");
                sendBroadcast(intent);
                finish();
                if (presenter.timer != null)
                    presenter.timer.cancel();
                break;
            case R.id.btn_bstart_start:
                presenter.startBurn(true);
                break;
            case R.id.btn_bstart_pause:
                presenter.startBurn(true);
                break;
            case R.id.btn_bstart_stop:
                presenter.stopDialog();
                break;
            case R.id.btn_burn_one:
                // 点击执行煲机一次
                BurnInfo.setBurnNumber(3);
                btnBurnOne.setVisibility(View.GONE);
                btnBurnThree.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_burn_three:
                // 点击执行煲机三次
                BurnInfo.setBurnNumber(1);
                btnBurnOne.setVisibility(View.VISIBLE);
                btnBurnThree.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public ImageView titleBarBack() {
        return titleBarBack;
    }

    @Override
    public ImageView titleBarabout() {
        return titleBarAbout;
    }

    @Override
    public TextView titleBarTitle() {
        return titleBarTitle;
    }

    @Override
    public ImageView titleBarAdd() {
        return titleBarAdd;
    }

    @Override
    public CirclePointView cpBackground() {
        return cpBackground;
    }

    @Override
    public TextView tvOver() {
        return tvOver;
    }

    @Override
    public TextView tvStage() {
        return tvStage;
    }

    @Override
    public TextView tvRest() {
        return tvRest;
    }

    @Override
    public TextView tvBurnNumber() {
        return tvBurnNumber;
    }

    @Override
    public TextView tvName() {
        return tvName;
    }

    @Override
    public Button btnBasic() {
        return btnBasic;
    }

    @Override
    public Button btnStart() {
        return btnStart;
    }

    @Override
    public Button btnPause() {
        return btnPause;
    }

    @Override
    public Button btnStop() {
        return btnStop;
    }

    @Override
    public Button btnBurnOne() {
        return btnBurnOne;
    }

    @Override
    public Button btnBurnThree() {
        return btnBurnThree;
    }

    @Override
    public BaseVisua2View mBaseVisualizerView() {
        return mBaseVisualizerView;
    }

    @Override
    public ImageView ivTrigon() {
        return ivTrigon;
    }

    @Override
    public ImageView ivCircle() {
        return ivCircle;
    }

    @Override
    public TextView tvFlag() {
        return tvFlag;
    }

    public Context getContext() {
        return this;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                //返回
                Intent intent2 = new Intent();
                intent2.putExtra(Constant.BURN_TYPE, 4);
                intent2.setAction("com.burnSigle.TotalTime");
                sendBroadcast(intent2);
                BurnStartActivity.this.finish();
                if (presenter.timer != null)
                    presenter.timer.cancel();
                return true;

        }

        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected void onDestroy() {
        presenter.onDestory();
        /*** 把这个activity移除ctivity的堆栈中 *****/
        BurnActivityCollector.removeActivity(BurnStartActivity.this);
        super.onDestroy();
    }

}

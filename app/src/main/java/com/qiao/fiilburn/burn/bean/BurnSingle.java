package com.qiao.fiilburn.burn.bean;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.utils.LogUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 描述： 煲机事件类的对象
 *
 * @author Yzz
 * @time 2016/2/20  16:27
 */

public class BurnSingle implements Cloneable, Parcelable {
    private String id;
    //    private String bid;// 蓝牙ID
//    private int type;// 蓝牙耳机：1；非蓝牙：0，，，旧版本的使用
    private int soundLevel;// FIIL: 1; FIIL Bestie: 2;**不在使用
    private String sname;// 方案名字
    private String diskDate;// 保存到本地的时间
    private String startDate;// 开始日期
    private long mTime;// 累计音乐时间（蓝牙耳机）
    private long cTime;// //累计其他时间（蓝牙耳机）
    private long remainingTime;// 剩余煲机时间 分钟
    private long totalTime;// 累计煲机时间 分钟
    private int remainingNum;// 剩余次数：**用来处理耳机位置的处理。     // 通过他来决定显示的类型
    private int totalNum;// //累计煲机次数
    private int firstRun;// 是否是第一次允许改方案.1允许更改，0不允许更改。
    /*
    2.0以后添加的新的字段，1对应F001，2对应F002,3对应F003,4对应的是F004   6对应的是F006或者F007 F015
    8对应的是F005或者F008   10对应的是F010  F11  F16
     */
    private int burntype;


    private static String weekStr = "日、一、二、三、四、五、六";//只是一个静态的变量。。
    private static String weekStrTiming = "日一二三四五六";//只是一个静态的变量,定时星期的选取。。老版本的weekStr

    private String timing;// 定时
    private String timingids;// 定时id列表字符串
    private transient SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    //	private SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");不在使用
    private transient SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    private boolean isOpen = false;


    public BurnSingle() {
        super();
    }


    public BurnSingle(int remainingNum) {
        this.remainingNum = remainingNum;
    }

    public BurnSingle(/*int type, */String id, String sname, int soundLevel, int burnType) {
        // 初始化参数,非蓝牙方案
        Date now = new Date();// 当前日期
        this.sname = sname;
        this.startDate = sdf.format(now);
        this.diskDate = sdf.format(now);
        this.cTime = 0;
        this.mTime = 0;
//        this.type = type;
        this.soundLevel = soundLevel;
        this.totalNum = 0;
        this.firstRun = 0;
        this.remainingNum = 1;
        this.remainingTime = Constant.BS_RTIME;
        this.id = sdf2.format(now) + id;
//        this.bid = "";
        this.timing = "21:00|一、二、三、四、五";
        this.timingids = "";
        this.burntype = burnType;
    }

    /**
     * 带有蓝牙值得构造函数
     */
   /* public BurnSingle(int type, String id, String sname, String bid, int soundLevel, int burnType) {
        // 初始化参数，蓝牙方案带有蓝牙地址
        Date now = new Date();// 当前日期
        this.sname = sname;
        this.startDate = sdf.format(now);
        this.diskDate = sdf.format(now);
        this.cTime = 0;
        this.mTime = 0;
        this.type = type;
        this.soundLevel = soundLevel;
        this.totalNum = 0;
        this.firstRun = 0;
        this.remainingNum = 1;
        this.remainingTime = Constant.BS_RTIME;
        this.id = sdf2.format(now) + id;
        this.bid = bid;
        this.timing = "21:00|一、二、三、四、五";
        this.timingids = "";
        this.burntype = burnType;
    }*/
    public BurnSingle clone() {
        BurnSingle cl = null;
        try {
            cl = (BurnSingle) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return cl;
    }

    /**
     * 获取时间日期字符串
     *
     * @return
     */
    public String gTime() {
        if (timing == null || timing.trim().equals("")) {
            return "21:00";
        }

        String[] temp = timing.split("\\|");
        if (temp == null || temp.length < 2) {
            return "21:00";
        }
        return temp[0];
    }

    /**
     * 获取日期字符串
     *
     * @return
     */
    public String gDate() {
        //多语言传来的值
        if (timing == null || timing.trim().equals("")) {
            return "日、一、二、三、四、五、六";
        }

        String[] temp = timing.split("\\|");
        if (temp == null || temp.length < 2) {
            return "日、一、二、三、四、五、六";
        }
        return temp[1];
    }

    /**
     * 获取日期字符串，weekDay是后面带空格的字符串
     *
     * @return
     */
    public String gDate(String[] weekDay) {
        Boolean isMonday, isTuesday, isWednesday, isThursday, isFriday, isSaturday, isSunday;//多语言处理
        //多语言传来的值
        if (timing == null || timing.trim().equals("")) {
            return gweekDay(weekDay);
        }

        String[] temp = timing.split("\\|");
        if (temp == null || temp.length < 2) {
            return gweekDay(weekDay);
        }

        String dateWeek = temp[1];
        String[] split = weekStr.split("、");
        Boolean[] week = new Boolean[7];
        for (int i = 0; i < split.length; i++) {
            week[i] = dateWeek.contains(split[i]);
        }
        isSunday = week[0];
        isMonday = week[1];
        isTuesday = week[2];
        isWednesday = week[3];
        isThursday = week[4];
        isFriday = week[5];
        isSaturday = week[6];
        String xq = (isSunday ? weekDay[0] + " " : "") + (isMonday ? weekDay[1] + " " : "") + (isTuesday ? weekDay[2] + " " : "") + (isWednesday ? weekDay[3] + " " : "")
                + (isThursday ? weekDay[4] + " " : "") + (isFriday ? weekDay[5] + " " : "") + (isSaturday ? weekDay[6] + " " : "");
//        xq = xq.length() > 0 ? xq.substring(0, xq.length() - 1) : xq;//最后的"、"被处理了
        return xq;
    }

    //处理多语言的day问题
    @Nullable
    private String gweekDay(String[] weekDay) {
        String day = "";
        for (int i = 0; i < weekDay.length; i++) {
            day += weekDay[i] + " ";
        }
        return day;
    }

    /**
     * 获取下一天字符串
     *
     * @return
     */
    public String gNextDate() {
        if (timing == null || "".equals(timing)) {
            return "";
        }
        // 获取今天星期
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int idx = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        String today = weekStrTiming.substring(idx, idx + 1);
        String arrStr = gDate();
        String[] arr = arrStr.split("、");

        if (arrStr.indexOf(today) >= 0) {// 此时说明天今天有定时任务
            // 比较当前时间是否已经过了提醒日期
            String temp = gTime();
            String[] tempArr = temp.split(":");
            int ds = Integer.parseInt(tempArr[0]) * 60
                    + Integer.parseInt(tempArr[1]);
            int now = calendar.get(Calendar.HOUR_OF_DAY) * 60
                    + calendar.get(Calendar.MINUTE);
            if (ds > now) {
                return "今天";
            }
        }

        // 判断明天是否有定时
        String tomorrow = weekStrTiming.substring((idx + 1) % 7, ((idx + 1) % 7) + 1);
        if (arrStr.indexOf(tomorrow) >= 0) {
            return "明天";
        }

        for (int i = idx + 1; i < weekStrTiming.length(); i++) {
            if (arrStr.indexOf(weekStrTiming.substring(i, i + 1)) >= 0) {
                return "周" + weekStrTiming.substring(i, i + 1);
            }
        }
        return "" + arr[0];
    }

    /**
     * 获取下一次运行日期
     *
     * @return
     */
    public String gNextTime() {
        if (timing == null || "".equals(timing)) {
            return "";
        }
        return gTime();
    }

    /**
     * 获取完成的百分比,由原来带有%修改为没有了
     *
     * @return
     */
    public String gFinishPercent() {
        if ((totalTime * 100) / remainingTime >= 100) {
            return 100 + "";
        }
        return (totalTime * 100) / remainingTime + "";
    }

    /**
     * 获取煲机了多少分钟
     *
     * @return
     */
    public String gFinishMin() {
        // 如果大于0，就取他的真实值;如果小于0就取值0
        return ((totalTime / 1000 / 60) > 0 ? (totalTime / 1000 / 60) : 0) + "";
    }

    /**
     * 获取剩余煲机多少分钟 没有用到！！！！！！
     *
     * @return
     */
    public String gSurplusMin() {
        return ((remainingTime / 1000 / 60) - ((totalTime / 1000 / 60) > 0 ? (totalTime / 1000 / 60) : 0)) + "";
    }

    /**
     * 获取煲机时使用的音量
     *
     * @return
     */
    public int gVolume() {
        int volume[][] = {{25, 35, 45, 60}, {25, 50, 65, 80}};
        int stage;
        if (totalTime < (remainingTime * 0.25)) {
            stage = 0;
        } else if (totalTime < (remainingTime * 0.5)) {
            stage = 1;
        } else if (totalTime < (remainingTime * 0.75)) {
            stage = 2;
        } else {
            stage = 3;
        }
        if (burntype - 1 == -1) {
            return volume[soundLevel][stage];
        }
        if (burntype < 1 || burntype > 8) {
            //就是处理下标越界
            return volume[0][stage];
        }
        return volume[burntype - 1][stage];
    }

    /**
     * 得到那个阶段
     *
     * @return
     */
    public int gStage() {
        if (totalTime < (remainingTime * 0.25)) {
            return 25;
        } else if (totalTime < (remainingTime * 0.5)) {
            return 50;
        } else if (totalTime < (remainingTime * 0.75)) {
            return 75;
        } else {
            return 100;
        }
    }

    /**
     * 得到那个阶段
     *
     * @return
     */
    public String gStageString() {
        if (totalTime < (remainingTime * 0.25)) {
            return "一阶段";
        } else if (totalTime < (remainingTime * 0.5)) {
            return "二阶段";
        } else if (totalTime < (remainingTime * 0.75)) {
            return "三阶段";
        } else {
            return "完成";
        }
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    /**
     * 获取提醒日期列表
     *
     * @return
     */
    public List<Calendar> getFirstRemind() {
        if (timing == null || "".equals(timing.trim())) {
            return null;
        }

        String[] temp = timing.split("\\|");
        if (temp == null || temp.length < 2) {
            return null;
        }

        if (temp[1].length() < 1 || temp[0].length() < 3) {
            return null;
        }

        // 判断一周都哪些天需要煲机提醒
        String[] xq = temp[1].split("、");
        String[] sj = temp[0].split(":");

        if (sj.length < 2 || xq.length < 1) {
            return null;
        }

        List<Calendar> list = new ArrayList<Calendar>();
        Calendar calendar = Calendar.getInstance();
        // 获取当前日期
        calendar.setTimeInMillis(System.currentTimeMillis());
        // 获取当前为周几
        int nowXq = calendar.get(Calendar.DAY_OF_WEEK);

        // 遍历定时的星期
        for (int i = 0; i < xq.length; i++) {
            String str = xq[i];
            String replace = str.replace("周", "");
            int idx = weekStrTiming.indexOf(replace) + 1;
            LogUtil.i("定时+" + "今天周几" + nowXq + "定时是周几+" + idx);
            if (nowXq < idx) {// 此时说明还没有过这一天
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sj[0]));
                cal.set(Calendar.MINUTE, Integer.parseInt(sj[1]));
                cal.set(Calendar.SECOND, 0);
                cal.add(Calendar.DAY_OF_YEAR, idx - nowXq);
                list.add(cal);
            } else if (nowXq == idx) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sj[0]));
                cal.set(Calendar.MINUTE, Integer.parseInt(sj[1]));
                cal.set(Calendar.SECOND, 0);

                // 判断当前日期是否大于给定日期
                if (calendar.getTimeInMillis() > cal.getTimeInMillis()) {
                    cal.add(Calendar.DAY_OF_YEAR, 7);
                }
                list.add(cal);

            } else {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sj[0]));
                cal.set(Calendar.MINUTE, Integer.parseInt(sj[1]));
                cal.set(Calendar.SECOND, 0);
                cal.add(Calendar.DAY_OF_YEAR, 7 + idx - nowXq);
                list.add(cal);
            }

        }

        return list;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

 /*   public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }*/

    public int getSoundLevel() {
        return soundLevel;
    }

    public void setSoundLevel(int soundLevel) {
        this.soundLevel = soundLevel;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getDiskDate() {
        return diskDate;
    }

    public void setDiskDate(String diskDate) {
        this.diskDate = diskDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public long getmTime() {
        return mTime;
    }

    public void setmTime(long mTime) {
        this.mTime = mTime;
    }

    public long getcTime() {
        return cTime;
    }

    public void setcTime(long cTime) {
        this.cTime = cTime;
    }

    public long getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(long remainingTime) {
        this.remainingTime = remainingTime;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }

    public int getRemainingNum() {
        return remainingNum;
    }

    public void setRemainingNum(int remainingNum) {
        this.remainingNum = remainingNum;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getFirstRun() {
        return firstRun;
    }

    public void setFirstRun(int firstRun) {
        this.firstRun = firstRun;
    }

    public int getBurntype() {
        return burntype;
    }

    public void setBurntype(int burntype) {
        this.burntype = burntype;
    }

    public String getTimingids() {
        return timingids;
    }

    public void setTimingids(String timingids) {
        this.timingids = timingids;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
//        dest.writeString(this.bid);
//        dest.writeInt(this.type);
        dest.writeInt(this.soundLevel);
        dest.writeString(this.sname);
        dest.writeString(this.diskDate);
        dest.writeString(this.startDate);
        dest.writeLong(this.mTime);
        dest.writeLong(this.cTime);
        dest.writeLong(this.remainingTime);
        dest.writeLong(this.totalTime);
        dest.writeInt(this.remainingNum);
        dest.writeInt(this.totalNum);
        dest.writeInt(this.firstRun);
        dest.writeInt(this.burntype);
        dest.writeString(this.timing);
        dest.writeString(this.timingids);
        dest.writeSerializable(this.sdf);
        dest.writeSerializable(this.sdf2);
        dest.writeByte(this.isOpen ? (byte) 1 : (byte) 0);
    }

    protected BurnSingle(Parcel in) {
        this.id = in.readString();
//        this.bid = in.readString();
//        this.type = in.readInt();
        this.soundLevel = in.readInt();
        this.sname = in.readString();
        this.diskDate = in.readString();
        this.startDate = in.readString();
        this.mTime = in.readLong();
        this.cTime = in.readLong();
        this.remainingTime = in.readLong();
        this.totalTime = in.readLong();
        this.remainingNum = in.readInt();
        this.totalNum = in.readInt();
        this.firstRun = in.readInt();
        this.burntype = in.readInt();
        this.timing = in.readString();
        this.timingids = in.readString();
        this.sdf = (SimpleDateFormat) in.readSerializable();
        this.sdf2 = (SimpleDateFormat) in.readSerializable();
        this.isOpen = in.readByte() != 0;
    }

    public static final Parcelable.Creator<BurnSingle> CREATOR = new Parcelable.Creator<BurnSingle>() {
        @Override
        public BurnSingle createFromParcel(Parcel source) {
            return new BurnSingle(source);
        }

        @Override
        public BurnSingle[] newArray(int size) {
            return new BurnSingle[size];
        }
    };

    @Override
    public String toString() {
        return "BurnSingle{" +
                "id='" + id + '\'' +
//                ", bid='" + bid + '\'' +
//                ", type=" + type +
                ", soundLevel=" + soundLevel +
                ", sname='" + sname + '\'' +
                ", diskDate='" + diskDate + '\'' +
                ", startDate='" + startDate + '\'' +
                ", mTime=" + mTime +
                ", cTime=" + cTime +
                ", remainingTime=" + remainingTime +
                ", totalTime=" + totalTime +
                ", remainingNum=" + remainingNum +
                ", totalNum=" + totalNum +
                ", firstRun=" + firstRun +
                ", burntype=" + burntype +
                ", timing='" + timing + '\'' +
                ", timingids='" + timingids + '\'' +
                ", sdf=" + sdf +
                ", sdf2=" + sdf2 +
                ", isOpen=" + isOpen +
                '}';
    }
}

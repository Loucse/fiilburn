package com.qiao.fiilburn.burn.create.iml;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.BaseIView;

import butterknife.BindView;

public interface ICreateBurnView extends BaseIView {


    ImageView titleBarBack();

    ImageView titleBarBabout();

    TextView titleBarTitle();

    ImageView titleBarAdd();

    ImageView hgeadsetImg();

    TextView headsetTv();

    LinearLayout headsetLl();

    ImageView earphoneImg();

    TextView earphoneTv();

    LinearLayout earphoneLl();

}

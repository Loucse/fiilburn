package com.qiao.fiilburn.burn.view.act;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.about.view.AboutActivity;
import com.qiao.fiilburn.base.BaseActivity;
import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.burn.create.view.CreateBurnActivity;
import com.qiao.fiilburn.burn.iml.main.IMainView;
import com.qiao.fiilburn.burn.presenter.MainPresenter;
import com.qiao.fiilburn.launcher.service.SplashService;
import com.qiao.fiilburn.utils.LogUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements IMainView {
    @BindView(R.id.title_bar_title)
    TextView titleBarTitle;
    @BindView(R.id.title_bar_add)
    ImageView titleBarAdd;
    @BindView(R.id.title_bar_back)
    ImageView titleBarBack;
    @BindView(R.id.title_bar_about)
    ImageView titleBarabout;
    @BindView(R.id.act_main_fragment)
    FrameLayout actMainFragment;

    MainPresenter mainPresenter = new MainPresenter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSystem7Gray();
        setTransNavigation();
        mainPresenter.onCreate();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @OnClick({R.id.title_bar_add, R.id.title_bar_about})
    public void onViewClicked(View v) {
        switch (v.getId()) {
            case R.id.title_bar_add:
                startActivity(new Intent(this, CreateBurnActivity.class));
                saveLog("1003", "");
                break;
            case R.id.title_bar_about:
                LogUtil.i("打开");
                startActivity(new Intent(this, AboutActivity.class));
        }
    }

    @Override
    public TextView getTitleBarTitle() {
        return titleBarTitle;
    }

    @Override
    public ImageView getTitleBarAdd() {
        return titleBarAdd;
    }

    @Override
    public ImageView getTitleBarBack() {
        return titleBarBack;
    }

    @Override
    public ImageView getTitleBarabout() {
        return titleBarabout;
    }

    @Override
    public FrameLayout getActMainFragment() {
        return actMainFragment;
    }

    @Override
    public void starLaunchImgSer() {
        startService(new Intent(getContext(), SplashService.class));
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        Intent intent2 = new Intent();
        intent2.putExtra(Constant.BURN_TYPE, 4);
        intent2.setAction("com.burnSigle.TotalTime");
        sendBroadcast(intent2);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onDestroy() {
        mainPresenter.onDestory();
        super.onDestroy();
    }
}

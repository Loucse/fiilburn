/*
package com.qiao.fiilburn.burn.helper;

import android.content.Context;

import com.fiil.sdk.config.Config;
import com.fiil.sdk.config.DeviceInfo;
import com.fiil.sdk.manager.FiilManager;
import com.qiao.fiilburn.utils.SPUtils;

*/
/**
 * Created by yuezongzhe on 2017/4/6.
 * 煲耳机页面的帮助类
 *//*


public class BurnStartHelp {
    private static BurnStartHelp burnStartHelp;

    public static BurnStartHelp getBurnStartHelp() {
        if (burnStartHelp == null) {
            burnStartHelp = new BurnStartHelp();
        }
        return burnStartHelp;
    }

    */
/**
     * 开始煲耳机处理该处理的音效
     *//*

    public void startSetSound(Context mContext, boolean setHeadset) {
        DeviceInfo deviceInfo = FiilManager.getInstance().getDeviceInfo();
        if (deviceInfo.isGaiaConnect()) {
            // 确定煲机的是蓝牙耳机
            if (setHeadset) {
                if (deviceInfo.isGaiaConnect()) {
                    if (deviceInfo.getEarType() == 2) {
                        String record = deviceInfo.getEq() + ":" + deviceInfo.getVal3D() + ":" + deviceInfo.getAnc();// 记录风格，声场，降噪的值
                        SPUtils.setString(mContext, "record", record);
                        // gaiaService.setANC(2);// 降噪设置。改为普通模式
                        FiilManager.getInstance().setAnc(Config.FIIL_WIRELESS_PUTONG, null);
                    } else if (deviceInfo.getEarType() == 8 || deviceInfo.getEarType() == 5 || deviceInfo.getEarType() == 250 || deviceInfo.getEarType() == 247) {
                        String record = deviceInfo.getEq() + ":" + deviceInfo.getVal3D() + ":" + deviceInfo.getAnc();// 记录风格，声场，降噪的值
                        SPUtils.setString(mContext, "record", record);
                        //gaiaService.setMAF(0);// 把MAF关闭。
                        FiilManager.getInstance().setAnc(Config.FIIL_DIVA_CLOSE, null);
                        if (deviceInfo.getEarType() == 5 & deviceInfo.getEarMode() == 2) {
                            FiilManager.getInstance().switchEarMode(Config.BLUE_MUSIC, null);
                        }
                        if (deviceInfo.getEarType() == 250 & deviceInfo.getEarMode() == 2) {
                            FiilManager.getInstance().switchEarMode(Config.BLUE_MUSIC, null);
                        }
                    } else if (deviceInfo.getEarType() == 6 || deviceInfo.getEarType() == 9) {//06,07，09没有降噪
                        String record = deviceInfo.getEq() + ":" + deviceInfo.getVal3D();// 记录风格，声场
                        SPUtils.setString(mContext, "record", record);
                    } else if (deviceInfo.getEarType() == 7) {
//                        LogUtil.i("this heat is >>>>>: + " + deviceInfo.getEarType() + "<<>>>" + deviceInfo.getEarMode());
                        if (deviceInfo.getEarType() == 7 & deviceInfo.getEarMode() == 2) {
                            FiilManager.getInstance().switchEarMode(Config.BLUE_MUSIC, null);
                        }
                    }
                    //BlueSetHelp.getBlueSetHelp().setStyle(gaiaService, 0);//风格
                    FiilManager.getInstance().setEq(Config.STYLE_MEDIUM, null);
                    //gaiaService.set3D(0);// 声场设置，改为关
                    FiilManager.getInstance().set3D(Config.VOIDE_CLOSE, null);
                } else {

                    return;
                }
            }
        }
    }


    */
/**
     * 煲耳机完成恢复
     *//*

    public void stopSetSound(Context mContext) {
        DeviceInfo deviceInfo = FiilManager.getInstance().getDeviceInfo();
        if (deviceInfo.isGaiaConnect()) {
            // 耳机的协议已经连接
            String instruct = SPUtils.getString(mContext, "record");
            if (instruct != null) {
                String[] split = instruct.split(":");
                FiilManager.getInstance().setEq(Integer.parseInt(split[0]), null);//风格设置
                FiilManager.getInstance().set3D(Integer.parseInt(split[1]), null);// 声场设置，改为关
                if (split.length == 3) {
                    if (deviceInfo.getEarType() == 2) {
                        int s = Integer.parseInt(split[2]);
                        if (0 < s & s < 4) {
                            FiilManager.getInstance().setAnc(Integer.parseInt(split[2]), null);// 降噪设置。改为普通模式
                        } else {
                            FiilManager.getInstance().setAnc(Config.FIIL_WIRELESS_PUTONG, null);// 防止出现不必要的错误
                        }
                    } else if (deviceInfo.getEarType() == 8 | deviceInfo.getEarType() == 5) {
                        FiilManager.getInstance().setAnc(Integer.parseInt(split[2]), null);
                    }
                }

            }
        }
    }


}
*/

package com.qiao.fiilburn.burn.view.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class SlideRecycleView extends RecyclerView {
    private int dp20;
    private int dp40;
    private int displayWidth;

    public SlideRecycleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public boolean onInterceptHoverEvent(MotionEvent event) {
        int dx = (int) event.getX();
        if (dx < dp20 || dx > displayWidth - dp40) {
            return true;
        }
        return super.onInterceptHoverEvent(event);
    }


    public void setDp20(int dp20) {
        this.dp20 = dp20;
    }

    public void setDp40(int dp40) {
        this.dp40 = dp40;
    }

    public void setDisplayWidth(int displayWidth) {
        this.displayWidth = displayWidth;
    }
}

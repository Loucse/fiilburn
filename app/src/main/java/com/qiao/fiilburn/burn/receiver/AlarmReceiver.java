package com.qiao.fiilburn.burn.receiver;

import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.burn.view.act.MainActivity;
import com.qiao.fiilburn.utils.LogUtil;
import com.qiao.fiilburn.utils.NotificationUtils;

import java.util.Date;

public class AlarmReceiver extends BroadcastReceiver {
    // 锁屏、唤醒相关
    private KeyguardManager km;
    private KeyguardLock kl;
    private PowerManager pm;
    private PowerManager.WakeLock wl;
    private String text[] = new String[]{};
    private Context context;

    @SuppressWarnings("deprecation")
    @Override
    public void onReceive(Context context, Intent intent) {
        Date date = new Date(System.currentTimeMillis());
        LogUtil.i("定时++开机之后firstTime" + "+此时systemTime+" + "+定时selectTime+" + date);
        String id = intent.getStringExtra(Constant.BURN_ID);
        String sname = intent.getStringExtra(Constant.BURN_SNAME);
        String percent = intent.getStringExtra(Constant.BURN_PERCNET);
        int earType = intent.getIntExtra(Constant.EAR_TYPE, 5);
        int perInt = Integer.parseInt(percent);
        this.context = context;
        wakeAndUnlock(true);
        // 发送通知栏消息  消息通知栏
        // 定义NotificationManager
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(ns);
        // 定义通知栏展现的内容信息
//        int icon = R.mipmap.verify_logo;
        CharSequence tickerText = "fiil+";
        long when = System.currentTimeMillis();
        text = context.getResources().getStringArray(R.array.burnremind);
        // 定义下拉通知栏时要展现的内容信息
        String contentTitle = context.getString(R.string.burnre_one) + sname + context.getString(R.string.burnre_two);
        String contentText = text[perInt / 25];
        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.putExtra(Constant.BURN_ID, id);
        notificationIntent.putExtra("place", 200);
        notificationIntent.putExtra(Constant.CHOOSE_MAIN, earType);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//		notification.setLatestEventInfo(context, contentTitle, contentText,contentIntent);//会出现问题，被下面的替代了
//		Notification notification = new Notification(icon, tickerText, when);//处理新的Notification来满足Notification
        /*Notification notification = new Notification.Builder(context)
                .setTicker(tickerText).setSmallIcon(icon).setWhen(when).setContentTitle(contentTitle)
                .setContentText(contentText).setContentIntent(contentIntent).getNotification();*/
        Notification notification = NotificationUtils.getInstance(context).sendNotification(tickerText, when, contentTitle, contentText, contentIntent);
        // 使用系统默认提示音
        notification.defaults = Notification.DEFAULT_SOUND;
        // 自定义声音 声音文件放在ram目录下，没有此目录自己创建一个
        // notification.sound = Uri.parse("android.resource://"
        // + context.getPackageName()
        // + "/" + R.raw.tx);//原来的坦克大战
        notification.flags |= Notification.FLAG_AUTO_CANCEL; // 点击一次后自动消除
        // 用mNotificationManager的notify方法通知用户生成标题栏消息通知
        mNotificationManager.notify(1, notification);

    }

    private void wakeAndUnlock(boolean b) {
        if (b) {
            // 获取电源管理器对象
            pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            // 获取PowerManager.WakeLock对象，后面的参数|表示同时传入两个值，最后的是调试用的Tag
            wl = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "bright");

            // 点亮屏幕
            wl.acquire();

            // 得到键盘锁管理器对象
            km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            kl = km.newKeyguardLock("unLock");

            // 解锁
//            kl.disableKeyguard();
        } else {
            // 锁屏
//            kl.reenableKeyguard();
            // 释放wakeLock，关灯
            wl.release();
        }
    }
}

package com.qiao.fiilburn.burn.create.presenter;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.burn.bean.BurnSingle;
import com.qiao.fiilburn.burn.create.iml.ICreateBurnPresenter;
import com.qiao.fiilburn.burn.create.iml.ICreateBurnView;
import com.qiao.fiilburn.burn.create.view.CreateBurnActivity;
import com.qiao.fiilburn.burn.secvicer.BurnService;
import com.qiao.fiilburn.headsetchoose.mode.ChooseEarMode;
import com.qiao.fiilburn.http.mode.HttpReqHeader;
import com.qiao.fiilburn.utils.LogUtil;

import java.lang.ref.SoftReference;

public class CreateBurnPresenter implements ICreateBurnPresenter {

    private SoftReference<Context> mContext = null;//通过弱引用实例化Context
    private SoftReference<ChooseEarMode> mChooseEarMode = null;//通过弱引用实例化Context
    private ICreateBurnView mView = null;//实例化接口类

    private BurnService burnService = BurnService.getInstance();

    public CreateBurnPresenter(ICreateBurnView mView) {
        this.mView = mView;
    }

    public ChooseEarMode getMode() {
        if (mChooseEarMode == null || mChooseEarMode.get() == null) {
            mChooseEarMode = new SoftReference<>(new ChooseEarMode(getContext()));
            return mChooseEarMode.get();
        }
        return mChooseEarMode.get();
    }

    @Override
    public Context getContext() {
        if (mContext == null || mContext.get() == null) {
            mContext = new SoftReference(mView.getContext());
            return mContext.get();
        }
        return mContext.get();
    }

    @Override
    public void onCreate() {
        mView.titleBarAdd().setVisibility(View.INVISIBLE);
        mView.titleBarBack().setVisibility(View.VISIBLE);
        mView.titleBarTitle().setText(getContext().getResources().getString(R.string.choose_earphone_type));
        mView.titleBarBabout().setVisibility(View.GONE);
    }


    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestory() {

    }

    /**
     * 创建煲机对话框
     */
    public void createBurnDialog(final int soundLeve, final int type) {
        AlertDialog alertDialog = null;
        final AlertDialog.Builder builderPeriod = new AlertDialog.Builder(getContext());
        View viewPeriod = ((CreateBurnActivity) getContext()).getLayoutInflater().inflate(R.layout.dialog_burn_create, null);
        final EditText etCreate = viewPeriod.findViewById(R.id.et_create);
        Button btnCancle = viewPeriod.findViewById(R.id.burn_creat_cancel);
        Button btnOk = viewPeriod.findViewById(R.id.burn_creat_ok);
        switch (type) {
            case 1:
                etCreate.setHint(getContext().getResources().getString(R.string.headset));
                etCreate.setText(getContext().getResources().getString(R.string.headset));
                etCreate.setSelection(getContext().getResources().getString(R.string.headset).length());
                break;
            case 2:
                etCreate.setHint(getContext().getResources().getString(R.string.earphone));
                etCreate.setText(getContext().getResources().getString(R.string.earphone));
                etCreate.setSelection(getContext().getResources().getString(R.string.earphone).length());
                break;
        }
        // 设置自定义的布局
        builderPeriod.setView(viewPeriod);
        // 是否可以被取消
        builderPeriod.setCancelable(false);
        alertDialog = builderPeriod.create();
        alertDialog.show();
        final AlertDialog finalAlertDialog = alertDialog;
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BurnSingle burnSigle = new BurnSingle(HttpReqHeader.getInstance().getUuid(), getEtCreate(), soundLeve, type);
                burnService.addBurnSigle(burnSigle);
                burnService.commitLocal();
                finalAlertDialog.dismiss();
                ((CreateBurnActivity) getContext()).saveLog("1004", etCreate.getText().toString());
                Intent intent = new Intent();
                intent.putExtra(Constant.BURN_ID, burnSigle.getId());
                intent.putExtra(Constant.BURN_TYPE, 2);
                intent.setAction("com.burnSigle.TotalTime");
                getContext().sendBroadcast(intent);
                ((CreateBurnActivity) getContext()).finish();
            }

            private String getEtCreate() {
                return etCreate.getText().toString().equals("") ? getContext().getResources().getString(R.string.myHead) : etCreate.getText().toString();
            }
        });
        btnCancle.setOnClickListener(v -> finalAlertDialog.dismiss());
    }
}

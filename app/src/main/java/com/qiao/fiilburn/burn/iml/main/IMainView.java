package com.qiao.fiilburn.burn.iml.main;

import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.qiao.fiilburn.base.BaseIView;

public interface IMainView extends BaseIView {
    /**
     * 返回title
     */
    TextView getTitleBarTitle();

    /**
     * 返回右上角
     */
    ImageView getTitleBarAdd();

    /**
     * 返回左上角
     */
    ImageView getTitleBarBack();

    /**
     * 返回左上角
     */
    ImageView getTitleBarabout();

    /**
     * 返回frame
     */
    FrameLayout getActMainFragment();

    /**
     * 启动首页下载
     */
    void starLaunchImgSer();
}

package com.qiao.fiilburn.burn.iml.start;


public interface IStartMode {
    /**
     * 获取随机的那个数组
     *
     * @return
     */
    String[] getBurnStart();

    /**
     * 获取煲机Id
     *
     * @return
     */
    String getBurnId();

    /**
     * 获取煲机标志
     */
    int getBurnFlag();

    /**
     * 获取实体
     */
    int getBurnEntry();

    /**
     * 获取音量
     *
     * @return
     */
    String getvolume();

    /**
     * 获取阶段名
     *
     * @return
     */
    String getStage(int i);

    /**
     * 获取请连接
     *
     * @return
     */
    String getPleaseConHead();

    /**
     * 获取煲机时间长
     * @param i
     * @return
     */
    String getBurnTime(int i);

    /**
     * 获取未连接耳机
     * @return
     */
    String getBurnNoar();
}

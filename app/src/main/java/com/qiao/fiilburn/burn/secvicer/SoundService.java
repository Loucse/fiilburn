package com.qiao.fiilburn.burn.secvicer;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.IBinder;
import android.text.TextUtils;

import com.qiao.fiilburn.base.BaseService;
import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.burn.bean.BurnInfo;
import com.qiao.fiilburn.burn.bean.BurnSingle;
import com.qiao.fiilburn.utils.LogUtil;
import com.qiao.fiilburn.utils.SPUtils;
import com.qiao.fiilburn.utils.SoundUtil;
import com.qiao.fiilburn.utils.blue.BlueUtils;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 播放音乐的服务。。
 */
public class SoundService extends BaseService {
    private long onTime = 0;// 播放时长
    private long waitTime = 0;// 第一次循环到第二次循环的等待时长
    private long lastTj;// 最后统计日期
    // private long presTime = 0;// 持久化后又播放的时长
    private boolean isBurning = false;
    private boolean isPlaying = false;
    private boolean isWaiting = false;// 是否正在等待。
    private boolean isPausedByTransientLossOfFocus = false;// 重新获取焦点后是否立马播放
    private boolean lastIsMusicOn = false;
    private boolean isMiddleStop = false;
    private MediaPlayer mp;
    private static Timer timer;
    private TimerTask timerTask;
    private AudioManager burnAudioManager;// 煲耳机的音频管理服务
    private HeadsetPlugReceiver headsetPlugReceiver;// 监听耳机插入拔出等事件
    private boolean isWaitingprepare = false;
    private BurnService burnService = BurnService.getInstance();
    private static BurnSingle burnSigle;// 得到的具体的煲机方案

    @Override
    public void onCreate() {
        super.onCreate();
        handleInit();
        // 注册监听耳机插入拔出等事件
        headsetPlugReceiver = new HeadsetPlugReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.HEADSET_PLUG");
        intentFilter.addAction(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED);
        intentFilter.addAction("android.bluetooth.adapter.action.BLE_ACL_DISCONNECTED");
        intentFilter.addAction("android.bluetooth.adapter.action.BLE_STATE_CHANGED");
//        intentFilter.addAction("android.bluetooth.adapter.extra.PREVIOUS_CONNECTION_STATE");
//        intentFilter.addAction("android.bluetooth.adapter.extra.CONNECTION_STATE");
        registerReceiver(headsetPlugReceiver, intentFilter);
    }

    private void handleInit() {
        onTime = 0;
        BurnInfo.setPresTime(0);
        if (mp == null) {
            initMp();
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return Service.START_STICKY_COMPATIBILITY;
        }
        if (mp == null) {
            handleInit();
        }
        int oper = intent.getIntExtra(Constant.BURN_OPER, 0);
        LogUtil.i(oper + " : ___ 这是oper ");
        String bsId = intent.getStringExtra(Constant.BURN_BSTOSOUND);
        if (oper == 0) {// 煲机任务从0开始
            // 参数初始化
            onTime = 0;
            BurnInfo.setPresTime(0);
            burning(0);
            // 刚刚开始煲机存储音量
            SPUtils.setInt(getApplicationContext(), Constant.MULTI_MEDIA_VOLUME, SoundUtil.getMediaVolume(getApplicationContext()));
            if (!TextUtils.isEmpty(bsId)) {
                burnSigle = burnService.getBurnSigle(String.valueOf(bsId));
            }
        }
        if (oper == 1) {// 开始
//            burning(1);
            // 将当前日期设置为最后统计日期
            lastTj = System.currentTimeMillis();
            isBurning = true;
            isPlaying = true;
            BurnInfo.setBurning(true);
            BurnInfo.setPlaying(true);
        } else if (oper == 2) {// 暂停
            mediaPause();
            isPlaying = false;
            BurnInfo.setPlaying(false);
        } else if (oper == 3) {// 结束
            mediaPause();
            isPlaying = false;
            isBurning = false;
            BurnInfo.setPlaying(false);
            BurnInfo.setBurning(false);
            waitTime = 0;
            isWaiting = false;
            stopSelf();
            LogUtil.i("BURN--oper==3!进行了结束的操作");
        } else if (oper == 4) {// 音乐继续
            LogUtil.i("BURN--mp.start()开始播放3333");
            mediaPlay();
            isMiddleStop = false;
        } else if (oper == 5) {// 音乐暂停
            waitTime = 0;
            isWaiting = true;
            mediaPause();
            isMiddleStop = true;
        } else if (oper == 6) {// 从第一个到第二个循环或者第二个到第三个循环
            onTime = BurnInfo.READY_TIME;
            BurnInfo.setOnTime(onTime);
            waitTime = 0;
            isWaiting = true;
            mediaPause();
            isMiddleStop = true;
            BurnInfo.setMiddlePauseOper(false);
            BurnInfo.setMiddleStartOper(false);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void burning(final int type) {
        isBurning = true;
        isPlaying = true;
        BurnInfo.setBurning(true);
        BurnInfo.setPlaying(true);

        // 将当前日期设置为最后统计日期
        lastTj = System.currentTimeMillis();
        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                if (isPlaying) {
                    if (!isWaiting) {
                        // 正在播放并且不是等待时间
                        if (mp == null) {
                            initMp();
                        }
                        if (onTime > BurnInfo.READY_TIME && !mp.isPlaying() && !isMiddleStop) {
                            SoundUtil.setVoise(getApplicationContext(), BurnInfo.getVolume());
                            // 获得音频焦点
                            LogUtil.i("BURN--获得音频焦点...");
                            if (burnAudioManager == null) {//处理一个
                                burnAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
                            }
                            int ret = burnAudioManager.requestAudioFocus(mAudioFocusListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                            if (ret != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                                LogUtil.i("BURN--获得音频焦点失败!" + "request audio focus fail. " + ret);
                            } else {
                                if (mp == null) {
                                    initMp();
                                }
                                LogUtil.i("BURN--mp.start()开始播放5555" + ret);
                                mediaPlay();
                            }
                            if (type == 0) {
                                if (!isWaitingprepare) {
                                    if (mp == null) {
                                        initMp();
                                    }
                                }
                            } else {
                                LogUtil.i("BURN--mp.start()开始播放");
                                mediaPlay();
                            }
                        }
                        onTime = onTime + (System.currentTimeMillis() - lastTj);
                        if (onTime > BurnInfo.READY_TIME) {
                            BurnInfo.setPresTime(BurnInfo.getPresTime() + (System.currentTimeMillis() - lastTj));
                        }
                        LogUtil.i("BURN--正常播放!" + "用来计时和处理播放声音 ___ : " + lastTj + ": +_+ :" + onTime);
                        // 读取煲机信息进行存储
                        if (System.currentTimeMillis() - BurnInfo.getLastPersistenceTime() > 60 * 1000) {// 一分钟持久化一次
//                            persistenceBurn();
                            if (burnSigle != null && burnService != null) {
                                burnSigle.setTotalTime(burnSigle.getTotalTime() + BurnInfo.getPresTime());
                                // 持久化操作
                                burnService.updateBurnSigle(burnSigle);
                                burnService.commitLocal();
                                Intent intent = new Intent();
                                intent.putExtra("time", burnSigle.getTotalTime());
                                intent.putExtra("BurnType", 1);
                                intent.setAction("com.burnSigle.TotalTime");
                                sendBroadcast(intent);
                                LogUtil.i("持久化问题+SoundService持久化之后最终存储的问题" + burnSigle.getTotalTime() / 1000);
                                BurnInfo.setPresTime(0);
                                /*if (!BurnInfo.isBurning()) {
                                    burnService.commitHttp();
                                }*/
                            }
                            BurnInfo.setLastPersistenceTime(System.currentTimeMillis());
                        }

                        lastTj = System.currentTimeMillis();
                        BurnInfo.setOnTime(onTime);
                        BurnInfo.setWaitTime(0);
                    } else {
                        // 正在播放并且处于等待时间
                        waitTime = waitTime + (System.currentTimeMillis() - lastTj);
                        lastTj = System.currentTimeMillis();
                        if (waitTime >= BurnInfo.REST_TIME) {
                            // 中间休息的时间
                            isWaiting = false;
                            // 下面是音乐继续
                            LogUtil.i("BURN--开始播放1111");
                            if (mp == null) {
                                initMp();
                            }
                            mediaPlay();
                            isMiddleStop = false;
                            waitTime = 0;
                        }
                        BurnInfo.setWaitTime(waitTime);
                    }

                }
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 1000);
    }

    /**
     * 初始化MP,完整地初始化
     */
    private void initMp() {

        try {
            AssetFileDescriptor fileDescriptor = getAssets().openFd("pink.mp3");
            mp = new MediaPlayer();
            mp.setDataSource(fileDescriptor.getFileDescriptor(), fileDescriptor.getStartOffset(), fileDescriptor.getLength());
            mp.setOnPreparedListener(preparedListener);
            mp.prepareAsync();
            mp.setLooping(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    OnPreparedListener preparedListener = new OnPreparedListener() {

        @Override
        public void onPrepared(MediaPlayer mp) {
            LogUtil.i("BURN--开始播放2222");
            if (onTime > BurnInfo.READY_TIME)
                mediaPlay();
        }
    };

    private OnAudioFocusChangeListener mAudioFocusListener = new OnAudioFocusChangeListener() {
        public void onAudioFocusChange(int focusChange) {
            switch (focusChange) {
                case AudioManager.AUDIOFOCUS_LOSS:
                    // 可以理解为别的音乐播放器在播放音乐
                    LogUtil.i("数值BURN--音频焦点...AUDIOFOCUS_LOSS");
                    // 会长时间失去，所以告知下面的判断，获得焦点后不要自动播放
                    // 因为会长时间失去，所以直接暂停
                    unusualPause();
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                    LogUtil.i("BURN--AUDIOFOCUS_LOSS_TRANSIENT");
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                    LogUtil.i("BURN--AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                    if (mp != null) {
                        synchronized (SoundService.class) {
                            if (mp.isPlaying()) {
                                // 短暂失去焦点，先暂停。同时将标志位置成重新获得焦点后就开始播放
                                if (mp != null) {
                                    lastIsMusicOn = mp.isPlaying();
                                    mediaPause();
                                    isPausedByTransientLossOfFocus = true;
                                }
                            }
                        }
                    }

                    break;
                case AudioManager.AUDIOFOCUS_GAIN:
                    LogUtil.i("BURN--音频重新获得焦点...AUDIOFOCUS_GAIN");
                    // 重新获得焦点，且符合播放条件，开始播放
                    isPausedByTransientLossOfFocus = false;
                    if (lastIsMusicOn) {
                        burning(1);
                        lastIsMusicOn = false;
                    }
                    break;
            }
        }
    };

    /**
     * 非人为的暂停
     */
    private void unusualPause() {
        mediaPause();
        isPlaying = false;
        BurnInfo.setPlaying(false);
        BurnInfo.setInterrupted(true);
    }

    /**
     * 监听耳机插入拔出等事件
     *
     * @author Administrator
     */
    public class HeadsetPlugReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            LogUtil.i("BURN--蓝牙适配器连接状态发生变化，判断是否为蓝牙耳机掉线...SoundService是否发生变化" + action);
            if ("android.bluetooth.adapter.action.BLE_ACL_DISCONNECTED".equals(action)) {
                if (!BlueUtils.isConnHeadSet()) {
                    LogUtil.i("BURN--蓝牙耳机失去连接，此时暂停煲机...");
                    unusualPause();
                }
            }
            if ("android.bluetooth.adapter.action.BLE_STATE_CHANGED".equals(action)) {
                if (!BlueUtils.isConnHeadSet()) {
                    LogUtil.i("BURN--蓝牙耳机失去连接，此时暂停煲机...");
                    unusualPause();
                }
            }
            if (BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {// 蓝牙适配器连接状态发生变化
                LogUtil.i("BURN--蓝牙适配器连接状态发生变化，判断是否为蓝牙耳机掉线...SoundService");
                if (!BlueUtils.isConnHeadSet()) {
                    LogUtil.i("BURN--蓝牙耳机失去连接，此时暂停煲机...");
                    unusualPause();
                }
                if (BlueUtils.isConnHeadSet()) {
                    BurnInfo.setInterrupted(false);
                }
            } else if ("android.intent.action.HEADSET_PLUG".equals(action) && !BlueUtils.isConnHeadSet()) {// 有插线的耳机
                if (intent.hasExtra("state")) {
                    if (intent.getIntExtra("state", 0) == 0) {
                        LogUtil.i("BURN--有线耳机断开连接,此时暂停煲机...Soundservice");
                        unusualPause();
                    } else if (intent.getIntExtra("state", 0) == 1) {
                        // LogUtil.i("BURN--有线耳机已连接...");
                        BurnInfo.setInterrupted(false);
                    }
                }
            }
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // 在我看来有两步，比较厉害的操作，timer的关闭，和广播的取消
        if (timer != null) {
            timer.cancel();
        }
        isPlaying = false;
        isBurning = false;
        BurnInfo.setPlaying(false);
        BurnInfo.setBurning(false);
        if (mp != null) {
            mediaPause();
            mp.release();
        }
        mp = null;
        if (burnAudioManager != null)
            burnAudioManager.abandonAudioFocus(mAudioFocusListener);
        burnAudioManager = null;
        stopSelf();
        unregisterReceiver(headsetPlugReceiver);
        headsetPlugReceiver = null;
        System.gc();
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void mediaPlay() {
        if (mp != null) {
           /* if (FiilManager.getInstance().getDeviceInfo().getEarType() == Config.FIIL_DRIIFTER_PRO || FiilManager.getInstance().getDeviceInfo().getEarType() == Config.FIIL_CARAT_PRO) {
                FiilManager.getInstance().setBurnInStatus(Config.START_BURN, null);
            }*/
            mp.start();
        }
    }

    private void mediaPause() {
        if (mp != null && mp.isPlaying()) {
           /* if (FiilManager.getInstance().getDeviceInfo().getEarType() == Config.FIIL_DRIIFTER_PRO) {
                FiilManager.getInstance().setBurnInStatus(Config.STOP_BURN, null);
            }*/
            mp.pause();
        }
    }

    private void mediaStop() {
    }

}
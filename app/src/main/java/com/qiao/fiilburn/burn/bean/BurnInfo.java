package com.qiao.fiilburn.burn.bean;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.utils.LogUtil;

import java.util.Date;

public class BurnInfo {
    private static String id;// 煲机的id，唯一标识符
    private static boolean isBurning;// 正在煲机(此次煲机任务未结束，包括暂停)
    private static boolean isPlaying;// 正在煲机（包含无声音休息部分，不包括暂停）
    private static boolean isInterrupted;// 被打断的
    private static Date makeDate;// 创建日期
    private static Date startDate;// 开始日期
    private static long onTime = 0;// 播放时长
    private static long waitTime = 0;// 等待的时长
    private static Date pauseDate;// 暂停时间
    private static int volume;// 音量
    //正式环境，需要的数据，
    public static final long REST_TIME = 5 * 60 * 1000;// 中途休息时长
    public static final long ALL_TIME = 30 * REST_TIME;// 总时长
    public static final long MIDDLE_TIME = 15 * REST_TIME;// 半途时长,就是半途开始停止的时间，到什么时候开始停止
//    测试短时间需要的数据
//    public static final long REST_TIME = 10 * 1000;// 中途休息时长
//    public static final long ALL_TIME = 2 * REST_TIME;// 总时长
//    public static final long MIDDLE_TIME = 1 * REST_TIME;// 半途时长,就是半途开始停止的时间，到什么时候开始停止

    public static boolean isMiddlePauseOper = false;// 中途暂停音乐
    public static boolean isMiddleStartOper = false;// 中途重新开启音乐
    private static long lastPersistenceTime = System.currentTimeMillis();// 最后一次持久化日期
    private static long presTime = 0;// 持久化后又播放的时长
    public static final long READY_TIME = 15 * 1000;// 前面的准备时间
    public static int burnNumber = 1;// 煲机时候执行的次数,1或者3
    public static int burnNowNumber = 1;// 煲机时候正在执行的次数，这是第几次循环

    public static void init() {
        id = "";
        isMiddlePauseOper = false;
        isMiddleStartOper = false;
        isPlaying = false;
        isBurning = false;
        makeDate = null;
        startDate = null;
        onTime = 0;
        waitTime = 0;
        presTime = 0;
        lastPersistenceTime = System.currentTimeMillis();
        isInterrupted = false;
    }

    public static boolean isMiddlePauseOper() {
        return isMiddlePauseOper;
    }

    public static void setMiddlePauseOper(boolean isMiddlePauseOper) {
        BurnInfo.isMiddlePauseOper = isMiddlePauseOper;
    }

    public static boolean isInterrupted() {
        return isInterrupted;
    }

    public static void setInterrupted(boolean isInterrupted) {
        BurnInfo.isInterrupted = isInterrupted;
    }

    /**
     * 获取煲机进度百分比
     *
     * @return
     */
    public static int getPercent() {
        return (int) ((BurnInfo.getRealOnTime() * 360) / ALL_TIME);
    }

    /**
     * 获取下次间歇时间
     *
     * @return
     */
    public static String getRestTime() {
        return trans(MIDDLE_TIME - onTime);
    }

    /**
     * 获取结束时间
     *
     * @return
     */
    public static String getEndTime() {
        return trans(ALL_TIME - getRealOnTime());
    }

    /**
     * 获取真实煲机时长
     *
     * @return
     */
    public static long getRealOnTime() {
        if (onTime < READY_TIME) {
            return 0;
        }
        // 真是傻啊
        if ((onTime - READY_TIME) >= ALL_TIME) {
            return ALL_TIME;
        }
        return onTime - READY_TIME;
    }

    /**
     * 获取煲机时长00：00格式
     */
    public static String getBurnTime() {
        if (onTime < READY_TIME) {
            return trans(0);
        }
        return trans(onTime - READY_TIME);
    }

    /**
     * 获取准备时剩余时长
     *
     * @return
     */
    public static long getReadySurplus() {
        if (onTime < READY_TIME) {
            return READY_TIME - onTime;
        }
        return 0;
    }

    /**
     * 获取暂停时剩余时长,返回的是字符串
     *
     * @return
     */
    public static String getPauseSurplus() {
        if (waitTime != 0) {
            // 返回
            return trans(REST_TIME - waitTime);
        }

        return trans(0);
    }

    /**
     * 获取暂停时剩余时长,返回的是字符串
     *
     * @return
     */
    public static long getPauseSurLong() {
        if (waitTime != 0) {
            // 返回
            return REST_TIME - waitTime;
        }

        return 0;
    }

    /**
     * 将毫秒转换为分：秒
     *
     * @return
     */
    private static String trans(long millisecond) {
        if (millisecond <= 0) {
            return "00:00";
        }
        int min = (int) ((millisecond / 1000) / 60);
        int second = (int) ((millisecond / 1000) % 60);

        return (min < 10 ? ("0" + min) : min) + ":"
                + (second < 10 ? ("0" + second) : second);
    }

    public static boolean isPlaying() {
        return isPlaying;
    }

    public static void setPlaying(boolean isPlaying) {
        BurnInfo.isPlaying = isPlaying;
//        EventBus.getDefault().post(new BlueServerNoticeCommand(26, 1));

    }

    public static Date getMakeDate() {
        return makeDate;
    }

    public static void setMakeDate(Date makeDate) {
        BurnInfo.makeDate = makeDate;
    }

    public static Date getStartDate() {
        return startDate;
    }

    public static void setStartDate(Date startDate) {
        BurnInfo.startDate = startDate;
    }

    public static long getOnTime() {
        return onTime;
    }

    public static void setOnTime(long onTime) {
        BurnInfo.onTime = onTime;
    }

    public static long getWaitTime() {
        return waitTime;
    }

    public static void setWaitTime(long waitTime) {
        BurnInfo.waitTime = waitTime;
    }

    public static Date getPauseDate() {
        return pauseDate;
    }

    public static void setPauseDate(Date pauseDate) {
        BurnInfo.pauseDate = pauseDate;
    }

    public static boolean isMiddleStartOper() {
        return isMiddleStartOper;
    }

    public static void setMiddleStartOper(boolean isMiddleStartOper) {
        BurnInfo.isMiddleStartOper = isMiddleStartOper;
    }

    public static long getLastPersistenceTime() {
        return lastPersistenceTime;
    }

    public static void setLastPersistenceTime(long lastPersistenceTime) {
        BurnInfo.lastPersistenceTime = lastPersistenceTime;
    }

    public static long getPresTime() {
        return presTime;
    }

    public static void setPresTime(long presTime) {
        BurnInfo.presTime = presTime;
    }

    public static boolean isBurning() {
        return isBurning;
    }

    public static void setBurning(boolean isBurning) {
        BurnInfo.isBurning = isBurning;
    }

    public static String getId() {
        return id;
    }

    public static void setId(String id) {
        BurnInfo.id = id;
    }

    public static int getVolume() {
        return volume;
    }

    public static void setVolume(int volume) {
        BurnInfo.volume = volume;
    }

    public static int getBurnNumber() {
        return burnNumber;
    }

    public static void setBurnNumber(int burnNumber) {
        BurnInfo.burnNumber = burnNumber;
    }

    public static int getBurnNowNumber() {
        return burnNowNumber;
    }

    public static void setBurnNowNumber(int burnNowNumber) {
        BurnInfo.burnNowNumber = burnNowNumber;
    }

}

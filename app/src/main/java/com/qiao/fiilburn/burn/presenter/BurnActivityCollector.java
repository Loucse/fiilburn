package com.qiao.fiilburn.burn.presenter;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

/**
 * 处理煲耳机进行完毕后不会，回收的情况。就会为了页面的销毁,就是为了处理煲耳机的页面。
 *
 * @author Administrator
 */
public class BurnActivityCollector {

    public static List<Activity> activities = new ArrayList<Activity>();

    public static void addActivity(Activity activity) {
        activities.add(activity);
    }

    public static void removeActivity(Activity activity) {
        activities.remove(activity);
    }

    public static void finishAll() {
        for (Activity activity : activities) {
            if (activity != null) {
                if (!activity.isFinishing()) {
                    activity.finish();
                }
            }
        }
    }

    /**
     * 得到存活的Activity的个数
     */
    public static int getActivitySize() {
        return activities.size();
    }

}

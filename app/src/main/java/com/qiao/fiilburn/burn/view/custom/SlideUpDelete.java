package com.qiao.fiilburn.burn.view.custom;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Scroller;

/**
 * 上滑删除
 * Created by hyb on 2016/3/29.
 */
public class SlideUpDelete extends FrameLayout {
    private View deleteView;
    private View contentView;
    private int viewWidth;
    private int contentViewHeight;
    private int deleteViewHeight;
    private Scroller scroller;

    private boolean isOpen  = false;
    public SlideUpDelete(Context context, AttributeSet attrs) {
        super(context, attrs);
        dragHelper = ViewDragHelper.create(this, callback);
        scroller = new Scroller(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        deleteView = getChildAt(0);
        contentView = getChildAt(1);
//        View view = (View) contentView.getParent();
        viewWidth = contentView.getMeasuredWidth();
        contentViewHeight = contentView.getMeasuredHeight();
        deleteViewHeight = deleteView.getMeasuredHeight();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
//        deleteView.layout(0,contentViewHeight-deleteViewHeight,viewWidth,contentViewHeight);
        deleteView.layout(0,contentViewHeight,viewWidth,contentViewHeight+deleteViewHeight);
    }

    private ViewDragHelper dragHelper;
    private ViewDragHelper.Callback callback = new ViewDragHelper.Callback() {

        //判断child是否是需要拖拽的子View, down时调用
        @Override
        public boolean tryCaptureView(View child, int pointerId) {

            return child == contentView || child == deleteView;
        }

        //限制view在水平方向真正移动的距离, move时调用
//        public int clampViewPositionHorizontal(View child, int left, int dx) {
//            Log.i("TAG", "clampViewPositionHorizontal()");
//            //限制contentView的移动范围
//            if(child == contentView){
//                if(left > 0){
//                    left = 0;
//                }else if(left < -menuWidth){
//                    left = -menuWidth;
//                }
//            }else{//限制menu的移动范围
//                if(left > contentWidth){
//                    left = contentWidth;
//                }else if(left <contentWidth-menuWidth){
//                    left = contentWidth-menuWidth;
//                }
//            }
//            return left;
//        };

        @Override
        public int clampViewPositionVertical(View child, int top, int dy) {
            if(child == contentView){
                if(top>0){
                    top =0;
                }else if(top<=- deleteViewHeight){
                    top = -deleteViewHeight;
                }
            }else{
                if(top>contentViewHeight){
                    top = contentViewHeight;
                }else if(top <contentViewHeight - deleteViewHeight){
                    top = contentViewHeight - deleteViewHeight;
                }
            }
            return top;
        }

        //当子view的位置发生改变时回调, 需要在其中去移动其它的子 View
        public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
//            Log.i("TAG", "onViewPositionChanged()");

            if(changedView == contentView){ //移动contentView,我们需要移动menuView
                deleteView.layout(0, deleteView.getTop()+dy,viewWidth , deleteView.getBottom()+dy);
//                deleteView.layout(0,contentViewHeight-deleteViewHeight,viewWidth,contentViewHeight);
            }else if(changedView == deleteView){//移动contentView,我们需要移动menuView
                contentView.layout(0, contentView.getTop()+dy, viewWidth, contentView.getBottom()+dy);
            }
        };

        //当子View被释放时回调, 即up时调用 int
        public void onViewReleased(View releasedChild, float xvel, float yvel) {
//            Log.i("TAG", "onViewReleased()");
            if(contentView.getTop() < -deleteViewHeight/2){
                //开启
                opean();
            }else{
                //关闭
                close();
            }
        };

        //返回子View在水平方向最大移到的距离
//        public int getViewHorizontalDragRange(View child) {
//            Log.i("TAG", "getViewHorizontalDragRange()");
//
//            return menuWidth;
//        };

        @Override
        public int getViewVerticalDragRange(View child) {
            return deleteViewHeight;
        }
    };
    /* 5. 如何解决手指滑动时ListView垂直方向滚动和SwipeView水平滑动的冲突问题?
     * 在斜45度之内都可以滑动出delete
    1). 在move事件时, 获取x轴和y轴方向的移动距离distanceX, distanceY
    2). 如果distanceX>distanceY, 执行requestDisallowInterceptTouchEvent(true), 使ListView不能拦截event*/
    private int fistX;
    private int fistY;
    private boolean canOpean = true; //标识是否能打开
    public boolean onTouchEvent(android.view.MotionEvent event) {
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:

                fistX = (int) event.getRawX();
                fistY = (int) event.getRawY();

                break;
            case MotionEvent.ACTION_MOVE:
                int moveX = (int) event.getRawX();
                int moveY = (int) event.getRawY();
                int distanceX = Math.abs(moveX - fistX);
                int distanceY = Math.abs(moveY - fistY);

                if(distanceX<distanceY){//用户想垂直滑动
                    //使listView不能拦截event
                    getParent().requestDisallowInterceptTouchEvent(true);
                }
                fistX = (int) event.getRawX();
                fistY = (int) event.getRawY();
                break;
        }

        //重写onTouchEvent(), 在其中让dragHelper对象来处理event对象
        if(canOpean){//只有标识了能打开才交给dragHelper处理
            isDrawBackNull();
            dragHelper.processTouchEvent(event);
        }

        return true;
    }

    //关闭滑动
    public void close() {
//		contentView.layout(0, 0, contentWidth, viewHeight);
//		menuView.layout(contentWidth, 0, contentWidth+menuWidth, viewHeight);
        //平滑关闭
        isDrawBackNull();
        if(dragHelper == null){
            return;
        }
        if(contentView == null){
            contentView = getChildAt(1);
        }
        if(contentView == null){
            return;
        }
        dragHelper.smoothSlideViewTo(contentView, 0, 0);
        ViewCompat.postInvalidateOnAnimation(this);
        if(onStateChangeListener != null){
            onStateChangeListener.onClose(this);
        }
        isOpen = false;
    }

    //开启滑动
    public void opean() {
//		contentView.layout(-menuWidth, 0, contentWidth-menuWidth, viewHeight);
//		menuView.layout(contentWidth-menuWidth, 0, contentWidth, viewHeight);
        //平滑打开
        isDrawBackNull();
        if(dragHelper == null){
            return;
        }
        dragHelper.smoothSlideViewTo(contentView, 0, -deleteViewHeight);
        ViewCompat.postInvalidateOnAnimation(this);
        if(onStateChangeListener != null){
            onStateChangeListener.onOpean(this);
        }
        isOpen = true;
    };



    @Override
    public void computeScroll() {
        super.computeScroll();
        isDrawBackNull();
        if(dragHelper == null){
            return;
        }
        if(dragHelper.continueSettling(true)){
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

	/*6. 如何解决SwipeView滑动与ContentView/DeleteView点击事件的冲突问题?
	1). 重写onInterceptTouchEvent()
	2). 返回dragHelper.shouldInterceptTouchEvent(ev), 由dragHelper来决定是否拦截event*/

    /**
     * 拦截事件
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        //处理事件
        if(ev.getAction()==MotionEvent.ACTION_DOWN) {
            //处理事件
            if(onStateChangeListener!=null) {
                canOpean = onStateChangeListener.onDown(this);
            }
        }

        boolean interceptTouchEvent = dragHelper.shouldInterceptTouchEvent(ev);
        return interceptTouchEvent;
    }

    /*7. 如何只让ListView中只有一个SwipeView能被打开?
        1). 为SwipeView定义枚举类型状态类
        2). 在SwipeView中定义监听接口,在接口中定义回调方法
        3). 定义设置监听接口对象的方法
        4). 在事件发生时, 调用监听对象处理
        5). 在Activity的Adapter中为每个SwipeView对象设置监听器对象, 在回调方法中做处理*/
    public interface OnStateChangeListener{
        public void onOpean(SlideUpDelete swapView);
        public void onClose(SlideUpDelete swapView);

        public boolean onDown(SlideUpDelete swapView);
    }

    private OnStateChangeListener onStateChangeListener;
    public void setOnStateChangeListener(OnStateChangeListener onStateChangeListener) {
        this.onStateChangeListener = onStateChangeListener;
    }

    public void deleteView(){
        isDrawBackNull();
        dragHelper.smoothSlideViewTo(contentView, deleteViewHeight, -deleteViewHeight-contentViewHeight);
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    private boolean isDrawBackNull(){
        if(dragHelper == null){
            dragHelper = ViewDragHelper.create(this, callback);
        }
        return true;
    }

    //    @Override
//    public void computeScroll() {
//        //先判断mScroller滚动是否完成
//        if (scroller.computeScrollOffset()) {
//            //这里调用View的scrollTo()完成实际的滚动
//            scrollTo(scroller.getCurrX(), scroller.getCurrY());
//            //必须调用该方法，否则不一定能看到滚动效果
//            invalidate();
//        }
//    }
//
//    private int downX;
//    private int downY;
//    private int lastY;
//    private int dy;
//    private int dx;
//
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        int moveX = (int) ev.getX();
//        int moveY = (int) ev.getY();
//        switch (ev.getAction()){
//            case MotionEvent.ACTION_DOWN:
//                downX = moveX;
//                downY = moveY;
//                scrollTo(0,-lastY);
//                break;
//            case MotionEvent.ACTION_MOVE:
//                dx = moveX - downX;
//                dy = moveY - downY;
//                if(Math.abs(dy)> Math.abs(dx)&& Math.abs(dy)>10){
//                    getParent().requestDisallowInterceptTouchEvent(true);
//                    if(dy>0){
//                        dy = 0;
//                    }else if(dy< - deleteViewHeight){
//                        dy = - deleteViewHeight;
//                    }
//                    scrollTo(0,-dy);
//                    return true;
//                }
//                break;
//            case MotionEvent.ACTION_UP:
//                if(Math.abs(dy)> Math.abs(dx)&& Math.abs(dy)>10) {
//                    if (dy < -deleteViewHeight / 2) {
//
//                    } else {
//
//                    }
//                    return true;
//                }
//                lastY = dy;
//                break;
//        }
//        return super.dispatchTouchEvent(ev);
//    }
}

package com.qiao.fiilburn.burn.secvicer;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.burn.bean.BurnSingle;
import com.qiao.fiilburn.burn.receiver.AlarmReceiver;
import com.qiao.fiilburn.burn.view.act.MainActivity;
import com.qiao.fiilburn.utils.JsonUtils;
import com.qiao.fiilburn.utils.LogUtil;
import com.qiao.fiilburn.utils.Md5Util;
import com.qiao.fiilburn.utils.SPUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class BurnService extends Service {


    private static BurnService burnSer;

    private Context context;
    private String burnStr;// 本地存储字符串,两个地方用到它，进入初始化，和保存本地
    private static Map<String, BurnSingle> burnMap = new TreeMap<String, BurnSingle>();// 存储煲机信息的map

    public BurnService() {
        super();
    }

    public static BurnService getInstance() {
        if (burnSer == null) {
            synchronized (BurnService.class) {
                if (burnSer == null) {
                    burnSer = new BurnService();
                }
            }
        }
        return burnSer;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void setContext(Context context) {
        this.context = context;
        //初始化
        if (context != null) {
            initBurn();
        }
    }

    private void initBurn() {
        burnStr = SPUtils.getString(context, Constant.BURN);
        if (burnStr == null || "".equals(burnStr.trim())) {
            LogUtil.e("这是 initBurn() if ");
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject(burnStr);
            Iterator<?> it = jsonObject.keys();
            while (it.hasNext()) {
                String key = it.next().toString();
                String value = jsonObject.getString(key);
                //将Value转化成BurnString
                BurnSingle bs = new Gson().fromJson(value, BurnSingle.class);
                burnMap.put(bs.getId(), bs);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            LogUtil.e(" 我的天那  json报错了  这个是BurnServer的哦 ____ " + e.getMessage());
        } catch (Exception e) {
            LogUtil.e("json转换:", e);
        }
    }

    /**
     * 获取煲机信息列表
     *
     * @return
     */

    public List<BurnSingle> getBurnList() {
        List<BurnSingle> list = new ArrayList<BurnSingle>();
        for (Map.Entry<String, BurnSingle> entry : burnMap.entrySet()) {
            list.add(entry.getValue().clone());
        }
        return list;
    }


    /**
     * 根据id获取BurnSigle
     *
     * @param id
     * @return
     */
    public BurnSingle getBurnSigle(String id) {
        if (burnMap != null) {
            BurnSingle burnSigle = burnMap.get(id);
            if (burnSigle != null)
                return burnSigle;
            else
                return null;
        } else {
            return null;
        }
    }

    /**
     * 删除某个煲机方案BurnSigle
     *
     * @param id
     */
    public void delBurnSigle(String id) {
        // 获取burnSigle，删除定时提醒
        BurnSingle bs = burnMap.get(id);
        if (bs != null) {
            String ids = bs.getTimingids();
            LogUtil.i("定时++删除时候有没有值ids" + ids);
            if (ids != null && !"".equals(ids.trim())) {
                // ids不为空才让他进入。。
                String[] idArr = ids.split(",");
                LogUtil.i("定时++删除时候有没有值ids长度的问题" + idArr.length);
                for (int i = 0; i <= idArr.length - 1; i++) {
                    cancleRemind(Integer.parseInt(idArr[i]));
                }
            }
            burnMap.remove(id);
        }
    }

    /**
     * 删除所有的煲机方案
     */
    public void delAllBurnSigle() {

        Iterator<Map.Entry<String, BurnSingle>> iterator = burnMap.entrySet()
                .iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, BurnSingle> entry = iterator.next();
            String id = entry.getKey();
            BurnSingle bs = entry.getValue();
            if (bs != null) {
                String ids = bs.getTimingids();
                if (ids != null && !"".equals(ids.trim())) {
                    String[] idArr = ids.split(",");
                    for (int i = 0; i <= idArr.length - 1; i++) {
                        LogUtil.i("定时+在退出的时候删除煲机方案" + Integer.parseInt(idArr[i]));
                        cancleRemind(Integer.parseInt(idArr[i]));
                    }
                }
            }
        }
        burnMap.clear();
    }

    /**
     * 更新BurnSigle
     *
     * @param bs
     */
    public void updateBurnSigle(BurnSingle bs) {
        BurnSingle bsOld = burnMap.get(bs.getId());
        try {
            // 判断是否修改了定时
            LogUtil.i("定时+新的方案+" + bs.getTiming() + "+旧的方案+" + bsOld.getTiming());
        } catch (Exception e) {
            e.getStackTrace();
        }
        if (bsOld != null) {
            if (!bs.getTiming().equals(bsOld.getTiming())) {
                LogUtil.i("定时+新的方案和旧的方案不同进行删除操作");
                // 删除所有定时提醒
                String ids = bsOld.getTimingids();
                if (ids != null && !"".equals(ids.trim())) {
                    String[] idArr = ids.split(",");
                    for (int i = 0; i <= idArr.length - 1; i++) {
                        cancleRemind(Integer.parseInt(idArr[i]));
                    }
                }

                if (bs.getTiming() == null || "".equals(bs.getTiming())) {
                    bs.setTimingids("");
                }

                // 重新创建定时提醒
                addRemind(bs);
            }
            burnMap.put(bs.getId(), bs);
        }

    }

    /**
     * 更新BurnSigle,专门用于时间修改时候的使用
     *
     * @param bs
     */
    public void updateBurnSigleForTiming(BurnSingle bs, String oldTime, String oldTimeIds) {

        // 判断是否修改了定时
        LogUtil.i("定时+新的方案+" + bs.getTiming() + "+旧的方案+" + oldTime);
        if (!bs.getTiming().equals(oldTime)) {
            LogUtil.i("定时+新的方案和旧的方案不同进行删除操作");
            // 删除所有定时提醒
            String ids = oldTimeIds;
            if (ids != null && !"".equals(ids.trim())) {
                String[] idArr = ids.split(",");
                for (int i = 0; i <= idArr.length - 1; i++) {
                    cancleRemind(Integer.parseInt(idArr[i]));
                }
            }
            if (bs.getTiming() == null || "".equals(bs.getTiming())) {
                bs.setTimingids("");
            }
            // 重新创建定时提醒
            addRemind(bs);
        }

        burnMap.put(bs.getId(), bs);
    }

    /**
     * 更新BurnSigle,专门用于开关的开启和关闭的改变
     *
     * @param bs
     */
    public void updateBurnSigleOpen(BurnSingle bs, BurnSingle bsOld, Boolean isOpen) {

        // 判断是否修改了定时
        LogUtil.i("定时+新的方案+" + bs.getTiming() + "+旧的方案+" + bsOld.getTiming());
        if (isOpen) {
            if (bs.getTiming() == null || "".equals(bs.getTiming())) {
                bs.setTimingids("");
            }
            // 重新创建定时提醒
            addRemind(bs);
        } else {
            LogUtil.i("定时+新的方案和旧的方案不同进行删除操作");
            // 删除所有定时提醒
            String ids = bsOld.getTimingids();
            if (ids != null && !"".equals(ids.trim())) {
                String[] idArr = ids.split(",");
                for (int i = 0; i <= idArr.length - 1; i++) {
                    LogUtil.i("定时+新的方案和旧的方案不同进行删除操作" + Integer.parseInt(idArr[i]));
                    cancleRemind(Integer.parseInt(idArr[i]));
                }

            }

        }

        burnMap.put(bs.getId(), bs);
    }

    /**
     * 添加BurnSigle
     *
     * @param bs
     */
    public void addBurnSigle(BurnSingle bs) {
        burnMap.put(bs.getId(), bs.clone());
    }

    /**
     * 持久化操作
     */
    public void commitLocal() {
        if (context != null) {
            /*
             * 有点意思下面的步骤老有才了，他会调用burnSingle哦里面得getFirstRemind（）方法，所以里面的logutil就得到了调用
             * ，这既是在打log的时候回答出特别的多的原因
             */
//            this.burnStr = new Gson().toJson(burnMap);
            this.burnStr = new GsonBuilder().enableComplexMapKeySerialization().create().toJson(burnMap);
            LogUtil.i("持久化+持久化的最后操作+就是最后存储在纸上的步骤" + burnStr);
            SPUtils.setString(context, Constant.BURN, burnStr);
        }
    }

    /**
     * 将煲机进度上传http服务器，也可以理解为持久化到服务器
     */
   /* public void commitHttp() {
        if (context != null) {
            LogUtil.i("持久化+持久化到网络Internet的最后操作");
            ((MainActivity) context).uploadBurnProcess();
        }
    }*/

    /**
     * 将服务器的煲机进度和本地的保进度合并
     *
     * @param httpBurn
     */
    /*public void mergeBurn(String httpBurn) {
        // 读取httpBurn,进行json解析
        try {
            JSONObject obj = new JSONObject(httpBurn);
            Iterator<?> it = obj.keys();
            int logNumber = 0;
            while (it.hasNext()) {// 遍历JSONObject
                String key = it.next().toString();
                String value = obj.getString(key);
                // 将value转为BurnSigle
//                BurnSigle bs = JSON.parseObject(value, BurnSigle.class);
                BurnSingle bs = JsonUtils.getInstance().getBurnSigle(value);
                // 修改代码，处理第一次进入的定时。蓝牙方案也是在这里丢失的。
                addRemindFirst(bs);
                //TODO 判断本地是否已经有该进度
                if (burnMap.get(key) == null) {
                    // 如果本地没有，合并
                    if (!"".equals(bs.getBid())) {
                        // LogUtil.i("数值+有没有进入。合并的问题，蓝牙方案合并的问题");
                        List<BurnSingle> burnList = getBurnList();
                        for (BurnSingle b : burnList) {
                            if (bs.getBid().equals(b.getBid())) {
                                // 如果本地没有并且是蓝牙方案，下载下来的方案和本地的方案相加
                                bs.setTotalTime(bs.getTotalTime() + b.getTotalTime());
                                burnMap.remove(b.getId());
                                break;
                            }
                        }
                    }
                    burnMap.put(bs.getId(), bs);
                } else {
                    // 此时本地同样有进度，此时进行比较，比较规则为 取totalTime大的那个
                    BurnSingle old = burnMap.get(key);
                    if (bs.getTotalTime() > old.getTotalTime()) {
                        // 如果线上的比本地的大，我们就让线上的代替本地的。
                        burnMap.remove(old.getId());
                        burnMap.put(bs.getId(), bs);
                    }
                }
                LogUtil.i("butnFragment _ viewHolder _ datas _ .tostring " + burnMap.toString());
                burnSer.commitLocal();
                logNumber++;
            }
            Constan.setLogNumber(logNumber);

        } catch (JSONException e) {
            LogUtil.e("json转换:", e);
            LogUtil.i("同步++JSONException++" + e.toString());
        } catch (Exception e) {
            LogUtil.e("json转换:", e);
            LogUtil.i("同步++Exception++" + e.toString());
        }
    }
*/

    /**
     * 添加提醒
     *
     * @param bs
     */
    public void addRemind(BurnSingle bs) {
        LogUtil.i("定时+重新创建定时器");
        // 获取提醒信息
        List<Calendar> list = bs.getFirstRemind();
        if (list == null || list.size() == 0) {
            return;
        }
        String ids = "";
        for (Calendar cal : list) {
            // LogUtil.i("定时+新的定时任务cal值是+" + cal);
            // 生成一个随机数用于存储这个Intent，并且在删除时作为一种标记
            int idFlag = (int) (Math.random() * 1000000) * 10 + cal.get(Calendar.DAY_OF_WEEK);
            LogUtil.i("定时+新的定时任务+" + idFlag);
            /***********************************************************/
            Intent intent = new Intent(context, AlarmReceiver.class);
            // 网上查询的方法
//            Intent intent = new Intent("FENGFAN_CLOCK");
            if (android.os.Build.VERSION.SDK_INT >= 12) {
                intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);// 3.1以后的版本需要设置Intent.FLAG_INCLUDE_STOPPED_PACKAGES
            }
            intent.putExtra(Constant.BURN_ID, bs.getId());
            intent.putExtra(Constant.BURN_SNAME, bs.getSname());
            intent.putExtra(Constant.BURN_PERCNET, bs.gFinishPercent());
            intent.putExtra(Constant.EAR_TYPE, bs.getBurntype());
            PendingIntent sender = null;
            try {
                sender = PendingIntent.getBroadcast(context, idFlag, intent, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (sender != null) {
                long firstTime = SystemClock.elapsedRealtime(); // 开机之后到现在的运行时间(包括睡眠时间)
                long systemTime = System.currentTimeMillis();// 当前系统时间
                long selectTime = cal.getTimeInMillis(); // 选择的每天定时时间
                LogUtil.i("定时++开机之后firstTime*****" + firstTime + "*****当前系统时间systemTime****" + systemTime + "****定时selectTime****" + selectTime);
                // 计算现在时间到设定时间的时间差
                long time = selectTime - systemTime;
                firstTime += time;
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy年-MM月dd日-HH时mm分ss秒");
                Date date = new Date(systemTime);
                Date date2 = new Date(selectTime);
                System.out.println(formatter.format(date));
                LogUtil.i("定时++开机之后firstTime" + firstTime + "+此时systemTime+" + date + "+定时selectTime+" + date2);
                // 进行闹铃注册
                AlarmManager manager = (AlarmManager) context.getSystemService(Activity.ALARM_SERVICE);
                manager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime, 7L * 24 * 60 * 60 * 1000, sender);
                /***********************************************************/
                ids = ids + idFlag + ",";
            }
        }

        bs.setTimingids(ids);
    }

    /**
     * 取消定时提醒
     *
     * @param requestCode
     */
    public void cancleRemind(int requestCode) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        // 取消闹铃
        AlarmManager am = (AlarmManager) context.getSystemService(Activity.ALARM_SERVICE);
        am.cancel(sender);
    }

    /**
     * 添加提醒，好像不是很好用，有点意思。
     *
     * @param bs
     */
   /* public void addRemindFirst(BurnSingle bs) {
        LogUtil.i("同步+addRemindFirst进行");
        // 获取提醒信息
        LogUtil.i("定时++addRemindFirst+今天周几 + nowXq + 定时是周几+ + idx");
        List<Calendar> list = bs.getFirstRemind();
        if (list == null || list.size() == 0) {
            bs.setTimingids("");
        } else {
            String ids = "";
            for (Calendar cal : list) {
                int id = (int) (Math.random() * 1000000) * 10 + cal.get(Calendar.DAY_OF_WEEK);
                ids = ids + id + ",";
            }
            LogUtil.i("定时+进入app同步的问题，但是并没有进行++" + ids);
            bs.setTimingids(ids);
        }
        if (bs.getRemainingTime() == 3000) {
            bs.setRemainingTime(180000000);
            long totalTime = bs.getTotalTime();
            bs.setTotalTime(totalTime * 60 * 1000);
        }
        if (bs.getBurntype() == 0) {//可以确定是原来的方案。
            if (bs.getSoundLevel() == 2) {//小耳机
                bs.setBurntype(3);
            } else {
                if (bs.getBid().equals("") && bs.getBid().length() < 2) {
                    bs.setBurntype(1);
                } else {
                    bs.setBurntype(2);
                }
            }
        }
        // 持久化操作
        // burnSer.updateBurnSigle(bs);
        // 保存本地有问题
        // burnSer.commitLocal();
    }*/

    /**
     * 处理是否有相同的方案,只需要穿蓝牙地址，就可以处理。
     * 有相同蓝牙地址就返回true
     */
    /*public boolean dealBurnSing(String addressId) {
        boolean isHave = false;
        List<BurnSingle> burnList = burnSer.getBurnList();
        // 比较一下这个蓝牙地址有没有在已经配对的列表中
        for (BurnSingle bs : burnList) {
            if (bs.getBid().equals(Md5Util.getMD5Str(addressId))) {
                isHave = true;
//                return isHave;
            }
        }
        return isHave;
    }*/
    public static void clean() {
        burnSer = null;
    }
}

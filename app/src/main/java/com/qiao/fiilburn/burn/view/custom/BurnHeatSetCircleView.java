package com.qiao.fiilburn.burn.view.custom;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.utils.DensityUtil;


/**
 * 煲耳机界面进度圆圈
 * Created by hyb on 2016/3/30.
 */
public class BurnHeatSetCircleView extends View {
    private int progress = 40;
    private String burnTime = "0";  //已经爆机时间
    private int max = 100;
    private String totalTime = "总时间3000分钟";

    private int width;

    private int bottomColor = Color.parseColor("#e9e9e9");
    private int progressColor;
    private int progressTextColor = Color.parseColor("#888888");
    private int burnTotalTimeColor = Color.parseColor("#888888");

    private int paintWidth;   //笔的宽度
    private int progressTextWidth;
    private int presentTextWidth;
    private int burnTimeTextWidth;

    private int progressTextSize;
    private int presentTextSize;
    private int burnTextSize;

    private Resources resources;

    private Paint paint;

    public BurnHeatSetCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        paint.setAntiAlias(true);
        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircleView, 0, 0);
        progressColor = array.getColor(R.styleable.CircleView_progressColor, Color.parseColor("#069adc"));
        array.recycle();
        paintWidth = DensityUtil.dip2px(context, 7);
        progressTextWidth = DensityUtil.dip2px(context, 40);
        presentTextWidth = DensityUtil.dip2px(context, 20);
        burnTimeTextWidth = DensityUtil.dip2px(context, 16);
        progressTextSize = DensityUtil.dip2px(context, 38);
        presentTextSize = DensityUtil.dip2px(context, 15);
        burnTextSize = DensityUtil.dip2px(context, 15);
        resources = context.getResources();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = getMeasuredWidth();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //画底原
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(paintWidth);
        paint.setColor(bottomColor);
        canvas.drawCircle(width / 2, width / 2, width / 2 - paintWidth / 2, paint);

        //画进度
        paint.setColor(progressColor);
        RectF rectF = new RectF(paintWidth / 2, paintWidth / 2, width - paintWidth / 2, width - paintWidth / 2);
        canvas.drawArc(rectF, -90, progress * 360 / max, false, paint);

        //画进度文本
        Rect bounds = new Rect();
//        paint.setFakeBoldText(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(progressTextColor);
        paint.setStrokeWidth(progressTextWidth);
        paint.setTextSize(progressTextSize);
        String text = null;
        text = String.valueOf(progress);
        paint.getTextBounds(text, 0, text.length(), bounds);
        if (progress == 1) {
            canvas.drawText(text, (float) (width / 2 - bounds.width() * 0.95), width / 2 - bounds.height() / 4, paint);
        } else {
            canvas.drawText(text, (float) (width / 2 - bounds.width() * 0.75), width / 2 - bounds.height() / 4, paint);
        }
        //画%号
        paint.setTextSize(presentTextSize);
        text = "%";
        canvas.drawText(text, (float) (width / 2 + bounds.width() * 0.35), width / 2 - bounds.height() / 4, paint);

        //画煲鸡时间
        paint.setColor(burnTotalTimeColor);
        paint.setTextSize(burnTextSize);
        paint.setStrokeWidth(burnTimeTextWidth);
        text = resources.getString(R.string.already_burn_time) + " " + burnTime + " " + resources.getString(R.string.min);
        paint.getTextBounds(text, 0, text.length(), bounds);
        canvas.drawText(text, (width / 2) - bounds.width() / 2, (float) (width / 2 + 1.5 * bounds.height()), paint);

        paint.getTextBounds(totalTime, 0, totalTime.length(), bounds);
        canvas.drawText(totalTime, (width / 2) - bounds.width() / 2, (float) (width / 2 + 3 * bounds.height()), paint);
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
        invalidate();
    }

    public void setProgress(int progress, String time) {
        this.progress = progress;
        this.burnTime = time;
        invalidate();
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getBottomColor() {
        return bottomColor;
    }

    public void setBottomColor(int bottomColor) {
        this.bottomColor = bottomColor;
    }

    public int getProgressColor() {
        return progressColor;
    }

    public void setProgressColor(int progressColor) {
        this.progressColor = progressColor;
    }

    public int getProgressTextColor() {
        return progressTextColor;
    }

    public void setProgressTextColor(int progressTextColor) {
        this.progressTextColor = progressTextColor;
    }

    public int getBurnTotalTimeColor() {
        return burnTotalTimeColor;
    }

    public void setBurnTotalTimeColor(int burnTotalTimeColor) {
        this.burnTotalTimeColor = burnTotalTimeColor;
    }

    public int getPaintWidth() {
        return paintWidth;
    }

    public void setPaintWidth(int paintWidth) {
        this.paintWidth = paintWidth;
    }

    public String getBurnTime() {
        return burnTime;
    }

    public void setBurnTime(String burnTime) {
        this.burnTime = burnTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }
}

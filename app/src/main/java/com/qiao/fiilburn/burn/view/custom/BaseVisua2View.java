package com.qiao.fiilburn.burn.view.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.audiofx.Visualizer;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;

/**
 * 描述： 重新写的音乐的柱子。。
 *
 * @author Yzz
 * @time 2016/3/25  10:39
 */
public class BaseVisua2View extends View {

    private static final int DN_W = 240;
    private static final int DN_H = 160;
    private static final int DN_SL = 1;
    private static final int DN_SW = 2;

    private int hgap = 0;
    private int vgap = 0;
    private int levelStep = 0;
    private float strokeWidth = 0;
    private float strokeLength = 0;

    private int viewHeight;//总视图的高度
    private int viewWidth;//总视图的宽度
    private int viewHeimin;//分20块。每块的高度。
    private Mode mMode = Mode.MODE_ONE;
    private int speed = 200;   //刷新频率

    /**
     * 用来刷新页面
     */
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            removeCallbacksAndMessages(null);
            switch (msg.what) {
                case 0:
                    invalidate();
                    handler.sendEmptyMessageDelayed(0, speed);
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }
    };


    /**
     * It is the max level.,最高的数目
     */
    protected final static int MAX_LEVEL = 13;

    /**
     * It is the cylinder number.
     */
    protected final static int CYLINDER_NUM = 60;

    /**
     * It is the visualizer.
     */
    protected Visualizer mVisualizer = null;

    /**
     * It is the paint which is used to draw to visual effect.
     */
    protected Paint mPaint = null;

    /**
     * It is the buffer of fft.
     */
    protected byte[] mData = new byte[CYLINDER_NUM * 2];

    boolean mDataEn = true;

    /**
     * It constructs the base visualizer view.
     *
     * @param context It is the context of the view owner.
     */
    public BaseVisua2View(Context context) {
        super(context);
        init();
    }

    public BaseVisua2View(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BaseVisua2View(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(0xFFBDBDBD);
//        mPaint.setStrokeJoin(Join.ROUND);
//        mPaint.setStrokeCap(Cap.ROUND);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
//        LogUtil.i("数值++BaseVisualizerView" + "left" + left + "right+" + right + "top" + top + "bottom" + bottom);
        float w, h, xr, yr;

        viewHeight = bottom - top;
        viewWidth = right - left;
        viewHeimin = viewHeight / 20;

        w = right - left;
        h = bottom - top;
        xr = w / (float) DN_W;
        yr = h / (float) DN_H;
        strokeWidth = DN_SW * yr;
        strokeLength = DN_SL * xr;
        hgap = (int) ((w - strokeLength * CYLINDER_NUM) / (CYLINDER_NUM + 1));
        vgap = (int) (h / (MAX_LEVEL + 2));
//        LogUtil.i("数值++BaseVisualizerView" + "strokeWidth" + strokeWidth + "strokeLength+" + strokeLength + "hgap" + hgap + "bottom" + vgap);
        mPaint.setStrokeWidth(strokeWidth * 2);
    }

    /**
     * 每次画都是走的他，，
     *
     * @param canvas
     * @param x
     * @param value
     */
    protected void drawCylinder(Canvas canvas, float x, byte value) {
        canvas.drawRect(x, getHeight() - value * viewHeimin, x + hgap / 2, getHeight(), mPaint);
    }

    @Override
    public void onDraw(Canvas canvas) {
        int number = 2 * CYLINDER_NUM;
        for (int i = 0; i < 2 * number; i++) {
            int r = 1;
            switch (mMode) {
                case MODE_ONE:
                    if (mData[i < number ? i : i - number] > 1) {
                        mData[i < number ? i : i - number]--;
                    } else {
                        mData[i < number ? i : i - number] = 1;
                    }
                    break;
                case MODE_TWO:
                    if (mData[i < number ? i : i - number] > 1) {
                        mData[i < number ? i : i - number]--;
                    } else {
                        r = (int) (Math.random() * 20);
                        mData[i < number ? i : i - number] = (byte) r;
                    }
                    break;
            }
            drawCylinder(canvas, i * (hgap / 2 + strokeLength / 2), mData[i < number ? i : i - number]);
        }
    }

    /**
     * It sets the visualizer of the view. DO set the viaulizer to null when exit the program.
     */
    public void setVisualizer(Visualizer visualizer) {
        mVisualizer = visualizer;
    }


    /**
     * It enables or disables the data processs.
     *
     * @param en If this value is true it enables the data process..
     */
    public void enableDataProcess(boolean en) {
        mDataEn = en;
    }


    /**
     * 更改模式
     *
     * @param mode
     */
    public void setMode(Mode mode) {
        this.mMode = mode;
    }


    public void startAnimation() {
        handler.sendEmptyMessageDelayed(0, speed);
    }

    public void stopAnimation() {
        handler.removeCallbacksAndMessages(null);
        handler = null;
//        mMode = Mode.MODE_FOUR;
//        invalidate();
    }

    /**
     * 模式一：要么没有值，要么是在减值。
     * 模式二： 一直运行
     * 模式三： 暂停时候
     */
    public enum Mode {
        MODE_ONE, MODE_TWO, MODE_THREE, MODE_FOUR;
    }

}

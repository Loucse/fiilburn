package com.qiao.fiilburn.burn.mode;


import android.content.Context;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.burn.iml.start.IStartMode;
import com.qiao.fiilburn.burn.view.act.BurnStartActivity;

import java.lang.ref.SoftReference;

public class StartMode implements IStartMode {

    private SoftReference<Context> mContext;

    public StartMode(Context context) {
        mContext = new SoftReference<Context>(context);
    }


    @Override
    public String[] getBurnStart() {
        return mContext.get().getResources().getStringArray(R.array.burnstart);
    }

    @Override
    public String getBurnId() {
        return ((BurnStartActivity) mContext.get()).getIntent().getStringExtra(Constant.BURN_ID);
    }

    @Override
    public int getBurnFlag() {
        return ((BurnStartActivity) mContext.get()).getIntent().getIntExtra(Constant.BURN_FLAG, 0);
    }

    @Override
    public int getBurnEntry() {
        return ((BurnStartActivity) mContext.get()).getIntent().getIntExtra(Constant.BURN_ENTRY, 0);
    }

    @Override
    public String getvolume() {
        return mContext.get().getResources().getString(R.string.burnstart_volume);
    }

    @Override
    public String getStage(int i) {
        String stage = null;
        switch (i) {
            case 1:
                stage = mContext.get().getResources().getString(R.string.burnstart_onestage);
                break;
            case 2:
                stage = mContext.get().getResources().getString(R.string.burnstart_twostage);
                break;
            case 3:
                stage = mContext.get().getResources().getString(R.string.burnstart_threestage);
                break;
            case 4:
                stage = mContext.get().getResources().getString(R.string.burnstart_fourtage);
                break;
        }
        if (stage != null) {
            return stage;
        } else {
            return mContext.get().getResources().getString(R.string.burnstart_fourtage);
        }
    }

    @Override
    public String getPleaseConHead() {
        return mContext.get().getResources().getString(R.string.please_conn_heatset);
    }

    @Override
    public String getBurnTime(int i) {
        String time = null;
        switch (i) {
            case 1:
                time = mContext.get().getResources().getString(R.string.burnstart_onetime);
                break;
            case 2:
                time = mContext.get().getResources().getString(R.string.burnstart_twotime);
                break;
            case 3:
                time = mContext.get().getResources().getString(R.string.burnstart_threetime);
                break;
        }
        if (time != null) {
            return time;
        } else {
            return mContext.get().getResources().getString(R.string.burnstart_threetime);
        }
    }

    @Override
    public String getBurnNoar() {
        return mContext.get().getResources().getString(R.string.burnre_noear);
    }


}

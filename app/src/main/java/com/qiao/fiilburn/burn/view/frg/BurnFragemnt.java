package com.qiao.fiilburn.burn.view.frg;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.qiao.fiilburn.R;
import com.qiao.fiilburn.adapter.BurnViewAdapter;
import com.qiao.fiilburn.base.BasePagerFragment;
import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.burn.bean.BurnInfo;
import com.qiao.fiilburn.burn.bean.BurnSingle;
import com.qiao.fiilburn.burn.presenter.BurnActivityCollector;
import com.qiao.fiilburn.burn.secvicer.BurnService;
import com.qiao.fiilburn.burn.secvicer.SoundService;
import com.qiao.fiilburn.burn.view.act.BurnStartActivity;
import com.qiao.fiilburn.burn.view.act.BurnTimeActivity;
import com.qiao.fiilburn.burn.view.act.MainActivity;
import com.qiao.fiilburn.burn.view.custom.SlideRecycleView;
import com.qiao.fiilburn.utils.DensityUtil;
import com.qiao.fiilburn.utils.DialogUtils;
import com.qiao.fiilburn.utils.DisplayUtils;
import com.qiao.fiilburn.utils.LogUtil;
import com.qiao.fiilburn.utils.SPUtils;
import com.qiao.fiilburn.utils.SoundUtil;
import com.qiao.fiilburn.utils.ToastUtils;
import com.qiao.fiilburn.utils.blue.BlueUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Spliterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BurnFragemnt extends BasePagerFragment {

    @BindView(R.id.fragment_burn_recycler)
    SlideRecycleView fragmentBurnRecycler;
    @BindView(R.id.fragment_burn_ll)
    LinearLayout fragmentBurnLl;
    Unbinder unbinder;
    private String weekDay[] = null;//得到多语言的数组，周日,周一....周六
    private ArrayList<BurnSingle> datas;
    private BurnService burnService = BurnService.getInstance();
    private int dp20, dp40, dp300;
    private int currentPosition;//当前修改的是哪个位置,修改名称，修改定时

    MyReceiver receiver;
    private BurnViewAdapter mBurnViewAdapter;

    @Override
    public View getView(ViewGroup container, Bundle savedInstanceState) {
        String datasStr = SPUtils.getString(getActivity().getApplicationContext(), Constant.WEEK);
        if (datasStr == null) {
            weekDay = mContext.getResources().getStringArray(R.array.burntime_week);
        } else {
//            weekDay = savedInstanceState.getStringArray(Constant.WEEK);
//            String datasStr = SPUtils.getString(getActivity().getApplicationContext(), Constant.WEEK);
//            LogUtil.i(" weekDay single __ : " + datasStr);
            Gson gson = new Gson();
            weekDay = gson.fromJson(datasStr, new TypeToken<String[]>() {
            }.getType());
            for (String weekStr : weekDay) {
//                LogUtil.i(" weekDay single __ : " + weekStr);
            }
        }
        View view = mLayoutInflater.inflate(R.layout.fragment_burn, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        receiver = new MyReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.burnSigle.TotalTime");
        getActivity().registerReceiver(receiver, filter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        LogUtil.i("这是 BurnFragment ___ : initData()");
        dp300 = DensityUtil.dip2px(mContext, 300);
        int width = DisplayUtils.getInstance().getDisplayWidth(mContext) - dp300;
        dp40 = (int) (width * ((float) 41 / 64));
        dp20 = (int) (width * ((float) 23 / 64));
        fragmentBurnRecycler.setDp20(dp20);
        fragmentBurnRecycler.setDp40(dp40);
        fragmentBurnRecycler.setDisplayWidth(DisplayUtils.getInstance().getDisplayWidth(mContext));
        String datasStr = SPUtils.getString(getActivity().getApplicationContext(), Constant.BURN_LIST);
        if (datasStr == null) {
            LogUtil.i("这是 BurnFragment ___ : initData() + if saved");
            datas = new ArrayList();
            LogUtil.i("这是 BurnFragment ___ : initData() + datas != null");
            getListData();
        } else {
//            datas = savedInstanceState.getParcelableArrayList(Constant.BURN_LIST);
//            String datasStr = SPUtils.getString(getActivity().getApplicationContext(), Constant.BURN_LIST);
//            LogUtil.i(" burn single __ : " + datasStr);
            Gson gson = new Gson();
            datas = gson.fromJson(datasStr, new TypeToken<List<BurnSingle>>() {
            }.getType());
            for (BurnSingle burnSingle : datas) {
//                LogUtil.i(" burn single __ : " + burnSingle.toString());
            }

        }

        fragmentBurnRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        fragmentBurnRecycler.setItemAnimator(new DefaultItemAnimator());

        mBurnViewAdapter = new BurnViewAdapter(getActivity(), datas, new int[]{dp20, dp40}, weekDay, mLayoutInflater);
        mBurnViewAdapter.setFragemnt(this);
        fragmentBurnRecycler.setAdapter(mBurnViewAdapter);

//        BlueUtils.isContendFiil(getContext());
    }

    private void getListData() {
        datas.clear();
        datas.add(new BurnSingle(0));
//        LogUtil.i("持久化+++得到的burnService.getBurnList()的长度" + burnService.getBurnList().size());
        datas.addAll(burnService.getBurnList());
        for (int i = 0; i < datas.size(); i++) {
            if (i != 0) {
                datas.get(i).setRemainingNum(1);
                datas.get(i).setOpen(false);
            }
        }
        datas.add(new BurnSingle(2));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (datas.size() <= 2) {
            fragmentBurnLl.setVisibility(View.VISIBLE);
            fragmentBurnRecycler.setVisibility(View.GONE);
        } else {
            fragmentBurnLl.setVisibility(View.GONE);
            fragmentBurnRecycler.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        String datasStr = new Gson().toJson(datas);
        String weekDayStr = new Gson().toJson(weekDay);
        SPUtils.setString(getActivity().getApplicationContext(), Constant.BURN_LIST, datasStr);
        SPUtils.setString(getActivity().getApplicationContext(), Constant.WEEK, weekDayStr);
//        outState.putParcelableArrayList(Constant.BURN_LIST, datas);
//        outState.putCharSequenceArray(Constant.WEEK, weekDay);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * 添加一个煲耳机界面
     */
    public void addBurnPager(BurnSingle bean) {
        add(bean); //添加代码
        LogUtil.e("这是 addBurnPager() __ burnfagment");
        fragmentBurnRecycler.setVisibility(View.VISIBLE);
        fragmentBurnRecycler.scrollToPosition(mBurnViewAdapter.getItemCount() - 1);
        fragmentBurnLl.setVisibility(View.GONE);
    }

    //#######################下面是删除修改等操作################################################################
    public void remove(int position, View v, List<BurnSingle> datas) {
        final BurnSingle bean = datas.get(position);
        if (BurnInfo.getId() != null && !"".equals(BurnInfo.getId())) {
            //有方案在播放看看是哪个方案
            if (BurnInfo.getId().equals(bean.getId()) && BurnInfo.isBurning()) {// 把按钮修改为正在播放
                deleteDialog(position, v, true);
            } else {
                deleteDialog(position, v, false);
            }
        } else {
            //没有方案在播放
            deleteDialog(position, v, false);
        }
        ((MainActivity) getActivity()).saveLog("1005", "");
//        notifyDataSetChanged();
    }

    //这个方法就是在添加的时候用到了，添加整个方案
    private void add(BurnSingle bean) {
        //下面是自己隐藏的@@
        try {
            datas.add(datas.size() - 1, bean);
            LogUtil.i("add 方案 看看错误信息 ———— ：" + datas.get(0));
            mBurnViewAdapter.notifyItemInserted(datas.size() - 2);
            mBurnViewAdapter.notifyItemRangeChanged(datas.size() - 2, 2);
        } catch (NullPointerException e) {
            LogUtil.i("add 方案 看看错误信息 ———— ：" + e.getMessage());
        }
    }

    /**
     * 删除的Dialog,里面的值就是处理删除的哪个方案
     */
    private void deleteDialog(final int position, final View vvv, final boolean isBurn) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_alarm_reset, null);
        ((TextView) view.findViewById(R.id.tv_warning)).setText(getString(R.string.delete_burn_top));
        ((TextView) view.findViewById(R.id.tv_info)).setText(getString(R.string.delete_butn_info));
        final AlertDialog dialog = builder.setView(view).show();
        view.findViewById(R.id.btn_reset_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TranslateAnimation animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, 0f,
                        Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, -1.3f);
                animation.setFillAfter(true);
                animation.setDuration(500);
                vvv.startAnimation(animation);
                dialog.dismiss();
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (isBurn)
                            stopService();
                        String idDetail = datas.get(position).getId();//要删除方案的id。
                        burnService.delBurnSigle(idDetail);
                        burnService.commitLocal();
                        LogUtil.i("commitLocal ———— ： onAnimationEnd");
//                        burnService.commitHttp();
                        datas.remove(position);
                        mBurnViewAdapter.notifyItemRemoved(position);
                        Message message = Message.obtain();
                        message.what = 0;
                        message.arg1 = position;
                        handler.sendMessageDelayed(message, 400);
//                        ((MainActivity) getActivity()).showAdd();//删除之后，执行是否显示添加按钮的方法

                        BurnActivityCollector.finishAll();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });
        view.findViewById(R.id.btn_reset_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    int position = msg.arg1;
                    if (datas.size() <= 2) {
                        fragmentBurnLl.setVisibility(View.VISIBLE);
                        fragmentBurnRecycler.setVisibility(View.GONE);
                    } else {
                        fragmentBurnLl.setVisibility(View.GONE);
                        fragmentBurnRecycler.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        }
    };

    /**
     * 停止音乐播放
     */
    private void stopService() {
        // 获取总煲机时长，进行计算存储
        BurnInfo.setId("");
        BurnInfo.init();
        // 暂停音乐
        Intent intent = new Intent(getActivity(), SoundService.class);
        intent.setAction("com.fengeek.service.SoundService");
        intent.putExtra(Constant.BURN_OPER, 3);
        getActivity().startService(intent);
    }

    //开始煲机按钮的监听
    public void updataData(int position, int progress, String burnTime) {
        BurnSingle bean = datas.get(position);
        if (!isConn(bean)) {
            return;
        }
        Intent intent1 = new Intent(getActivity(), BurnStartActivity.class);
        ((MainActivity) getActivity()).saveLog("1006", "");
        if (BurnInfo.isBurning()) {
            //正在煲机
            intent1.putExtra(Constant.BURN_ID, bean.getId());// 传过去正在煲机的Id
            intent1.putExtra(Constant.BURN_ENTRY, 2);// 传过去的值是1，为的是标记从哪个进去的。
            getActivity().startActivityForResult(intent1, 100);// 请求码100
        } else {
            //没有煲机
            intent1.putExtra(Constant.BURN_ID, bean.getId());// 传过去此方案的id
            intent1.putExtra(Constant.BURN_FLAG, 1);// 传过去标记就是1,为的就是主动播放
            intent1.putExtra(Constant.BURN_ENTRY, 2);// 传过去的值是1，为的是标记从哪个进去的。
            getActivity().startActivityForResult(intent1, 100);// 请求码100
        }
    }

    /**
     * 耳机的判断
     *
     * @return
     */
    private boolean isConn(BurnSingle burnSigle) {
        LogUtil.i("数值++drawCylinder" + "判断是否有项目正在播放BurnInfo.getId()" + BurnInfo.getId() + "burnSigle.toString" + burnSigle.toString());
        if (!TextUtils.isEmpty(BurnInfo.getId())) {
            //有正在进行的方案
            if (!BurnInfo.getId().equals(burnSigle.getId())) {
                //方案不匹配
                LogUtil.i("数值++drawCylinder" + "判断是否有项目正在播放BurnInfo.getId()" + BurnInfo.getId() + "burnSigle.getId()" + burnSigle.getId());
                ToastUtils.showToast(getActivity(), getResources().getString(R.string.heatset_pi_burn), 1000);
                return false;
            }
        } else {
            // 判断是否有耳机连接，或者耳机是否匹配
            if (!BlueUtils.isConnHeadSet() && !SoundUtil.isWiredHeadsetOn(getActivity())) {// 此时没有连接任何耳机
                ToastUtils.showToast(getActivity(), getResources().getString(R.string.please_conn_heatset), 1000);
                return false;
            }
        }
        return true;
    }

    /**
     * 从正在煲机过来的监听,
     */
    public void updataDataSuc() {
        mBurnViewAdapter.notifyDataSetChanged();
    }

    /**
     * 持久化过来的监听。
     */
    public void updataSuc(long timeall) {
        for (int i = 1; i < datas.size() - 1; i++) {
            if (datas.get(i).getId().equals(BurnInfo.getId())) {
                BurnSingle bean = datas.get(i);
                bean.setTotalTime(timeall);
                mBurnViewAdapter.notifyItemChanged(i);
            }
        }
    }

    //编辑耳机的名称的监听
    public void updataHeatSetName(int postion, String name) {
        ceateDialog(postion);
    }

    /**
     * 修改耳机名称的对话框
     */
    private void ceateDialog(int position) {
        currentPosition = position;
        DialogUtils.getInstance().burnNameChange(mContext, currentPosition, datas.get(position), burnService, mBurnViewAdapter);
    }

    //修改时间
    public void updataPrompt(int position, String time, String week) {
        currentPosition = position;
        Intent intent = new Intent(getActivity(), BurnTimeActivity.class);
        BurnSingle bs = datas.get(position);
        LogUtil.i("这是BurnFramgent的 打印一个 +—— ：" + bs.getId());
        intent.putExtra(Constant.BURN_ID, bs.getId());
        intent.putExtra(Constant.TIME, bs.getTiming());
        getActivity().startActivityForResult(intent, 100);
    }

    //修改定时时间,成功
    public void updataPromptSuc(String time) {
        BurnSingle bean = datas.get(currentPosition);
        bean.setTiming(time);
        mBurnViewAdapter.notifyItemChanged(currentPosition);
    }

    /**
     * 获取广播数据，用来处理数据的更新。。就是从煲耳机过来的值。。
     *
     * @author
     */
    public class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if ("com.burnSigle.TotalTime".equals(intent.getAction())) {
                Bundle bundle = intent.getExtras();
                int type = bundle.getInt(Constant.BURN_TYPE);
                LogUtil.i("数据大小 ___ : ");
                switch (type) {
                    case 1:
                        long count = bundle.getLong(Constant.TIME);
                        updataSuc(count);
                        break;
                    case 2://新建方案
                        String id = bundle.getString(Constant.BURN_ID);
                        BurnSingle bs = burnService.getBurnSigle(id);
                        addBurnPager(bs);
                        break;
                    case 3://修改时间
                        String timing = bundle.getString(Constant.TIMEING);
                        updataPromptSuc(timing);
                        break;
                    case 4://从煲耳机页面的返回
                        updataDataSuc();
                        break;
                }

            }
        }
    }

}

package com.qiao.fiilburn.burn.receiver;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.qiao.fiilburn.utils.LogUtil;
import com.qiao.fiilburn.utils.blue.BlueUtils;

public class HeadsetReveiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
            LogUtil.i("Burn __ 蓝牙状态发生变化");

            if (!BlueUtils.isConnHeadSet()) {
                LogUtil.i("Burn __ 蓝牙失去连接,暂停煲机");

            }
        }
    }
}

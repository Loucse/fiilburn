package com.qiao.fiilburn.burn.presenter;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.burn.bean.BurnInfo;
import com.qiao.fiilburn.burn.bean.BurnSingle;
import com.qiao.fiilburn.burn.iml.start.IStartPresenter;
import com.qiao.fiilburn.burn.iml.start.IStartView;
import com.qiao.fiilburn.burn.mode.StartMode;
import com.qiao.fiilburn.burn.secvicer.BurnService;
import com.qiao.fiilburn.burn.view.act.BurnStartActivity;
import com.qiao.fiilburn.burn.view.custom.BaseVisua2View;
import com.qiao.fiilburn.utils.LogUtil;
import com.qiao.fiilburn.utils.SPUtils;
import com.qiao.fiilburn.utils.SoundUtil;
import com.qiao.fiilburn.utils.ToastUtils;
import com.qiao.fiilburn.utils.blue.BlueUtils;

import java.lang.ref.SoftReference;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class StartPresenter implements IStartPresenter {


    private SoftReference<Context> mContext = null;//通过弱引用实例化Context
    private SoftReference<StartMode> mMode = null;//通过弱引用实例化Context
    private IStartView mView = null;//实例化接口类
    private RefreshHandler refreshHandler = new RefreshHandler();
    private boolean isBurnMiddle = false;// 是否位于煲机中间休息过程
    private int timerRate = 1000;// timer刷新频率

    private int position = 0;// 出现了几次

    public StartPresenter(IStartView mView) {
        this.mView = mView;
    }

    public StartMode getMode() {
        if (mMode == null || mMode.get() == null) {
            mMode = new SoftReference<>(new StartMode(getContext()));
            return mMode.get();
        }
        return mMode.get();
    }

    @Override
    public Context getContext() {
        if (mContext == null || mContext.get() == null) {
            mContext = new SoftReference(mView.getContext());
            return mContext.get();
        }
        return mContext.get();
    }

    private String text[];
    private String id;
    private int flag, entry;// 判断 是否直接进行，从哪里来


    private BurnService burnService = BurnService.getInstance();

    private HeadsetPlugReceiver headsetPlugReceiver;// 监听耳机插入拔出等事件

    @Override
    public void onCreate() {
        id = getMode().getBurnId();
        flag = getMode().getBurnFlag();
        entry = getMode().getBurnEntry();// 入口，从哪里过来的
        text = getMode().getBurnStart();//得到煲耳机时候所有提示语
        burnSigle = burnService.getBurnSigle(id);
        initView();
        if (flag == 1) {
            // 直接开始啊
            startBurn(true);
            LogUtil.i("开始煲机");
        }
        registerHeadListener();
    }

    private void registerHeadListener() {
        Timer animTimer = new Timer();
        TimerTask animTask = new TimerTask() {
            @Override
            public void run() {
                refreshHandler.sendEmptyMessage(3);
            }
        };
        animTimer.schedule(animTask, 0, 55);
        // 注册监听耳机插入拔出等事件
        headsetPlugReceiver = new HeadsetPlugReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.HEADSET_PLUG");
        intentFilter.addAction(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED);
        intentFilter.addAction("android.bluetooth.adapter.action.BLE_ACL_DISCONNECTED");
        intentFilter.addAction("android.bluetooth.adapter.action.BLE_STATE_CHANGED");
        getContext().registerReceiver(headsetPlugReceiver, intentFilter);

    }

    private Boolean isStart = false;

    private void initView() {
        mView.titleBarabout().setVisibility(View.GONE);
        setTime();
        textChange();
        mView.titleBarAdd().setVisibility(View.INVISIBLE);
        mView.titleBarBack().setVisibility(View.VISIBLE);
        if (!BlueUtils.isConnHeadSet() && !SoundUtil.isWiredHeadsetOn(getContext().getApplicationContext())) {// 此时没有连接任何耳机
            mView.ivTrigon().setVisibility(View.VISIBLE);
            mView.ivCircle().setVisibility(View.INVISIBLE);
            mView.tvFlag().setText(getMode().getPleaseConHead());
            LogUtil.i("这里打印了 initView ");
        } else {
            mView.ivTrigon().setVisibility(View.INVISIBLE);
            mView.ivCircle().setVisibility(View.VISIBLE);
            mView.tvFlag().setText(text[0]);
        }
        // 判断是否正在煲机
        if (BurnInfo.isBurning()) {
            createSector(BurnInfo.getPercent());
            // 判断按钮是否为正在播放
            if (BurnInfo.isPlaying()) {// 把按钮修改为正在播放
                LogUtil.i("数值++走到这一步没有？？77777");
                mView.btnStart().setVisibility(View.INVISIBLE);
                mView.btnPause().setVisibility(View.VISIBLE);
                startBurn(false);// 上面的代码进行了一下修改，有点意思。
            } else {
                isStart = false;
                LogUtil.i("数值++走到这一步没有？？66666");
                mView.btnStart().setVisibility(View.VISIBLE);
                mView.btnPause().setVisibility(View.INVISIBLE);
            }
        }
        if (BurnInfo.getBurnNumber() == 3) {
            // 现在煲机三次
            mView.btnBurnOne().setVisibility(View.GONE);
            mView.btnBurnThree().setVisibility(View.VISIBLE);
        }
        if (BurnInfo.getBurnNowNumber() == 1) {
            mView.tvBurnNumber().setVisibility(View.GONE);
        } else if (BurnInfo.getBurnNowNumber() == 2) {
            mView.tvBurnNumber().setVisibility(View.VISIBLE);
            mView.tvBurnNumber().setText(getMode().getBurnTime(2));
        } else if (BurnInfo.getBurnNowNumber() == 3) {
            mView.tvBurnNumber().setVisibility(View.VISIBLE);
            mView.tvBurnNumber().setText(getMode().getBurnTime(3));
        }
    }

    /**
     * 这个方法用来更新扇形的面积，需要传的是进度。
     *
     * @param progress
     */
    private void createSector(int progress) {
        if (progress > 360) {
            progress = 360;
        }
        mView.cpBackground().setProgressNotInUiThread(progress);
    }


    public static Timer timer;
    private TimerTask timerTask;

    /**
     * 抽取出来，用于煲机的开始
     */
    public void startBurn(Boolean setHeadset) {
        if (!isStart) {
            LogUtil.i("this heat is >>>>>: + " + setHeadset);
            if (!isConn()) {
                return;
            }
            mView.mBaseVisualizerView().startAnimation();
//            BurnStartHelp.getBurnStartHelp().startSetSound(mContext, setHeadset);
            mView.ivTrigon().setVisibility(View.INVISIBLE);
            mView.ivCircle().setVisibility(View.VISIBLE);
            if (BurnInfo.getRealOnTime() != 0) {
                mView.tvFlag().setText(text[1]);
            }
            mView.btnStart().setVisibility(View.INVISIBLE);
            mView.btnPause().setVisibility(View.VISIBLE);
            LogUtil.i("数值++走到这一步没有？？");
            // 设置音量
            BurnInfo.setVolume(burnSigle != null ? burnSigle.gVolume() : 25);
            // 如果未启动煲机服务，此时启动
            if (!BurnInfo.isBurning()) {
                BurnInfo.setMakeDate(new Date());
            }
            BurnInfo.setStartDate(new Date());
            burnStart();
            isStart = true;

            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    // 更新时间，绘制圆
                    if (BurnInfo.getRealOnTime() != 0) {
                        refreshHandler.sendEmptyMessage(0);
                        LogUtil.i("数值++处理煲机的小猪猪0000000");
                    }
                    if (BurnInfo.getReadySurplus() != 0) {
                        // 准备时间，从15开始倒计时
                        refreshHandler.sendEmptyMessage(2);
//                        LogUtil.i("数值++处理煲机的小猪猪2222222");
                    }
                    // 如果煲机声音被打断，此时，将页面设置为暂停
                    if (BurnInfo.isInterrupted()) {
                        refreshHandler.sendEmptyMessage(1);
                        BurnInfo.setInterrupted(false);
                        LogUtil.i("数值++处理煲机的小猪猪11111111");
                    }
                    if (BurnInfo.getPauseSurLong() != 0) {
                        // 发4进行处理
                        refreshHandler.sendEmptyMessage(4);
                        LogUtil.i("数值++处理煲机的小猪猪44444444");
                    }
                }
            };
            timer.scheduleAtFixedRate(timerTask, 0, timerRate);
        } else {
            LogUtil.i("this heat is >>else>>>: + " + setHeadset);
            // 正在播放的时候执行这一步操作,就是暂停
            mView.mBaseVisualizerView().setMode(BaseVisua2View.Mode.MODE_ONE);
            LogUtil.i("数值++走到这一步没有？？222");
            mView.btnStart().setVisibility(View.VISIBLE);
            mView.btnPause().setVisibility(View.INVISIBLE);
            BurnInfo.setPauseDate(new Date());
            timer.cancel();
            isStart = false;
            burnPause();
        }
    }

    private void burnStart() {
        if (burnSigle != null) {
            BurnInfo.setId(burnSigle.getId());
            mView.burnStart(burnSigle.getId());
            LogUtil.i("数值++走到 BurnInfo.setId(burnSigle.getId());" + BurnInfo.getId());
        }
    }

    private void burnPause() {
        persistenceBurn();// 保存数据
        mView.burnPause();
    }

    /**
     * 处理
     */
    private void dealConpleteBurn() {
        stopService();
        int int1 = SPUtils.getInt(getContext().getApplicationContext(), Constant.MULTI_MEDIA_VOLUME);
        if (int1 != 0) {
            // 声音恢复到煲机之前
            SoundUtil.setVoise(getContext().getApplicationContext(), int1);
        }
//        BurnStartHelp.getBurnStartHelp().stopSetSound(getContext());
    }

    private void stopService() {
        // 获取总煲机时长，进行计算存储
        persistenceBurn();
        mView.stopService();
    }

    /**
     * 持久化煲机数据
     */
    private void persistenceBurn() {
        if (burnSigle == null) {
            return;
        }
        // 获取burnSigle
        burnSigle.setTotalTime(burnSigle.getTotalTime() + BurnInfo.getPresTime());
        // 持久化操作
        burnService.updateBurnSigle(burnSigle);
        burnService.commitLocal();
        Intent intent = new Intent();
        intent.putExtra(Constant.TIME, burnSigle.getTotalTime());
        intent.putExtra(Constant.BURN_TYPE, 1);
        intent.setAction("com.burnSigle.TotalTime");
        getContext().sendBroadcast(intent);
        LogUtil.i("持久化问题+持久化之后最终存储的问题" + burnSigle.getTotalTime() / 1000);
        BurnInfo.setPresTime(0);
        /*if (!BurnInfo.isBurning()) {
            burnService.commitHttp();
        }*/
    }

    private boolean isConn() {
        // 判断是否有耳机连接，或者耳机是否匹配
        if (!BlueUtils.isConnHeadSet() && !SoundUtil.isWiredHeadsetOn(getContext().getApplicationContext())) {// 此时没有连接任何耳机
            ToastUtils.showToast(getContext().getApplicationContext(), getMode().getPleaseConHead());//请连接您的耳机
            LogUtil.i("这里打印了 isConn ");
            return false;
        }
        // 判断是之前煲过耳机
        return true;
    }

    private static BurnSingle burnSigle;// 得到的具体的煲机方案

    private void setTime() {
        mView.tvOver().setText(BurnInfo.getBurnTime());
        mView.tvName().setText(burnSigle != null ? burnSigle.getSname() : "FIIL");
    }

    /**
     * 文字的改变。
     */
    public void textChange() {
        if (BurnInfo.getPauseSurLong() == 0) {
            if (BurnInfo.getRealOnTime() != 0) {
                mView.tvOver().setText(BurnInfo.getBurnTime());
            } else {
                mView.tvOver().setText(BurnInfo.getReadySurplus() / 1000 + "");
            }
//            LogUtil.i("数值++倒计时的时间6666666+++" + BurnInfo.getBurnTime());
            if (burnSigle != null) {
                String sta = burnSigle.gStage() == 25 ? getMode().getStage(1) : burnSigle.gStage() == 50 ? getMode().getStage(2) : burnSigle.gStage() == 75 ? getMode().getStage(3) : getMode().getStage(4);
                mView.tvStage().setText(sta + ":" + getMode().getvolume() + burnSigle.gVolume() + "%");
            }
            mView.tvRest().setVisibility(View.GONE);
        } else {
            mView.tvOver().setText(BurnInfo.getPauseSurplus());
//            LogUtil.i("数值++倒计时的时间7777777+++" + BurnInfo.getPauseSurplus());
        }
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestory() {
        mView.mBaseVisualizerView().stopAnimation();
        getContext().unregisterReceiver(headsetPlugReceiver);
    }

    /**
     * 取消耳机进度的对话框
     */
    public void stopDialog() {
        AlertDialog alertDialog = null;
        AlertDialog.Builder builderPeriod = new AlertDialog.Builder(getContext());
        View viewPeriod = ((BurnStartActivity) getContext()).getLayoutInflater().inflate(R.layout.burn_burn_dialog, null);
        Button btnCancle = viewPeriod.findViewById(R.id.burn_creat_cancel);
        Button btnOk = viewPeriod.findViewById(R.id.burn_creat_ok);
        // 设置自定义的布局
        builderPeriod.setView(viewPeriod);
        // 是否可以被取消
        builderPeriod.setCancelable(false);
        alertDialog = builderPeriod.create();
        alertDialog.show();
        final AlertDialog finalAlertDialog = alertDialog;
        btnOk.setOnClickListener(v -> {
            // 手动停止添加统计
            BurnInfo.setBurnNumber(1);
            BurnInfo.setBurning(false);
            if (timer != null)
                timer.cancel();
            mView.sendReveicer(4);
            BurnInfo.setBurnNowNumber(1);
            dealConpleteBurn();
            ((BurnStartActivity) getContext()).saveLog("1009", BurnInfo.getBurnTime());
            ((BurnStartActivity) getContext()).saveLog("1008", "");
            finalAlertDialog.dismiss();
        });
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalAlertDialog.dismiss();
            }
        });
    }

    /**
     * 监听耳机插入拔出等事件
     *
     * @author Administrator
     */
    public class HeadsetPlugReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("android.bluetooth.adapter.action.BLE_ACL_DISCONNECTED".equals(action)) {
                if (!BlueUtils.isConnHeadSet()) {
                    LogUtil.i("BURN--蓝牙耳机失去连接，此时暂停煲机...");
                    // BURN--蓝牙耳机失去连接，此时暂停煲机...
                    mView.ivTrigon().setVisibility(View.VISIBLE);
                    mView.ivCircle().setVisibility(View.INVISIBLE);
                    mView.tvFlag().setText(getMode().getBurnNoar());
                    LogUtil.i("这里打印了 onReceive 1");
                    mView.btnStart().setVisibility(View.VISIBLE);
                    mView.btnPause().setVisibility(View.INVISIBLE);
                    BurnInfo.setInterrupted(false);
                }
            }
            if ("android.bluetooth.adapter.action.BLE_STATE_CHANGED".equals(action)) {
                if (!BlueUtils.isConnHeadSet()) {
                    LogUtil.i("BURN--蓝牙耳机失去连接，此时暂停煲机...");
                    // BURN--蓝牙耳机失去连接，此时暂停煲机...
                    mView.ivTrigon().setVisibility(View.VISIBLE);
                    mView.ivCircle().setVisibility(View.INVISIBLE);
                    mView.tvFlag().setText(getMode().getBurnNoar());
                    LogUtil.i("这里打印了 onReceive 2");
                    mView.btnStart().setVisibility(View.VISIBLE);
                    mView.btnPause().setVisibility(View.INVISIBLE);
                    BurnInfo.setInterrupted(false);
                }
            }
            if (BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {// 蓝牙适配器连接状态发生变化
                LogUtil.i("BURN--蓝牙适配器连接状态发生变化，判断是否为蓝牙耳机掉线...BurnStart");
                if (!BlueUtils.isConnHeadSet()) {
                    // BURN--蓝牙耳机失去连接，此时暂停煲机...
                    mView.ivTrigon().setVisibility(View.VISIBLE);
                    mView.ivCircle().setVisibility(View.INVISIBLE);
                    mView.tvFlag().setText(getMode().getBurnNoar());
                    LogUtil.i("这里打印了 onReceive 3");
                    mView.btnStart().setVisibility(View.VISIBLE);
                    mView.btnPause().setVisibility(View.INVISIBLE);
                    BurnInfo.setInterrupted(false);
                } else if (BlueUtils.isConnHeadSet()) {
                    // BURN--蓝牙耳机连接
                    mView.ivTrigon().setVisibility(View.INVISIBLE);
                    mView.ivCircle().setVisibility(View.VISIBLE);
                    BurnInfo.setInterrupted(false);
                    LogUtil.i("数值++走到这一步没有？？6666");
                    if (BurnInfo.getRealOnTime() != 0) {
                        mView.tvFlag().setText(text[1]);
                    } else {
                        mView.tvFlag().setText(text[0]);
                        mView.tvOver().setText(BurnInfo.getReadySurplus() / 1000 + "");
                    }
                    new Thread() {
                        public void run() {
                            try {
                                sleep(3000);
//                                BlueUtils.isContendFiil(getApplicationContext());
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }.start();
                }

            } else if ("android.intent.action.HEADSET_PLUG".equals(action)) {// 有插线的耳机
                if (intent.hasExtra("state")) {
                    if (intent.getIntExtra("state", 0) == 0 && !BlueUtils.isConnHeadSet()) {
                        // 插线耳机断开，蓝牙没有连接，并且没有这个蓝牙的地址
                        LogUtil.i("BURN--有线耳机断开连接,此时暂停煲机...BurnStart");
                        mView.ivTrigon().setVisibility(View.VISIBLE);
                        mView.ivCircle().setVisibility(View.INVISIBLE);
                        mView.tvFlag().setText(getMode().getBurnNoar());
                        LogUtil.i("这里打印了 onReceive 4");
                        mView.btnStart().setVisibility(View.VISIBLE);
                        mView.btnPause().setVisibility(View.INVISIBLE);
//                        DeviceInfoHelp.setFiil(false);
//                        DeviceInfoHelp.setIsFiilAddress("");
                    } else if (intent.getIntExtra("state", 0) == 1) {
                        LogUtil.i("BURN--有线耳机已连接...BurnStart");
                        BurnInfo.setInterrupted(false);
                        mView.ivTrigon().setVisibility(View.INVISIBLE);
                        mView.ivCircle().setVisibility(View.VISIBLE);
                        if (BurnInfo.getRealOnTime() != 0) {
                            mView.tvFlag().setText(text[1]);
                        } else {
                            // 插着耳机从列表直接进入的时候会走这一步
                            mView.tvFlag().setText(text[0]);
                        }
                    }
                }
            }
        }
    }// 主线程中的handler

    class RefreshHandler extends Handler {
        /**
         * 接受子线程传递的消息机制
         */
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int what = msg.what;
            switch (what) {
                case 0:
                    // 刷新页面的文字
                    if (BurnInfo.getPauseSurLong() != 0) {
                        mView.mBaseVisualizerView().setMode(BaseVisua2View.Mode.MODE_ONE);
                    } else {
                        mView.mBaseVisualizerView().setMode(BaseVisua2View.Mode.MODE_TWO);
                    }
                    createSector(BurnInfo.getPercent());
                    textChange();
                    if (BurnInfo.getRealOnTime() >= BurnInfo.ALL_TIME) {//煲机完成才走这里
                        // 一次煲机完成
                        if (BurnInfo.getBurnNumber() == 1) {
                            // 只是让煲机一次
                            autoBurnStop();
                        } else {
                            // 让煲机三次
                            if (BurnInfo.getBurnNowNumber() == 1) {
                                mView.startService(6);
                                mView.tvBurnNumber().setVisibility(View.VISIBLE);
                                mView.tvBurnNumber().setText(getMode().getBurnTime(2));
                                BurnInfo.setWaitTime(0);
                                createSector(0);
                                mView.tvOver().setText("00:00");
                                LogUtil.i("数值+soundService第二二二个循环66666");
                                BurnInfo.setBurnNowNumber(2);
                            } else if (BurnInfo.getBurnNowNumber() == 2) {
                                mView.startService(6);
                                mView.tvBurnNumber().setVisibility(View.VISIBLE);
                                mView.tvBurnNumber().setText(getMode().getBurnTime(3));
                                BurnInfo.setWaitTime(0);
                                createSector(0);
                                mView.tvOver().setText("00:00");
                                LogUtil.i("数值+soundService第三三三个循环66666");
                                BurnInfo.setBurnNowNumber(3);
                            } else if (BurnInfo.getBurnNowNumber() == 3) {
                                // 三次煲机完成
                                autoBurnStop();
                                BurnInfo.setBurnNumber(1);
                                BurnInfo.setBurnNowNumber(1);
                            }
                        }
                    } else if ((!BurnInfo.isMiddlePauseOper) && (BurnInfo.getRealOnTime() >= BurnInfo.MIDDLE_TIME) && (!BurnInfo.isMiddleStartOper)) {// 判断当前状态，如果播放到中间位置，此时暂停音乐
                        mView.mBaseVisualizerView().setMode(BaseVisua2View.Mode.MODE_ONE);
                        isBurnMiddle = true;
                        BurnInfo.setMiddlePauseOper(true);
                    } else if (BurnInfo.isMiddlePauseOper && (BurnInfo.getRealOnTime() >= (BurnInfo.MIDDLE_TIME + 2000)) && (!BurnInfo.isMiddleStartOper)) {//
                        mView.mBaseVisualizerView().setMode(BaseVisua2View.Mode.MODE_TWO);
                        isBurnMiddle = false;
                        BurnInfo.setMiddleStartOper(true);
                    }
                    position += 1;
                    break;
                case 1: // 暂停煲机操作
                    mView.mBaseVisualizerView().setMode(BaseVisua2View.Mode.MODE_ONE);
                    if (!BlueUtils.isConnHeadSet() && !SoundUtil.isWiredHeadsetOn(getContext().getApplicationContext())) {
                        // 既没有连接蓝牙有没有连接有线
                        mView.ivTrigon().setVisibility(View.VISIBLE);
                        mView.ivCircle().setVisibility(View.INVISIBLE);
                        LogUtil.i("这里打印了  case 1: // 暂停煲机操作 ");
                        mView.tvFlag().setText(getMode().getPleaseConHead());
                        mView.btnStart().setVisibility(View.VISIBLE);
                        mView.btnPause().setVisibility(View.INVISIBLE);
                        BurnInfo.setPauseDate(new Date());
                        timer.cancel();
                        isStart = false;
                        LogUtil.i("煲机暂停了 ———— case1 if");
                        burnPause();
                    } else {
                        LogUtil.i("数值++走到这一步没有？？33331111");
                        // 有外音播放，并且是有线耳机
                        mView.btnStart().setVisibility(View.VISIBLE);
                        mView.btnPause().setVisibility(View.INVISIBLE);
                        BurnInfo.setPauseDate(new Date());
                        timer.cancel();
                        isStart = false;
                        LogUtil.i("煲机暂停了 ———— case1 else");
                        burnPause();
                    }

                    break;
                case 2:
                    mView.mBaseVisualizerView().setMode(BaseVisua2View.Mode.MODE_ONE);
                    mView.tvFlag().setText(text[0]);
                    mView.tvOver().setText(BurnInfo.getReadySurplus() / 1000 + "");
                    if (BurnInfo.getReadySurplus() / 1000 == 0) {
                        mView.tvFlag().setText(text[0]);
                    }
                    break;
                case 3:
                    mView.setPlayingAnim(isBurnMiddle);
                    break;
                case 4:
                    if (BurnInfo.getBurnNowNumber() == 2) {
                        mView.tvBurnNumber().setVisibility(View.VISIBLE);
                        mView.tvBurnNumber().setText(getMode().getBurnTime(2));
                    } else if (BurnInfo.getBurnNowNumber() == 3) {
                        mView.tvBurnNumber().setVisibility(View.VISIBLE);
                        mView.tvBurnNumber().setText(getMode().getBurnTime(3));
                    } else if (BurnInfo.getBurnNowNumber() == 1) {
                        mView.tvBurnNumber().setVisibility(View.GONE);
                    }
                    textChange();
                    break;
                default:
                    break;
            }
        }

        /**
         * 自动煲机完成,就是退出这个页面
         */
        private void autoBurnStop() {
            timer.cancel();
            Toast.makeText(getContext().getApplicationContext(), getContext().getString(R.string.burn_compleat), Toast.LENGTH_SHORT).show();
            BurnInfo.setBurnNumber(1);
            BurnInfo.setBurnNowNumber(1);
            // 将按钮设置为不可点击
            mView.btnStart().setClickable(false);
            mView.btnPause().setClickable(false);
            mView.btnStart().setEnabled(false);
            mView.btnPause().setEnabled(false);
            dealConpleteBurn();
            /************** 停止时总进度的统计 *********************/
            if (entry == 2) {
                mView.sendReveicer(4);
                if (timer != null)
                    timer.cancel();
                LogUtil.i("数值自杀+entry == 2+有没有自杀");
                BurnActivityCollector.finishAll();
            } else {
                LogUtil.i("数值自杀有没有自杀");
                mView.sendReveicer(4);
                if (timer != null)
                    timer.cancel();
                BurnActivityCollector.finishAll();
            }
        }
    }

}

package com.qiao.fiilburn.burn.create.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.BaseActivity;
import com.qiao.fiilburn.burn.create.iml.ICreateBurnView;
import com.qiao.fiilburn.burn.create.presenter.CreateBurnPresenter;

import butterknife.BindView;
import butterknife.OnClick;

public class CreateBurnActivity extends BaseActivity implements ICreateBurnView {


    @BindView(R.id.title_bar_back)
    ImageView titleBarBack;
    @BindView(R.id.title_bar_title)
    TextView titleBarTitle;
    @BindView(R.id.title_bar_add)
    ImageView titleBarAdd;
    @BindView(R.id.act_cteateear_headset_img)
    ImageView hgeadsetImg;
    @BindView(R.id.act_cteateear_headset_tv)
    TextView headsetTv;
    @BindView(R.id.act_cteateear_headset_ll)
    LinearLayout headsetLl;
    @BindView(R.id.act_cteateear_earphone_img)
    ImageView earphoneImg;
    @BindView(R.id.act_cteateear_earphone_tv)
    TextView earphoneTv;
    @BindView(R.id.act_cteateear_earphone_ll)
    LinearLayout earphoneLl;
    @BindView(R.id.title_bar_about)
    ImageView titleBarAbout;


    private CreateBurnPresenter presenter = new CreateBurnPresenter(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSystem7Gray();
        setTransNavigation();
        presenter.onCreate();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_create_earphone;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @OnClick({R.id.title_bar_back, R.id.act_cteateear_headset_ll, R.id.act_cteateear_earphone_ll})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.title_bar_back:
                this.finish();
                break;
            case R.id.act_cteateear_headset_ll:
                presenter.createBurnDialog(2, 1);
                break;
            case R.id.act_cteateear_earphone_ll:
                presenter.createBurnDialog(1, 2);
                break;
        }
    }

    @Override
    public ImageView titleBarBack() {
        return titleBarBack;
    }

    @Override
    public ImageView titleBarBabout() {
        return titleBarAbout;
    }

    @Override
    public TextView titleBarTitle() {
        return titleBarTitle;
    }

    @Override
    public ImageView titleBarAdd() {
        return titleBarAdd;
    }

    @Override
    public ImageView hgeadsetImg() {
        return hgeadsetImg;
    }

    @Override
    public TextView headsetTv() {
        return headsetTv;
    }

    @Override
    public LinearLayout headsetLl() {
        return headsetLl;
    }

    @Override
    public ImageView earphoneImg() {
        return earphoneImg;
    }

    @Override
    public TextView earphoneTv() {
        return earphoneTv;
    }

    @Override
    public LinearLayout earphoneLl() {
        return earphoneLl;
    }




  /*  @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                viewflipper.setInAnimation(this, R.anim.in_leftright);
                viewflipper.setOutAnimation(this, R.anim.out_leftright);
                viewflipper.showPrevious();
                finish();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }*/
}

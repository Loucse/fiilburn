package com.qiao.fiilburn.burn.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.qiao.fiilburn.R;


/**
 * 描述： 煲耳机页面，自己画的圆圈
 *
 * @author Yzz
 * @time 2016/3/25  15:19
 */
public class CirclePointView extends View {
    private int progress = 0;
    private int maxProgress = 360;
    private float position = 0;
    private int progressColor;

    public CirclePointView(Context context) {
        super(context);
    }

    public CirclePointView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircleView,0,0);
        progressColor = array.getColor(R.styleable.CircleView_progressColor, Color.parseColor("#069adc"));
        array.recycle();
    }

    public CirclePointView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * 要画图形，最起码要有三个对象： 1.颜色对象 Color 2.画笔对象 Paint 3.画布对象 Canvas
     */

    @Override
    public void onDraw(Canvas canvas) {

        Paint paint = new Paint();
        paint.setColor(progressColor);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        float f1 = dipToPx(6);
        paint.setStrokeWidth(f1);
        //画的是整个圆圈
        Paint paint1 = new Paint();
        paint1.setARGB(255, 233,233,233);
        paint1.setAntiAlias(true);
        paint1.setStyle(Paint.Style.STROKE);
        paint1.setStrokeWidth(f1);

        Paint paint2 = new Paint();
        paint.setColor(progressColor);
        paint2.setAntiAlias(true);
//        paint2.setStyle(Paint.Style.STROKE);
        paint2.setStrokeWidth(f1);

        position = dipToPx(228);
        paint.setStrokeCap(Paint.Cap.ROUND);//这个就是圆角的问题
        int re = dipToPx(16);
        float angle = ((float) progress / maxProgress) * 360;

        canvas.drawArc(new RectF(re, re, position + re, position + re), -90, 360, false, paint1);
        canvas.drawArc(new RectF(re, re, position + re, position + re), -90, angle, false, paint);
        super.onDraw(canvas);
    }

    public void setProgressNotInUiThread(int progress) {
        this.progress = progress;
        this.postInvalidate();
    }

    private int dipToPx(int dip) {
        float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (dip * scale + 0.5f * (dip >= 0 ? 1 : -1));
    }

    public int getProgressColor() {
        return progressColor;
    }

    public void setProgressColor(int progressColor) {
        this.progressColor = progressColor;
    }
}

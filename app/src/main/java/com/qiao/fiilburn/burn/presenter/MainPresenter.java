package com.qiao.fiilburn.burn.presenter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.burn.iml.main.IMainPresenter;
import com.qiao.fiilburn.burn.iml.main.IMainView;
import com.qiao.fiilburn.burn.mode.MainMode;
import com.qiao.fiilburn.burn.secvicer.BurnService;
import com.qiao.fiilburn.burn.view.act.MainActivity;
import com.qiao.fiilburn.burn.view.frg.BurnFragemnt;
import com.qiao.fiilburn.http.RetrofitInit;
import com.qiao.fiilburn.http.manager.DownLoadManager;
import com.qiao.fiilburn.launcher.bean.UrlImageVideo;
import com.qiao.fiilburn.utils.AppUtils;
import com.qiao.fiilburn.utils.LogUtil;
import com.qiao.fiilburn.utils.Md5Util;
import com.qiao.fiilburn.utils.NetUtils;
import com.qiao.fiilburn.utils.SPUtils;

import java.io.File;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter implements IMainPresenter {
    private String[] imageUrls;
    private ArrayList<String> imageUrlList = new ArrayList<>();
    private int startDown = 0;// 下载的数量
    private int endDown = 0;// 下载完成的数量
    private boolean isDown = false;// 是否开始下载。有下载的话，就不关闭启动页服务

    private SoftReference<Context> mContext = null;//通过弱引用实例化Context
    private SoftReference<MainMode> mMode = null;//通过弱引用实例化Context
    private IMainView mView = null;//实例化接口类

    Disposable disposable;


    private BurnService burnService;// 操作煲机服务

    public MainPresenter(IMainView mView) {
        this.mView = mView;
    }

    public MainMode getMode() {
        if (mMode == null || mMode.get() == null) {
            mMode = new SoftReference<>(new MainMode(getContext()));
            return mMode.get();
        }
        return mMode.get();
    }

    @Override
    public Context getContext() {
        if (mContext == null || mContext.get() == null) {
            mContext = new SoftReference(mView.getContext());
            return mContext.get();
        }
        return mContext.get();
    }

    @Override
    public void onCreate() {
        burnService = BurnService.getInstance();
        burnService.setContext(getContext());

        mView.getTitleBarBack().setVisibility(View.GONE);
        mView.getTitleBarabout().setVisibility(View.VISIBLE);

        FragmentTransaction ftBurn = ((MainActivity) getContext()).getSupportFragmentManager().beginTransaction();
        BurnFragemnt burnFragment = new BurnFragemnt();
        ftBurn.add(R.id.act_main_fragment, burnFragment);
        ftBurn.commit();
        RetrofitInit.getInstance().initPost();
        initLauncherImg();
    }

    @SuppressLint("CheckResult")
    private void initLauncherImg() {
        disposable = RetrofitInit.getApi().getLauncher(Constant.CODE_SPLASH_START)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listResult -> {
                    if (listResult.getCode() == 200) {
                        Constant.setSplashData(listResult.getData());
                        LogUtil.i("获取启动页成功");
//                        startDown();
                        mView.starLaunchImgSer();
                    }
                });
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onResume() {

    }


    @Override
    public void onDestory() {
        Intent intent2 = new Intent();
        intent2.putExtra(Constant.BURN_TYPE, 4);
        intent2.setAction("com.burnSigle.TotalTime");
        getContext().sendBroadcast(intent2);
        disposable.dispose();
    }
}

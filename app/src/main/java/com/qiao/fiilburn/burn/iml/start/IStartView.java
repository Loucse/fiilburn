package com.qiao.fiilburn.burn.iml.start;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.BaseIView;
import com.qiao.fiilburn.burn.view.custom.BaseVisua2View;
import com.qiao.fiilburn.burn.view.custom.CirclePointView;

import butterknife.BindView;

public interface IStartView extends BaseIView {

    /**
     * 返回
     *
     * @return
     */
    ImageView titleBarBack();
    /**
     * 返回
     *
     * @return
     */
    ImageView titleBarabout();

    /**
     * 标题
     *
     * @return
     */
    TextView titleBarTitle();

    /**
     * 添加
     *
     * @return
     */
    ImageView titleBarAdd();

    /**
     * 煲耳机的圆
     *
     * @return
     */
    CirclePointView cpBackground();

    /* ImageView ivStartBkd();*/


    /**
     * 时间文字
     *
     * @return
     */
    TextView tvOver();

    /**
     * 阶段音量
     *
     * @return
     */
    TextView tvStage();


    /**
     * 不知道啥时候显示的休息中
     *
     * @return
     */
    TextView tvRest();

//    RelativeLayout rlStartImage();

    /**
     * 第几次啊
     *
     * @return
     */
    TextView tvBurnNumber();

    /**
     * 煲机名称
     *
     * @return
     */
    TextView tvName();

    /**
     * 不知道显示什么的时候显示
     *
     * @return
     */
    Button btnBasic();

    /**
     * 开始
     *
     * @return
     */
    Button btnStart();

    /**
     * 暂停
     *
     * @return
     */
    Button btnPause();

    /**
     * 停止
     *
     * @return
     */
    Button btnStop();

    /**
     * 循环几次煲机 1
     *
     * @return
     */
    Button btnBurnOne();

    /**
     * 循环几次煲机 2
     *
     * @return
     */
    Button btnBurnThree();

    //下面是对音乐柱子的在处理。
    BaseVisua2View mBaseVisualizerView();

    /**
     * 笑脸啊
     *
     * @return
     */
    ImageView ivTrigon();


    /**
     * 不知道为啥两个笑脸
     *
     * @return
     */
    ImageView ivCircle();


    /**
     * 随机的文字数组
     *
     * @return
     */
    TextView tvFlag();

    /**
     * 煲机开始
     *
     * @param id
     */
    void burnStart(String id);

    /**
     * 煲机暂停
     */
    void burnPause();

/*
    RelativeLayout rlStartFlag();*/

    /**
     * 停止煲机的服务
     */
    void stopService();

    void startService(int type);

    void sendReveicer(int i);

    /**
     * 开始煲机动画
     * @param isBurnMiddle
     */
    void setPlayingAnim(boolean isBurnMiddle);

}

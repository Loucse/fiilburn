package com.qiao.fiilburn.burn.view.act;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.base.BaseActivity;
import com.qiao.fiilburn.base.Constant;
import com.qiao.fiilburn.burn.bean.BurnSingle;
import com.qiao.fiilburn.burn.secvicer.BurnService;
import com.qiao.fiilburn.burn.view.custom.PickerView;
import com.qiao.fiilburn.utils.LogUtil;
import com.qiao.fiilburn.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * 描述： 定时页面
 *
 * @author Yzz
 * @time 2016/4/7  21:37
 */
public class BurnTimeActivity extends BaseActivity {


    @BindView(R.id.title_bar_back)
    ImageView titleBarBack;
    @BindView(R.id.title_bar_title)
    TextView titleBarTitle;
    @BindView(R.id.title_bar_add)
    ImageView titleBarAdd;
    @BindView(R.id.pv_hour)
    PickerView pvHour;
    @BindView(R.id.pv_minute)
    PickerView pvMinute;
    @BindView(R.id.btn_burntime_sun)
    Button btnSun;
    @BindView(R.id.btn_burntime_mon)
    Button btnMon;
    @BindView(R.id.btn_burntime_tue)
    Button btnTue;
    @BindView(R.id.btn_burntime_wed)
    Button btnWed;
    @BindView(R.id.btn_burntime_thu)
    Button btnThu;
    @BindView(R.id.btn_burntime_fri)
    Button btnFri;
    @BindView(R.id.btn_burntime_sat)
    Button btnSat;
    @BindView(R.id.tv_burntime_save)
    TextView tvBurntimeSave;
    @BindView(R.id.title_bar_about)
    ImageView titleBarAbout;
    private String timing;//总的时间字符串
    private String id;//煲机方案的id
    private Boolean isMonday, isTuesday, isWednesday, isThursday, isFriday, isSaturday, isSunday;
    private String xqString = "日、一、二、三、四、五、六";
    private int dayNumber;//日期的数量
    private BurnService burnService = BurnService.getInstance();
    private String hourChoose;//选中的小时
    private String minuteChoose;//选中的分钟

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSystem7Gray();
        setTransNavigation();
        Intent intent = getIntent();
        titleBarAbout.setVisibility(View.GONE);
        timing = intent.getStringExtra(Constant.TIME);
        id = intent.getStringExtra(Constant.BURN_ID);//获取的方案的id
        LogUtil.i("持久化++获取到的时间" + timing);
        titleBarTitle.setVisibility(View.VISIBLE);
        titleBarTitle.setText(getResources().getText(R.string.burntime_title));
        titleBarAdd.setVisibility(View.INVISIBLE);
        dealTime();
        dealDay();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_burn_time;
    }

    /**
     * 处理时间
     */
    private void dealTime() {
        String time = gTime();
        String[] time1 = time.split(":");
//        timepicker.setIs24HourView(true);
//        timepicker.setCurrentHour(Integer.parseInt(time1[0]));
//        timepicker.setCurrentMinute(Integer.parseInt(time1[1]));
        hourChoose = time1[0];
        minuteChoose = time1[1];
        pvHour.setLeft(true);
        pvMinute.setLeft(false);
        List<String> hour = new ArrayList<String>();
        List<String> minute = new ArrayList<String>();
        for (int i = 0; i < 24; i++) {
            if (i < 10) {
                hour.add("0" + i);
            } else {
                hour.add(String.valueOf(i));
            }
        }
        for (int i = 0; i < 60; i++) {
            minute.add(i < 10 ? "0" + i : "" + i);
        }
        pvHour.setData(hour);
        pvHour.setOnSelectListener(new PickerView.onSelectListener() {

            @Override
            public void onSelect(String text) {
                hourChoose = text;
                LogUtil.i("数值++++的户型好pvHour+" + text);
            }
        });
        pvMinute.setData(minute);
        pvMinute.setOnSelectListener(new PickerView.onSelectListener() {

            @Override
            public void onSelect(String text) {
                minuteChoose = text;
                LogUtil.i("数值++++的户型好+" + text);
            }
        });
        pvHour.setSelected(Integer.parseInt(time1[0]));
        pvMinute.setSelected(Integer.parseInt(time1[1]));
    }


    /**
     * 处理日期
     */
    private void dealDay() {
        String dateWeek = gDate();
        String[] split = xqString.split("、");
        Boolean[] week = new Boolean[7];
        for (int i = 0; i < split.length; i++) {
            week[i] = dateWeek.contains(split[i]);
            if (week[i]) {
                dayNumber++;
            }
        }
        isSunday = week[0];
        isMonday = week[1];
        isTuesday = week[2];
        isWednesday = week[3];
        isThursday = week[4];
        isFriday = week[5];
        isSaturday = week[6];

        btnMon.setSelected(isMonday);
        btnTue.setSelected(isTuesday);
        btnWed.setSelected(isWednesday);
        btnThu.setSelected(isThursday);
        btnFri.setSelected(isFriday);
        btnSat.setSelected(isSaturday);
        btnSun.setSelected(isSunday);
    }


    /**
     * 获取时间日期字符串
     *
     * @return
     */
    public String gTime() {
        if (timing == null || timing.trim().equals("")) {
            return "21:00";
        }

        String[] temp = timing.split("\\|");
        if (temp == null || temp.length < 2) {
            return "21:00";
        }
        return temp[0];
    }

    /**
     * 获取日期字符串
     *
     * @return
     */
    public String gDate() {
        if (timing == null || timing.trim().equals("")) {
            return "日、一、二、三、四、五、六";
        }

        String[] temp = timing.split("\\|");
        if (temp == null || temp.length < 2) {
            return "日、一、二、三、四、五、六";
        }
        return temp[1];
    }

    /**
     * 更新burnSigle
     */
    private void updateBurn() {
        LogUtil.i("这是 BurnTimeActivity 的 打印一个 +—— ：" + id);
        BurnSingle burnSigle = burnService.getBurnSigle(id);
        String t = burnSigle.getTiming();
        String tids = burnSigle.getTimingids();
        String time;
        Integer currentHour = Integer.parseInt(hourChoose);
        Integer currentMinute = Integer.parseInt(minuteChoose);
        if (currentMinute < 10) {
            if (currentHour < 10) {
                time = "0" + currentHour + ":0" + currentMinute;
            } else {
                time = currentHour + ":0" + currentMinute;
            }
        } else {
            if (currentHour < 10) {
                time = "0" + currentHour + ":" + currentMinute;
            } else {
                time = currentHour + ":" + currentMinute;
            }
        }
        String xq = (isSunday ? "日、" : "") + (isMonday ? "一、" : "") + (isTuesday ? "二、" : "") + (isWednesday ? "三、" : "") + (isThursday ? "四、" : "")
                + (isFriday ? "五、" : "") + (isSaturday ? "六、" : "");
        xq = xq.length() > 0 ? xq.substring(0, xq.length() - 1) : xq;//最后的"、"被处理了
        String timeXq = time + "|" + xq;
        burnSigle.setTiming(timeXq);
        LogUtil.i("数值++++最后的定时+" + timeXq);
        // burnSigle.setSname(tvTitle.getText().toString());
        burnService.updateBurnSigleForTiming(burnSigle, t, tids);
        burnService.commitLocal();
        Intent intent = new Intent();
        intent.putExtra(Constant.TIMEING, timeXq);
        intent.putExtra(Constant.BURN_TYPE, 3);
        intent.setAction("com.burnSigle.TotalTime");
        sendBroadcast(intent);
        BurnTimeActivity.this.finish();
    }


    @OnClick({R.id.title_bar_back, R.id.btn_burntime_sun, R.id.btn_burntime_mon, R.id.btn_burntime_tue, R.id.btn_burntime_wed, R.id.btn_burntime_thu, R.id.btn_burntime_fri, R.id.btn_burntime_sat, R.id.tv_burntime_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.title_bar_back:
                finish();
                break;
            case R.id.btn_burntime_sun:
                if (isSunday) {
                    if (dayNumber == 1) return;
                    btnSun.setSelected(false);
                    isSunday = false;
                    dayNumber--;
                } else {
                    btnSun.setSelected(true);
                    isSunday = true;
                    dayNumber++;
                }
                break;
            case R.id.btn_burntime_mon:
                if (isMonday) {
                    if (dayNumber == 1) return;
                    btnMon.setSelected(false);
                    isMonday = false;
                    dayNumber--;
                } else {
                    btnMon.setSelected(true);
                    isMonday = true;
                    dayNumber++;
                }
                break;
            case R.id.btn_burntime_tue:
                if (isTuesday) {
                    if (dayNumber == 1) return;
                    btnTue.setSelected(false);
                    isTuesday = false;
                    dayNumber--;
                } else {
                    btnTue.setSelected(true);
                    isTuesday = true;
                    dayNumber++;
                }
                break;
            case R.id.btn_burntime_wed:
                if (isWednesday) {
                    if (dayNumber == 1) return;
                    btnWed.setSelected(false);
                    isWednesday = false;
                    dayNumber--;
                } else {
                    btnWed.setSelected(true);
                    isWednesday = true;
                    dayNumber++;
                }
                break;
            case R.id.btn_burntime_thu:
                if (isThursday) {
                    if (dayNumber == 1) return;
                    btnThu.setSelected(false);
                    isThursday = false;
                    dayNumber--;
                } else {
                    btnThu.setSelected(true);
                    isThursday = true;
                    dayNumber++;
                }
                break;
            case R.id.btn_burntime_fri:
                if (isFriday) {
                    if (dayNumber == 1) return;
                    btnFri.setSelected(false);
                    isFriday = false;
                    dayNumber--;
                } else {
                    btnFri.setSelected(true);
                    isFriday = true;
                    dayNumber++;
                }
                break;
            case R.id.btn_burntime_sat:
                if (isSaturday) {
                    if (dayNumber == 1) return;
                    btnSat.setSelected(false);
                    isSaturday = false;
                    dayNumber--;
                } else {
                    btnSat.setSelected(true);
                    isSaturday = true;
                    dayNumber++;
                }
                break;
            case R.id.tv_burntime_save:
                //最下面的保存的按钮
                if (dayNumber == 0) {
                    ToastUtils.showToast(this, "至少选择某一天");
                    return;
                }
                updateBurn();
                break;
        }
    }
}

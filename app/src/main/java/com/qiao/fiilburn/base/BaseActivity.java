package com.qiao.fiilburn.base;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.burn.bean.BurnInfo;
import com.qiao.fiilburn.burn.secvicer.BurnService;
import com.qiao.fiilburn.utils.LogUtil;
import com.umeng.analytics.MobclickAgent;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by qiao9 on 2018/6/20.
 */

public class BaseActivity extends FragmentActivity {


    private Unbinder unbinder;
    private Map<String, String> saveUmMap;// 友盟上传数据的map

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getLayoutId() != 0) {
            setContentView(getLayoutId());
        }
        if (unbinder == null) {
            unbinder = ButterKnife.bind(this);
        }

        saveUmMap = new HashMap<String, String>();// 友盟上传数据的map
    }

    /**
     * 设置沉浸式状态栏
     */
    protected void setStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final ViewGroup linear_bar = findViewById(R.id.rl_title);
            final int statusHeight = getStatusBarHeight();
            linear_bar.post(new Runnable() {
                @Override
                public void run() {
                    int titleHeight = linear_bar.getHeight();
                    android.widget.LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linear_bar.getLayoutParams();
                    params.height = statusHeight + titleHeight;
                    linear_bar.setLayoutParams(params);
                }
            });
        }
        setSystem7Gray();
    }

    /**
     * 获取状态栏的高度
     */
    protected int getStatusBarHeight() {
        try {
            Class<?> c = Class.forName("com.android.internal.R$dimen");
            Object obj = c.newInstance();
            Field field = c.getField("status_bar_height");
            int x = Integer.parseInt(field.get(obj).toString());
            return getResources().getDimensionPixelSize(x);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 解决7.0沉浸式状态栏灰色
     */
    protected void setSystem7Gray() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            try {
                Class decorViewClazz = Class.forName("com.android.internal.policy.DecorView");
                Field field = decorViewClazz.getDeclaredField("mSemiTransparentStatusBarColor");
                field.setAccessible(true);
                field.setInt(getWindow().getDecorView(), Color.TRANSPARENT);  //改为透明
            } catch (Exception e) {
                LogUtil.e("设置7.0沉浸式状态栏失败");
            }
        }
    }

    /**
     * 设置透明导航栏
     */
    protected void setTransNavigation() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);//设置透明状态栏
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);//设置透明导航栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //注意要清除 FLAG_TRANSLUCENT_STATUS flag
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(getResources().getColor(android.R.color.transparent));
            getWindow().getDecorView().setSystemUiVisibility(/*View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | */View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    @LayoutRes
    public int getLayoutId() {
        return 0;
    }

    public void setHideTile() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);    //设置全屏
    }

    /**
     * 保存行为日志，就是所有的地方加统计热点
     *
     * @param aid
     * @param params
     */
    public void saveLog(String aid, String params) {
        /************** 添加友盟的统计 ***************/
        saveUmMap.clear();
        if (TextUtils.isEmpty(params)) {
            saveUmMap.put("isLog", "");
        } else {
            String[] split = params.split(",");
            if (split.length == 2) {
                saveUmMap.put("isLog", split[0]);
                saveUmMap.put("isLog1", split[1]);
            } else {
                saveUmMap.put("isLog", params);
            }
        }
        MobclickAgent.onEventValue(this, aid, saveUmMap, 0);
      /*  ************ 结束友盟的统计 *******************
        actionList.add(aid + (params == null ? "" : (":" + params)));
        if (actionList.size() > SAVETHEMINIMUM) {
            saveActionLog();
        }*/
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (BurnInfo.isPlaying()) {
            saveLog("1010", "");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        // 保存煲机进度到服务器
        BurnService.getInstance().commitLocal();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


}

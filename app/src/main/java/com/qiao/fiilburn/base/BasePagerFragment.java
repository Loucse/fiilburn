package com.qiao.fiilburn.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qiao.fiilburn.utils.LogUtil;

import java.lang.reflect.Field;


/**
 * 主界面
 * Created by hyb on 2016/2/26.
 */
public abstract class BasePagerFragment extends Fragment {
    protected Context mContext;
    protected LayoutInflater mLayoutInflater;
    protected boolean isFirstLookSet = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.mContext = BasePagerFragment.this.getActivity();
        mLayoutInflater = inflater;
        ViewGroup v = (ViewGroup) getView(container, savedInstanceState).getParent();
        if (v != null) {
            v.removeAllViews();
        }
        View view = getView(container, savedInstanceState);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData(savedInstanceState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        LogUtil.e(isVisibleToUser + "");
    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtil.e("onResume");
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }

    /**
     * 得到试图
     *
     * @return
     */
    public abstract View getView(ViewGroup container, Bundle savedInstanceState);

    /**
     * 初始化数据
     */
    public abstract void initData(Bundle savedInstanceState);

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
//            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
//            throw new RuntimeException(e);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLayoutInflater = null;
        mContext = null;
    }
}

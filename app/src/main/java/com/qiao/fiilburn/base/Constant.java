package com.qiao.fiilburn.base;

import com.qiao.fiilburn.launcher.bean.UrlImageVideo;

import java.util.List;

/**
 * Created by qiao9 on 2018/6/20.
 * 静态常量类
 */

public class Constant {

    public static final String earJson = "{\"code\":\"200\",\"desc\":\"SUCCESS\",\"data\":[{\"type\":1,\"setitle\":\"Driifter\",\"seimg\":\"http://manage.fengeek.com/fileupload/images/20170915/20170915181350_457.png\",\"sename\":\"‘随身星’系列\"},{\"type\":2,\"setitle\":\"Driifter\",\"seimg\":\"http://manage.fengeek.com/fileupload/images/20170915/20170915181350_457.png\",\"sename\":\"‘随身星’系列\"}]}";
    //    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // 存储耳机类型的数据，用于耳机选择页，和主页的左侧
    public static final String GET_EAR_MODEL = "getearmodel";
    public static final String FLOW_DOWNLOAD = "flow_download";  //使用流量下载true 为不可以  false  为可以
    public static final String SPLASH_VIDEO_VOICE = "videovoice";// 用来存储在开始的视频文件声音的处理，记录是否有声音。
    public static final String CURRENT_COME = "CURRENT_COME";//用来判断第几次进入app
    public static final String ENTER_CHOOSE = "enterChoose"; // 记录从哪里进入ChooseEarphone的问题,就是记录位置
    public static final String SPLASH_DATA = "splashData";// 用来存储一份永久的服务器获取的url图片，视频，开始停止日期，就是服务器存贮的数据
    public static final String CHOOSE_MAIN = "choose_main";// 游客进入记录从选择耳机到主界面选择的值，
    public static final String BURN = "burn"; //煲机保存到本地
    public static final String BURN_ID = "id";//跳转ID
    public static final String BURN_TYPE = "BurnType";//煲机类型
    public static final String BURN_ENTRY = "entry";
    public static final String BURN_FLAG = "flag"; //煲机意图传递
    public static final String TIMEING = "timing";//煲机时间
    public static final String TIME = "time";
    public static final String BURN_LIST = "burnlist";
    public static final String WEEK = "week";
    public static final String MULTI_MEDIA_VOLUME = "multimediavolume";// 开始煲机之前多媒体音量的存储，目的在于煲机完成声音的恢复
    public static final String BURN_OPER = "oper";//用来传递判断煲机状态
    public static final String BURN_BSTOSOUND = "bsToSound";//传递BurnID到SoundService  不知道为啥用这个传递
    public static final String BURN_SNAME = "sname";
    public static final String BURN_PERCNET = "percent";
    public static final String EAR_TYPE = "earType";

    //    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public static final int CODE_SPLASH_START = 116;// 启动页/启动视频数据接口

    //    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // 每一个方案的煲机时长,remainingTime时间,BurnSigle用BS代替
    public static final long BS_RTIME = 3000 * 60 * 1000;


    //    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public static final String UMENG_KEY = "5b398538f43e4820d000003d";

    public static final String LAUNCHER = "1001";
    public static final String FIRST_LAUNCHER = "1002";
    public static final String CREATE_BRUN = "1003";
    public static final String CREATE_BURN_SUCCESS = "1004";
    public static final String DELETE_BURN = "1005";
    public static final String START_BURN = "1006";
    public static final String START_BURN_MIN = "1007";
    public static final String STOP_BURN = "1008";
    public static final String STOP_BURN_MIN = "1009";
    public static final String BURNING_APPED = "1010";
    public static final String SHOW_LAUNCHER_PIC = "1001";
    public static final String JUMP_LAUNCHER_URL = "1012";
    public static final String SHOW_LAUNCHER_VID = "1013";
    public static final String JUMP_LAUNCHER_VID = "1001";
    public static final String SKIP_VID = "1015";
    public static final String VID_DOWNING = "1016";
    public static final String VID_DOWNED = "1017";

    //    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    public static List<UrlImageVideo> splashData = null;// 就是存储从服务器获取的数据，简单的存储在这个APP中

    public static List<UrlImageVideo> getSplashData() {
        return splashData;
    }

    public static void setSplashData(List<UrlImageVideo> splashData) {
        Constant.splashData = splashData;
    }
}

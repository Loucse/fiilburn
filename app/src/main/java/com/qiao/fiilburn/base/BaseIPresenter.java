package com.qiao.fiilburn.base;

import android.content.Context;

public interface BaseIPresenter {
    Context getContext();

    void onCreate();

    void onStart();

    void onResume();

    void onDestory();
}

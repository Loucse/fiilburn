package com.qiao.fiilburn.base;

import android.support.multidex.MultiDexApplication;

import com.qiao.fiilburn.utils.ChannelUtil;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;

public class BaseApplication extends MultiDexApplication {

    public static BaseApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        if (sInstance == null) {
            synchronized (BaseApplication.class) {
                if (sInstance == null) {
                    sInstance = this;
                }
            }
        }
        /**
         2.* 设置组件化的Log开关
         3.* 参数: boolean 默认为false，如需查看LOG设置为true
         4.*/
        umengInit();
//        x.Ext.init(this);//Xutils初始化
    }

    private void umengInit() {
        String channel = ChannelUtil.getChannel(this, "Umeng");
        UMConfigure.setLogEnabled(false);
        UMConfigure.init(this, Constant.UMENG_KEY, channel, UMConfigure.DEVICE_TYPE_PHONE, null);
        MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_UM_NORMAL);
        MobclickAgent.setCatchUncaughtExceptions(true);
    }


    public static BaseApplication getInstance() {
        return sInstance;
    }

}

package com.qiao.fiilburn.base;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.umeng.analytics.MobclickAgent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by qiao9 on 2018/6/21.
 */

public class BaseService extends Service implements Constant_url {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    /**
     * 保存行为日志，就是所有的地方加统计热点
     *
     * @param aid
     * @param params
     */
    public static Map<String, String> saveUmMap = new HashMap<String, String>();// 友盟上传数据的map
    public static List<String> actionList = new ArrayList<String>();
    //    protected HttpService httpService = HttpService.getInstance(coreUrl, upUrl, actionLogUrl, coreVoiceUrl);
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    public void saveLog(String aid, String params) {
        try {
            /************** 添加友盟的统计 ***************/
            saveUmMap.clear();
            if (params == null || params.equals("")) {
                saveUmMap.put("isLog", "");
            } else {
                String[] split = params.split(",");
                if (split.length == 2) {
                    saveUmMap.put("isLog", split[0]);
                    saveUmMap.put("isLog1", split[1]);
                } else {
                    saveUmMap.put("isLog", params);
                }
            }
            MobclickAgent.onEventValue(this, aid, saveUmMap, 0);
            /************* 结束友盟的统计 ********************/
            actionList.add(aid + (params == null ? "" : ("," + params)));
            /*if (actionList.size() > SAVETHEMINIMUM) {
                saveActionLog();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 友盟上传1个参数  服务器上传2个参数
     *
     * @param aid
     * @param params
     * @param flag
     */
    public void saveLog(String aid, String params, boolean flag) {
        try {
            /************** 添加友盟的统计 ***************/
            saveUmMap.clear();
            if (params == null || params.equals("")) {
                saveUmMap.put("isLog", "");
            } else {
                String[] split = params.split(",");
                if (split.length == 2) {
                    saveUmMap.put("isLog", split[0]);
                    saveUmMap.put("isLog1", split[1]);
                } else {
                    saveUmMap.put("isLog", params);
                }
            }
            actionList.add(aid + (params == null ? "" : (":" + params)));
           /* if (actionList.size() > 0) {
                saveActionLog();
            }*/
            if (flag)
                saveUmMap.remove("isLog1");
            MobclickAgent.onEventValue(this, aid, saveUmMap, 0);
            /************* 结束友盟的统计 ********************/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 保存行为日志
     */
   /* private void saveActionLog() {
        if (actionList == null || actionList.size() == 0) {
            return;
        }
        Map<String, String> params = new HashMap<String, String>();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < actionList.size(); i++) {
            if (i == actionList.size() - 1) {
                sb.append(actionList.get(i));
                break;
            }
            sb.append(actionList.get(i) + "|");
        }
        actionList.clear();
        params.put("p", sb.toString());
        if (NetUtils.isConnected(getApplicationContext())) {
            httpService.doActionLog(null, HttpReqHeader.CODE_UPLOAD_ACTION, params);
        } else {
            LogUtil.apendActionLog("[" + sdf.format(new Date()) + "&&&" + HttpReqHeader.getInstance().toString() + "&&&" + JSONArray.toJSON(params).toString() + "]");//取消头部信息
//            LogUtil.i("统计点问题++没有网络" + JSONArray.toJSON(params).toString());
        }
    }*/
}

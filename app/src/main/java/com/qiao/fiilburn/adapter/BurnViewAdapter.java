package com.qiao.fiilburn.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qiao.fiilburn.R;
import com.qiao.fiilburn.burn.bean.BurnInfo;
import com.qiao.fiilburn.burn.bean.BurnSingle;
import com.qiao.fiilburn.burn.secvicer.BurnService;
import com.qiao.fiilburn.burn.view.custom.BurnHeatSetCircleView;
import com.qiao.fiilburn.burn.view.custom.SlideUpDelete;
import com.qiao.fiilburn.burn.view.frg.BurnFragemnt;
import com.qiao.fiilburn.utils.LogUtil;
import com.qiao.fiilburn.utils.SPUtils;

import java.util.List;

public class BurnViewAdapter extends RecyclerView.Adapter<BurnViewAdapter.ViewHolder> {

    private Activity act;
    private List<BurnSingle> datas;
    private int dp20, dp40;
    protected LayoutInflater mLayoutInflater;
    private String weekDay[];
    private BurnFragemnt fragemnt;

    public void setFragemnt(BurnFragemnt fragemnt) {
        this.fragemnt = fragemnt;
    }

    public BurnViewAdapter(Activity act, List<BurnSingle> datas, int[] dparray, String[] weekDay, LayoutInflater mLayoutInflater) {
        this.act = act;
        this.datas = datas;
        if (dparray != null && dparray.length >= 1) {
            dp20 = dparray[0];
            dp40 = dparray[1];
        }
        this.weekDay = weekDay;
        this.mLayoutInflater = mLayoutInflater;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LogUtil.i("持久化+++得到的适配器里面的值+ new ViewHolder(view)得到视图" + viewType + "datas.size()" + datas.size());
        for (BurnSingle burnSingle : datas) {
            LogUtil.i("butnFragment _ viewHolder _ datas _ .tostring " + burnSingle.toString());
        }
        ViewHolder viewHolder = null;
        if (viewType == 0) {
            View view = mLayoutInflater.inflate(R.layout.burn_jiange, parent, false);
            AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(dp20, ViewGroup.LayoutParams.MATCH_PARENT);
            view.setLayoutParams(layoutParams);
            viewHolder = new ViewHolder(view);
        } else if (viewType == 1) {  //
            viewHolder = new ViewHolder(mLayoutInflater.inflate(R.layout.item_pager_burn, parent, false));
        } else {
            View view = mLayoutInflater.inflate(R.layout.burn_jiange, parent, false);
            AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(dp40, ViewGroup.LayoutParams.MATCH_PARENT);
            view.setLayoutParams(layoutParams);
            viewHolder = new ViewHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final BurnSingle bean = datas.get(position);
        if (getItemViewType(position) != 1) {
            return;
        }

        int burntype = bean.getBurntype();
        switch (burntype) {
            case 1:
//                    holder.iv_burn_heatset_mode.setImageResource(R.mipmap.fiil_mode_burn2);
                holder.iv_burn_heatset_mode.setImageResource(R.mipmap.fiil_mode_burn8);
                break;
            case 2:
//                    holder.iv_burn_heatset_mode.setImageResource(R.mipmap.fiil_mode_burn2);
                holder.iv_burn_heatset_mode.setImageResource(R.mipmap.fiil_mode_burn8);
                break;
        }
        if (bean.isOpen()) {
            holder.burn_sud.close();
        }

        holder.burn_sud.setOnStateChangeListener(new SlideUpDelete.OnStateChangeListener() {
            @Override
            public void onOpean(SlideUpDelete swapView) {
                bean.setOpen(true);
            }

            @Override
            public void onClose(SlideUpDelete swapView) {
                bean.setOpen(false);
            }

            @Override
            public boolean onDown(SlideUpDelete swapView) {
                return true;
            }
        });
        holder.tv_burn_name.setText(bean.getSname());
        holder.eac_progress.setTotalTime(act.getString(R.string.burn_heatset_total_time));
        holder.eac_progress.setProgress(Integer.parseInt(bean.gFinishPercent()), bean.gFinishMin());//进度数字+具体的时间
        Boolean isTime = SPUtils.getBoolean(act, bean.getId());//根据方案的id得到是否有定时的选择
        holder.cb_show_tishi.setChecked(isTime);
        holder.rl_burn_prompt_time_more.setEnabled(isTime);
        holder.tv_burn_prompt_time.setEnabled(isTime);
        holder.tv_burn_prompt_week.setEnabled(isTime);
        holder.tv_burn_prompt_time.setText(bean.gTime());
        holder.tv_burn_prompt_week.setText(bean.gDate(weekDay));
        LogUtil.i("数值++drawCylinder持久化问题" + "正在播放BurnInfo.getId()" + BurnInfo.getId() + "BurnInfo.isBurning()" + BurnInfo.isBurning() + "煲机的时间+" + bean.gFinishMin());
        //判断是否有项目正在播放
        if (BurnInfo.getId() != null && !"".equals(BurnInfo.getId())) {
            //有方案在播放看看是哪个方案
            if (BurnInfo.getId().equals(bean.getId()) && BurnInfo.isBurning()) {// 把按钮修改为正在播放
                holder.btn_start_burn_heatset.setSelected(true);
                holder.btn_start_burn_heatset.setText(act.getResources().getString(R.string.burn_heatset_burning));
                /*if (BurnInfo.isPlaying()) {
                    holder.btn_start_burn_heatset.setText(act.getResources().getString(R.string.burn_heatset_burning));
                } else {
                    holder.btn_start_burn_heatset.setText(act.getResources().getString(R.string.burn_heatset_pause));
                }*/
            } else {
                holder.btn_start_burn_heatset.setText(act.getResources().getString(R.string.start_burn_heatset));
                //没有方案在播放
                holder.btn_start_burn_heatset.setSelected(false);
            }
        } else {
            //没有方案在播放
            holder.btn_start_burn_heatset.setSelected(false);
            holder.btn_start_burn_heatset.setText(act.getResources().getString(R.string.start_burn_heatset));
        }
        //删除item
        holder.tv_burn_delete.setOnClickListener(v -> fragemnt.remove(holder.getLayoutPosition(), holder.ll_burn_whole_layout, datas));
        //编辑耳机的名称的监听
        holder.iv_burn_edit_name.setOnClickListener(v -> fragemnt.updataHeatSetName(holder.getLayoutPosition(), "改变名称。。"));
        //开始煲机按钮的监听
        holder.btn_start_burn_heatset.setOnClickListener(v -> fragemnt.updataData(holder.getLayoutPosition(), holder.eac_progress.getProgress() + 1, "300"));
        //定时按钮的开关
        holder.cb_show_tishi.setOnCheckedChangeListener((buttonView, isChecked) -> {
            LogUtil.i("持久化+++得到的onBindViewHolderlimship适配器里面的值+" + "定时器点击出现的是" + isChecked);
            SPUtils.setBoolean(act, bean.getId(), isChecked);
            BurnSingle burnSigle2 = bean;
            BurnService.getInstance().updateBurnSigleOpen(bean, burnSigle2, isChecked);
            BurnService.getInstance().commitLocal();
            holder.rl_burn_prompt_time_more.setEnabled(isChecked);
            holder.tv_burn_prompt_time.setEnabled(isChecked);
            holder.tv_burn_prompt_week.setEnabled(isChecked);
        });
        //时间的修改
        holder.rl_burn_prompt_time_more.setOnClickListener(v -> fragemnt.updataPrompt(holder.getLayoutPosition(), "17:00", "llijljjals"));
    }

    @Override
    public int getItemViewType(int position) {
        return datas.get(position).getRemainingNum();
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout ll_burn_whole_layout;
        TextView tv_burn_delete;
        ImageView iv_burn_heatset_mode, iv_burn_edit_name;  //图标。编辑耳机名称
        TextView tv_burn_name;   //耳机名称
        BurnHeatSetCircleView eac_progress;  //进度显示
        Button btn_start_burn_heatset; //开始煲耳机
        CheckBox cb_show_tishi;  //显示提示
        SlideUpDelete burn_sud;   //滑動刪除控件
        TextView tv_burn_prompt_time;   //耳机提示的时间点
        TextView tv_burn_prompt_week;   //耳机提示的星期时间
        RelativeLayout rl_burn_prompt_time_more;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_burn_heatset_mode = itemView.findViewById(R.id.iv_burn_heatset_mode);
            iv_burn_edit_name = itemView.findViewById(R.id.iv_burn_edit_name);
            tv_burn_name = itemView.findViewById(R.id.tv_burn_name);
            eac_progress = itemView.findViewById(R.id.eac_progress);
            btn_start_burn_heatset = itemView.findViewById(R.id.btn_start_burn_heatset);
            cb_show_tishi = itemView.findViewById(R.id.cb_show_tishi);
            tv_burn_delete = itemView.findViewById(R.id.tv_burn_delete);
            burn_sud = itemView.findViewById(R.id.burn_sud);
            tv_burn_prompt_time = itemView.findViewById(R.id.tv_burn_prompt_time);
            tv_burn_prompt_week = itemView.findViewById(R.id.tv_burn_prompt_week);
            rl_burn_prompt_time_more = itemView.findViewById(R.id.rl_burn_prompt_time_more);
            ll_burn_whole_layout = itemView.findViewById(R.id.ll_burn_whole_layout);
        }
    }

}


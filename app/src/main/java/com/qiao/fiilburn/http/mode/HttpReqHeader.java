package com.qiao.fiilburn.http.mode;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;


import com.qiao.fiilburn.base.BaseActivity;
import com.qiao.fiilburn.utils.AppUtils;
import com.qiao.fiilburn.utils.AuthorityUtil;
import com.qiao.fiilburn.utils.LogUtil;

import java.util.Date;
import java.util.Locale;

/**
 * 描述： http请求头部消息
 *
 * @author Yzz
 * @time 2016/2/23  12:00
 */
public class HttpReqHeader {

    private String aid; // 内部Appid
    private int pla; // 平台 1:ios，2:Android
    private String opcode; // 操作码
    private String hw; // 硬件类型
    private String idfa; // IDFA
    private String idfv; // 重新安装后会变化
    private int jail; // 是否破解 1：破解，0：非破解
    private String lan; // 语言
    private String net; // 网络
    private String oudid; // OpenUDID
    private int pon; // Push是否打开 0:未打开，1:打开
    private String sv; // 系统版本号
    private String ua; // 渠道
    private String uuid; // uuid，只在用户第一次启动时候会生成；重新安装时会变化
    private String ver; // 版本号
    private String res; // 屏幕分辨率
    private String auth; // 认证
    private String timeid;//时间戳

    public static final int CODE_REG_DEVICE = 101;// 注册设备
    public static final int CODE_GET_INITPARAM = 102;// 没有用到
    public static final int CODE_GET_VERSION_HEADSET = 103;// 获取耳机是否有更新版本（附带最新版本号,版本描述，下载地址）
    public static final int CODE_REG_THIRD_PARTY = 104;// app第三方登陆，向数据库中写用户表(返回个人信息，包括头像信息)
    public static final int CODE_UPLOAD_PROCESS_BURN = 105;// 上传煲机进度
    public static final int CODE_GET_PROCESS_BURN = 106;// 获取煲机进度
    public static final int CODE_UPDOAD_DEVICE_TOKEN = 107;// 上传devicetoken
    public static final int CODE_GET_QA = 108;// 获取Q&A
    public static final int CODE_UPLOAD_SETTING_HEADSET = 109;// 上传耳机设置信息
    public static final int CODE_GET_SETTING_HEADSET = 110;// 获取耳机设置信息
    public static final int CODE_UPLOAD_ACTION = 111;// 用户行为统计
    public static final int CODE_UPLOAD_LOG = 112;// 离线日志上传
    public static final int CODE_LOGIN = 113;// 用户登陆
    public static final int CODE_CHECK_PRO = 114;// 正品验证
    public static final int CODE_FEED_BACK = 115;// 意见反馈
    public static final int CODE_SPLASH_START = 116;// 启动页/启动视频数据接口
    public static final int CODE_BOUND = 119;// 用户绑定设备，解除设备等
    public static final int CODE_PRODUCT_REGIST = 120;  //产品注册
    public static final int CODE_PRODUCT_MODEL = 121;  //获取耳机型号(3.0以后的版本替换掉)
    public static final int CODE_GET_USER_MUSIC = 123;  //得到用户的最爱列表
    public static final int CODE_KUWO_MUSIC = 124;  //搜索音乐
    public static final int CODE_SPOTIFY_MUSIC = 125;  //搜索spotify音乐
    public static final int CODE_PARAMETER_CONFIG = 127;  //app启动参数配置列表
    public static final int CODE_UPDATE = 128;   //自升级
    public static final int CODE_HEATH_INFORMATION = 129;  //用户健康信息
    public static final int CODE_USER_STEPS = 130;  //用户日步数信息上传/获取
    public static final int CODE_HEART_SPROT_UPLOAD = 131;  //用户心律数据上传
    public static final int CODE_HEART_SPROT_REQUEST = 132;  //获取用户心律数据
    public static final int CODE_HEART_SPROT_DELETE = 133;  //删除用户心律数据
    public static final int CODE_UPLOAD_HEART_RATE = 134;  //上传用户测量记录（包括静息心率、最大心率、疲劳度）
    public static final int CODE_DOWN_HEART_RATE = 135;  //下载用户测量记录（包括静息心率、最大心率、疲劳度）
    public static final int CODE_SPEECH_RECOGNITION = 136;  //语义识别，服务器自己拆分歌手歌名
    public static final int CODE_MUSIC_SERVER = 137;  //根据歌手，歌名处理
    public static final int CODE_MUSIC_URL = 138;  //得到具体的播放地址
    public static final int CODE_HEART_TOTAL_DATA = 139;   //得到用户的总里程和总卡路里数据
    public static final int CODE_HEART_AVG_DATA = 140;   //得到平均数值
    //    public static final int CODE_WEIRUAN_GOOGLE = 143;   //使用谷歌还是微软2.1.8的接口废掉了
    public static final int CODE_GET_SONG_LRC = 144;   //得到歌曲歌词
    public static final int CODE_GET_SONG_IMG = 145;   //得到歌曲图片
    public static final int CODE_MUSIC_HOME_BANNER = 146;   //音乐首页获取banner图片
    public static final int CODE_MUSIC_HOME_TYPE = 147;   //音乐首页下面的分类的信息
    public static final int CODE_MUSIC_LIST = 148;   //音乐榜单的所有数据
    public static final int CODE_MUSIC_DETAILS = 149;   //音乐列表的详细信息。
    public static final int UPDATE_MUSIC_PURE = 151;   //更新歌曲库
    public static final int CODE_CHOICE_MODEL = 150;   //选择耳机最新的接口用于3.0.0


    private static final HttpReqHeader single = new HttpReqHeader();

    // 静态工厂方法
    public static HttpReqHeader getInstance() {
        return single;
    }

    /**
     * 构造函数
     */
    private HttpReqHeader() {
    }

    public void init(Context context) {
        PackageInfo info = null;
        try {
            info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            // 安卓版本
            this.setPla(2);
            // 手机型号
            this.setHw(android.os.Build.MODEL);
            // String androidId = ""
            // + android.provider.Settings.Secure.getString(
            // c.getContentResolver(),
            // android.provider.Settings.Secure.ANDROID_ID);
            // 获取手机系统的语言
            Locale locale = context.getResources().getConfiguration().locale;
            this.setLan(locale.getLanguage());
            // 网络
            ConnectivityManager manager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = manager.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                // ConnectivityManager.TYPE_WIFI;
                // activeNetworkInfo.getType();
                // 返回一个"说人话"的网络名称:WIFI/MOBILE
                String wifiName = activeNetworkInfo.getTypeName();
                if (TextUtils.isEmpty(wifiName)) {
                    this.setNet("unkown");
                } else {
                    this.setNet(activeNetworkInfo.getTypeName());
                }
            }
            // 系统版本号 比如安卓4.2.2
            this.setSv(android.os.Build.VERSION.RELEASE);
            // 版本号
            this.setVer(info.versionName);
            this.setTimeid(String.valueOf(new Date().getTime()));//2.0以后添加的东西。。
            this.setAuth(AuthorityUtil.auth(this.timeid));
            this.setUa(AppUtils.getChannel(context));
            if (context instanceof BaseActivity) {
                DisplayMetrics displaysMetrics = new DisplayMetrics();
                ((BaseActivity) context).getWindowManager().getDefaultDisplay().getMetrics(displaysMetrics);
                String showSize = "{" + displaysMetrics.widthPixels + "*" + displaysMetrics.heightPixels + "}";
                this.setRes(showSize);
            }
            // 手机的IMEI唯一标识,在安卓6.0以后这个权限需要，动态处理
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                this.setIdfa(telephonyManager.getDeviceId());
            }
            this.setUuid(getIdfa());
        } catch (NameNotFoundException e) {
            Log.e("HttpReqHeader", Log.getStackTraceString(e));
        } catch (Exception e) {
            Log.e("HttpReqHeader", Log.getStackTraceString(e));
        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        /*
         * sb.append( (this.opcode == null || "".equals(this.opcode)) ? " " :
         * this.opcode).append("|");// 操作码
         */
        sb.append("|");
        sb.append(this.pla).append("|");// 平台 1:ios，2:Android
        sb.append((this.hw == null || "".equals(this.hw)) ? " " : this.hw)
                .append("|");// 硬件类型
        sb.append((this.sv == null || "".equals(this.getSv())) ? " " : this.sv)
                .append("|");// 系统版本号
        sb.append((this.aid == null || "".equals(this.aid)) ? " " : this.aid)
                .append("|");// 内部Appid
        sb.append((this.ver == null || "".equals(this.ver)) ? " " : this.ver)
                .append("|");// app版本号
        sb.append((this.ua == null || "".equals(this.ua)) ? " " : this.ua)
                .append("|");// 渠道
        sb.append((this.lan == null || "".equals(this.lan)) ? " " : this.lan)
                .append("|");// 语言
        sb.append((this.net == null || "".equals(this.net)) ? " " : this.net)
                .append("|");// 网络
        sb.append(this.pon).append("|");// Push是否打开 0:未打开，1:打开
        sb.append(this.jail).append("|");// 是否破解 1：破解，0：非破解
        sb.append((this.res == null || "".equals(this.res)) ? " " : this.res)
                .append("|");// 屏幕分辨率{320, 568}
        sb.append((this.idfa == null || "".equals(this.idfa)) ? " " : this.idfa)
                .append("|");// IDFA
        sb.append((this.uuid == null || "".equals(this.uuid)) ? " " : this.uuid)
                .append("|");// uuid，只在用户第一次启动时候会生成；重新安装时会变化
        sb.append((this.idfv == null || "".equals(this.idfv)) ? " " : this.idfv)
                .append("|");// idfv重新安装后会变化
        sb.append((this.oudid == null || "".equals(this.oudid)) ? " " : this.oudid);////OpenUDID
        sb.append((this.timeid == null || "".equals(this.timeid)) ? " " : this.timeid);//timeid
        return sb.toString();
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public int getPla() {
        return pla;
    }

    public void setPla(int pla) {
        this.pla = pla;
    }

    public String getOpcode() {
        return opcode;
    }

    public void setOpcode(String opcode) {
        this.opcode = opcode;
    }

    public void setOpcode(int opcode) {
        this.opcode = String.valueOf(opcode);
    }

    public String getHw() {
        return hw;
    }

    public void setHw(String hw) {
        this.hw = hw;
    }

    public String getIdfa() {
        return idfa;
    }

    public void setIdfa(String idfa) {
        this.idfa = idfa;
    }

    public String getIdfv() {
        return idfv;
    }

    public void setIdfv(String idfv) {
        this.idfv = idfv;
    }

    public int getJail() {
        return jail;
    }

    public void setJail(int jail) {
        this.jail = jail;
    }

    public String getLan() {
        return lan;
    }

    public void setLan(String lan) {
        this.lan = lan;
    }

    public String getNet() {
        return net;
    }

    public void setNet(String net) {
        this.net = net;
    }

    public String getOudid() {
        return oudid;
    }

    public void setOudid(String oudid) {
        this.oudid = oudid;
    }

    public int getPon() {
        return pon;
    }

    public void setPon(int pon) {
        this.pon = pon;
    }

    public String getSv() {
        return sv;
    }

    public void setSv(String sv) {
        this.sv = sv;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getTimeid() {
        return timeid;
    }

    public void setTimeid(String timeid) {
        this.timeid = timeid;
    }
}

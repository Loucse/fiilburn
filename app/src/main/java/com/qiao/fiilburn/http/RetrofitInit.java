package com.qiao.fiilburn.http;

import com.qiao.fiilburn.base.Constant_url;
import com.qiao.fiilburn.http.httphandler.ApiService;
import com.qiao.fiilburn.http.mode.OkHttpForHead;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @auther qiao9 on 2018/7/10
 * @Project AppTestComponent
 */
public class RetrofitInit implements Constant_url {

    private static RetrofitInit instance;

    private static Retrofit retrofit;

    private static volatile ApiService apiService = null;


    public static RetrofitInit getInstance() {
        if (instance == null) {
            synchronized (RetrofitInit.class) {
                if (instance == null) {
                    instance = new RetrofitInit();
                }
            }
        }
        return instance;
    }


    /**
     * 初始化
     */
    public void initGet() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        retrofit = new Retrofit.Builder()
                .baseUrl(Base_url)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .callbackExecutor(executorService)
                .build();
    }

    public void initPost() {
        OkHttpClient okHttpClient = OkHttpForHead.getBuilder().build();
        retrofit = new Retrofit.Builder()
                .baseUrl(Base_url)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiService getApi() {
        if (apiService == null) {
            synchronized (Request.class) {
                apiService = retrofit.create(ApiService.class);
            }
        }
        return apiService;
    }

}

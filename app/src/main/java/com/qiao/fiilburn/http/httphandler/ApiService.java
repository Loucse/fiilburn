package com.qiao.fiilburn.http.httphandler;

import com.qiao.fiilburn.http.mode.Result;
import com.qiao.fiilburn.launcher.bean.UrlImageVideo;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Streaming;
import retrofit2.http.Url;


/**
 * @auther qiao9 on 2018/7/10
 * @Project AppTestComponent
 */
public interface ApiService {
    @POST("/core.fill")
    Observable<Result<List<UrlImageVideo>>> getLauncher(@Header("opcode") int opcode);

    @Streaming
    @GET
    Observable<ResponseBody> downloadFile(@Url String url);
}

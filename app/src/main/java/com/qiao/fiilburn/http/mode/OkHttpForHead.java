package com.qiao.fiilburn.http.mode;

import java.io.IOException;

import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @auther qiao9 on 2018/7/10
 * @Project AppTestComponent
 */
public class OkHttpForHead {
    private static OkHttpClient.Builder builder = new OkHttpClient.Builder();

    public static OkHttpClient.Builder getBuilder() {
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request chainRequest = chain.request();
                HttpReqHeader reqHeader = HttpReqHeader.getInstance();
                Request.Builder reBuilder = chainRequest.newBuilder()
                        .addHeader("aid", reqHeader.getAid() == null ? "" : reqHeader.getAid())
                        .addHeader("pla", String.valueOf(reqHeader.getPla()))
//                        .addHeader("opcode", String.valueOf(opcode))
                        .addHeader("hw", reqHeader.getHw() == null ? "" : reqHeader.getHw())
                        .addHeader("idfa", reqHeader.getIdfa() == null ? "" : reqHeader.getIdfa())
                        .addHeader("idfv", reqHeader.getIdfv() == null ? "" : reqHeader.getIdfv())
                        .addHeader("jail", String.valueOf(reqHeader.getJail()))
                        .addHeader("lan", reqHeader.getLan() == null ? "" : reqHeader.getLan())
                        .addHeader("net", reqHeader.getNet() == null ? "" : reqHeader.getNet())
                        .addHeader("oudid", reqHeader.getOudid() == null ? "" : reqHeader.getOudid())
                        .addHeader("pon", String.valueOf(reqHeader.getPon()))
                        .addHeader("sv", reqHeader.getSv() == null ? "" : reqHeader.getSv())
                        .addHeader("ua", reqHeader.getUa() == null ? "" : reqHeader.getUa())
                        .addHeader("uuid", reqHeader.getUuid() == null ? "" : reqHeader.getUuid())
//                        .addHeader("ver", reqHeader.getVer() == null ? "" : reqHeader.getVer())
                        .addHeader("ver", "3.2")
                        .addHeader("res", reqHeader.getRes() == null ? "" : reqHeader.getRes())
                        .addHeader("auth", reqHeader.getAuth() == null ? "" : reqHeader.getAuth())
                        .addHeader("timeid", reqHeader.getTimeid() == null ? "" : reqHeader.getTimeid());
                Request request = reBuilder.cacheControl(CacheControl.FORCE_NETWORK).build();
                return chain.proceed(request);
            }
        });
        return builder;
    }
}

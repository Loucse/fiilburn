package com.qiao.fiilburn.http;

import com.qiao.fiilburn.http.exception.ApiException;
import com.qiao.fiilburn.http.exception.CustomException;
import com.qiao.fiilburn.http.mode.Result;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.functions.Function;

/**
 * @auther qiao9 on 2018/7/10
 * @Project AppTestComponent
 */
public class ResponseTransformer {
    public static <T> ObservableTransformer<Result<T>, T> handleResult() {
        return upstream -> upstream.onErrorResumeNext(new ErrorResumeFunction()).flatMap(new ResponseFunctino());
    }

    /**
     * 非服务器产生的异常，比如本地无网络请求，json数据解析错误等等
     *
     * @param <T>
     */

    private static class ErrorResumeFunction<T> implements Function<Throwable, ObservableSource<? extends Result<T>>> {

        @Override
        public ObservableSource<? extends Result<T>> apply(Throwable throwable) throws Exception {
            return Observable.error(CustomException.handleException(throwable));
        }
    }


    private static class ResponseFunctino<T> implements Function<Result<T>, ObservableSource<T>> {

        @Override
        public ObservableSource<T> apply(Result<T> tResult) throws Exception {
            int code = tResult.getCode();
            String message = tResult.getDesc();
            if (code == 200) {
                return Observable.just(tResult.getData());
            } else {
                return Observable.error(new ApiException(code, message));
            }
        }
    }

}
